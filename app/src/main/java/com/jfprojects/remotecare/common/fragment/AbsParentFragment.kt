package com.jfprojects.remotecare.common.fragment

import android.app.Activity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.jfprojects.remotecare.helper.APIConstants
import com.jfprojects.remotecare.common.viewmodel.ApiLogTableViewModel
import com.jfprojects.remotecare.database.models.APILogTable
import com.jfprojects.remotecare.database.models.JSONType
import com.jfprojects.remotecare.helper.views.Progress
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject

abstract class AbsParentFragment : Fragment() {
    protected var activity: Activity? = null

    companion object {
        public var isApiInProcess = false
    }

    private lateinit var mProgress: Progress
    private val apiLogTableViewModel by viewModels<ApiLogTableViewModel>()

    protected abstract fun apiLogSuccess(table: APILogTable?)

    protected abstract fun apiLogFailure(table: APILogTable?)

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)

        mProgress = Progress(context)
        mProgress.setCancelable(false)
        apiLogTableViewModel.getAllApiLogs()!!
            .observe(viewLifecycleOwner, Observer { getAllApiLogs(it) })

    }

    private fun getAllApiLogs(apiLogTables: List<APILogTable>?) {
        if (apiLogTables != null && apiLogTables.isNotEmpty()) {
            hideProgress()
            isApiInProcess = false
            val table: APILogTable = apiLogTables.first()
            try {
                if (TextUtils.isEmpty(table.jsonType) || !TextUtils.isEmpty(
                        table.jsonType
                    ) && table.jsonType.equals(JSONType.JsonObject.name)
                ) {
                    var jsonObject = JSONObject()
                    if (table.body != null)
                        jsonObject = JSONObject(table.body!!)

                    if (table.statusCode != 200) {
                        if (jsonObject.has(APIConstants.ERR) && jsonObject.get(
                                APIConstants.ERR
                            ) is String
                        ) {
//                            if (!table.apiType
//                                    .equalsIgnoreCase(CommonConstants.refreshUser)
//                            ) {
                            apiLogFailure(table)
//                            showMessage(jsonObject.get(APIConstants.ERR).toString())
//                            } else logOutFromModules()
                        } else {
//                            if (!table.apiType
//                                    .equalsIgnoreCase(CommonConstants.refreshUser)
//                            ) {
                            apiLogFailure(table)
//                            } else logOutFromModules()
                        }
                    } else if (table.statusCode == 200) {
                        apiLogSuccess(table)
                    }
                } else {
                    if (table.statusCode != 200) {
//                        if (!table.apiType.equalsIgnoreCase(CommonConstants.refreshUser)) {
                        apiLogFailure(table)
//                        } else logOutFromModules()
                    } else if (table.statusCode == 200) {
                        apiLogSuccess(table)
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
//                if (!table.apiType.equals(CommonConstants.refreshUser)) {
                apiLogFailure(table)
//                } else logOutFromModules()
            } finally {
                apiLogTableViewModel.deleteAllApiLogTable()
            }
        }
    }

    open fun showProgress() {
        lifecycleScope.launch {
            if (!mProgress.isShowing) mProgress.show()
        }

    }

    open fun hideProgress() {
        lifecycleScope.launch {
            if (mProgress.isShowing) mProgress.dismiss()
        }
    }

    fun showMessage(str: String) {
        Toast.makeText(context, str, Toast.LENGTH_SHORT).show()
    }

}
