package com.jfprojects.remotecare.common.fragment

import android.os.Bundle
import android.view.View
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*

abstract class AbsNavParentFragment : AbsParentFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        requireActivity().navigation.visibility = View.VISIBLE
        requireActivity().nav_view.visibility = View.VISIBLE

        requireActivity().drawerDL.setDrawerLockMode(
            DrawerLayout.LOCK_MODE_UNLOCKED,
            GravityCompat.START
        )
    }
}
