package com.jfprojects.remotecare.common.activity

import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import com.jfprojects.remotecare.helper.ContextUtils
import com.jfprojects.remotecare.helper.SharedPreference
import java.util.*

abstract class ParentActivity : AppCompatActivity() {
    var changedLang = ""

    override fun attachBaseContext(newBase: Context?) {
        val lang = SharedPreference.getInstance().getString("lang")
        if (!lang.isNullOrEmpty()) {
            val localeToSwitchTo = Locale(lang)
            val localeUpdatedContext: ContextWrapper =
                ContextUtils.updateLocale(newBase, localeToSwitchTo)
            super.attachBaseContext(localeUpdatedContext)

        } else {
            val localeToSwitchTo = Locale("en")
            val localeUpdatedContext: ContextWrapper =
                ContextUtils.updateLocale(newBase, localeToSwitchTo)
            super.attachBaseContext(localeUpdatedContext)

        }
    }

//    fun updateResources(
//        context: Context,
//        language: String
//    ): Boolean {
//        val locale = Locale(language)
//        Locale.setDefault(locale)
//        val resources = context.resources
//        val configuration = resources.configuration
//        configuration.locale = locale
//        resources.updateConfiguration(configuration, resources.displayMetrics)
//        return true
//    }
//    override fun attachBaseContext(newBase: Context?) {
//        val langCode = SharedPreference.getInstance().getString("lang")
//        //load it from SharedPref
//        val context: ContextWrapper? = changeLang(newBase!!, langCode)
//        super.attachBaseContext(context)
//    }

    fun changeLang(context: Context, lang_code: String): ContextWrapper? {
        val sysLocale: Locale
        val rs: Resources = context.resources
        val config: Configuration = rs.configuration
        sysLocale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.locales[0]
        } else {
            config.locale
        }
        if (lang_code != "" && sysLocale.language != lang_code) {
            val locale = Locale(lang_code)
            Locale.setDefault(locale)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                config.setLocale(locale)
            } else {
                config.locale = locale
            }
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//                context = context.createConfigurationContext(config)
//            } else {
            context.resources
                .updateConfiguration(config, context.resources.displayMetrics)
//            }
        }
        return ContextWrapper(context)
    }
//    fun changeLang(context: Context, lang_code: String){
//    val languageToLoad = "fr_FR"
//    val locale = Locale(languageToLoad)
//    Locale.setDefault(locale)
//    val config = Configuration()
//    config.locale = locale
//    context.resources.updateConfiguration(config, context.resources.displayMetrics)
//
//    val intent = Intent(this@XYZ, XYZ::class.java)
//    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//    startActivity(intent)
//    }
}
