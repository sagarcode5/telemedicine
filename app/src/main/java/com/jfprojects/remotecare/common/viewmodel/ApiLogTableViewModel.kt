package com.jfprojects.remotecare.common.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.jfprojects.remotecare.database.repository.ApiLogTableRepository

class ApiLogTableViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: ApiLogTableRepository? = null

    init {
        repository = ApiLogTableRepository(application)
    }

    fun getAllApiLogs() = repository?.getAllApiLogs()

    fun deleteAllApiLogTable() {
        repository?.deleteAllApiLogTable()
    }

}