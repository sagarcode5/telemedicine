package com.jfprojects.remotecare.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Set;

public class SharedPreference {
    private static final String MY_PREFERENCES = "RemoteCare_MY_PREFERENCES";
    private static final int MODE = Context.MODE_PRIVATE;
    private static SharedPreference pref;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;

    private SharedPreference() {
        sharedPreference = RemoteCareApp.Companion.getAppContext().getSharedPreferences(MY_PREFERENCES, MODE);
        editor = sharedPreference.edit();
    }

    public static SharedPreference getInstance() {
        if (pref == null) {
            pref = new SharedPreference();
        }
        return pref;
    }

    public String getString(String key) {
        return sharedPreference.getString(key, "");
    }

    public Set<String> getStringSet(String key) {
        return sharedPreference.getStringSet(key, null);
    }

    public void putString(String key, String value) {
        editor.putString(key, value).commit();
    }

    public void putStringSet(String key, Set<String> value) {
        editor.putStringSet(key, value).commit();
    }

    public void setObject(String key, Object object) {
        editor.putString(key, new Gson().toJson(object));
        editor.commit();
    }

    public <T> T getObject(String key, Class<T> a) {

        String strJson = sharedPreference.getString(key, null);
        if (TextUtils.isEmpty(strJson)) {
            return null;
        } else {
            try {
                return new Gson().fromJson(strJson, a);
            } catch (Exception e) {
                return null;
//                throw new IllegalArgumentException("Object stored with key " + key + " is instanceof other class");
            }
        }
    }

    public void setArrayList(String key, ArrayList a) {
        editor.putString(key, new Gson().toJson(a));
        editor.commit();
    }

//    public ArrayList<FilterModel> getFilterList(String key) {
//        Gson gson = new Gson();
//        ArrayList<FilterModel> productFromShared;
//        String jsonPreferences = sharedPreference.getString(key, null);
//
//        Type type = new TypeToken<ArrayList<FilterModel>>() {
//        }.getType();
//        productFromShared = gson.fromJson(jsonPreferences, type);
//
//        return productFromShared;
//    }

    public <T> T getArrayList(String key) {

        String strJson = sharedPreference.getString(key, null);
        Type type = new TypeToken<ArrayList<T>>() {
        }.getType();
        if (TextUtils.isEmpty(strJson)) {
            return new Gson().fromJson("", type);
        } else {
            try {
                return new Gson().fromJson(strJson, type);
            } catch (Exception e) {
                return new Gson().fromJson("", type);
            }
        }
    }

    public int getInt(String key) {
        return sharedPreference.getInt(key, 0);
    }

    public void putInt(String key, int value) {
        editor.putInt(key, value).commit();
    }


    public long getLong(String key) {
        return sharedPreference.getLong(key, 0l);
    }


    public void putLong(String key, long value) {
        editor.putLong(key, value).commit();
    }


    public float getFloat(String key) {
        return sharedPreference.getFloat(key, 0.0f);
    }


    public void putFloat(String key, float value) {
        editor.putFloat(key, value).commit();
    }

    public boolean getBoolean(String key) {
        return sharedPreference.getBoolean(key, false);
    }


    public void putBoolean(String key, boolean value) {
        editor.putBoolean(key, value).commit();
    }


    public boolean contains(String key) {
        return sharedPreference.contains(key);
    }

    public void remove(String key) {
        editor.remove(key).commit();
    }

}
