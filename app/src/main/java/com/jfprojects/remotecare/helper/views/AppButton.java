package com.jfprojects.remotecare.helper.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

import com.jfprojects.remotecare.R;

public class AppButton extends AppCompatButton {

    public AppButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        handleStyleable(context, attrs);
    }

    public AppButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        handleStyleable(context, attrs);
    }

    private void handleStyleable(Context context, AttributeSet attrs) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.fontTxt);
        FONT_VAL font_val = FONT_VAL.NONE;
        try {
            for (FONT_VAL mode : FONT_VAL.values()) {
                if (ta.getInt(R.styleable.fontTxt_txt_font, 0) == mode.getId()) {
                    font_val = mode;
                    break;
                }
            }
//            if (font_val == FONT_VAL.REGULAR) {
//                setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/normal_400.ttf"));
//            } else if (font_val == FONT_VAL.BOLD) {
//                setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/normal_600.ttf"));
//            } else if (font_val == FONT_VAL.LIGHT) {
//                setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/normal_300.ttf"));
//            } else if (font_val == FONT_VAL.ITALIC) {
//                setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/italic300.ttf"));
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public enum FONT_VAL {
        REGULAR(0), BOLD(1), LIGHT(2), ITALIC(3), NONE(4);
        private final int ID;

        FONT_VAL(final int id) {
            this.ID = id;
        }

        public int getId() {
            return ID;
        }
    }

}