package com.jfprojects.remotecare.helper

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.util.Log
import com.jfprojects.remotecare.BuildConfig
import java.text.SimpleDateFormat
import java.util.*


class SharedPrefConstants {
    companion object {
        const val KEY_VALUE_LANG = "lang"
        const val KEY_VALUE_IS_LANG_CHANGED = "is_lang_changed"
        const val KEY_VALUE_MOBILE = "mobile"
        const val KEY_VALUE_Questions = "questions"
        const val KEY_VALUE_USER_LOGGED_IN = "user_logged_in"
        const val KEY_VALUE_USER_PROFILE_READY = "profile_ready"
    }
}

class APIConstants {
    companion object {
            const val GOOGLE_API_KEY = "AIzaSyCibU_pXruCZxkPVjlCzH9ExdDME-RC3tk"
        const val BASE_URL = "http://192.168.5.85:9000/crm-new-version/"

        const val ERR = "err"

        const val symptoms_url = "symptomsQuestion"
        const val userResponse_url = "userResponse"
        const val refId_query = "refid"

        const val getAllQuestions = "getAllQuestions"
        const val getAllQuestionsByOptionId = "getAllQuestionsByOptionId"
        const val updateQuestionsToServer = "updateQuestionsToServer"
    }
}


class Params {
    companion object {
        const val DATA = "data"
        const val APPOINTMENT_ID = "appointmentId"
        const val ITEMS = "items"
        const val KEY_VALUE_USER_LOGGED_IN = "user_logged_in"
        const val KEY_VALUE_USER_PROFILE_READY = "profile_ready"
    }
}

class QuestionAIInteractionType {
    companion object {
        const val TYPE_AIQuestion = "AIQuestion"
        const val TYPE_AIOption = "AIOption"
        const val TYPE_UserAnswer = "UserAnswer"
    }
}

class RequestCode {
    companion object {
        const val Labs = 1
        const val Gallery = 2
        const val TakePicture = 3
        const val Patients = 4
        const val LabTestType = 5
        const val Pharmacy = 6
        const val Hospital = 7
        const val HealthCare = 8
        const val Department = 9

        const val COUNTRY = 10

        const val HealthCareTag = "HealthCare"
        const val DepartmentTag = "Department"
        const val HospitalTag = "Hospital"
        const val LabsTag = "Labs"
        const val PharmacyTag = "Pharmacy"
    }
}

class AppointmentDetailStatusType {
    companion object {
        const val TYPE_AIStatus = 1 // AI status provided
        const val TYPE_AIOption_Single = 2 // single selection from the answer options
        const val TYPE_AIOption_Multiple = 3 // multiple selection from the the answer options
        const val TYPE_UserAnswer = 4 // User answer to be displayed
    }
}

/*
* API Urls credentials
* */

//SharedPreference Keys

const val KEY_VALUE_IS_USER_LOGGED_IN = "is_user_logged_in"
const val KEY_VALUE_NAME = "name"
const val KEY_VALUE_EMAIL = "email"
const val KEY_VALUE_MOBILE = "mobile"

const val KEY_VALUE_USER_TYPE = "user_type"


//Navigation PARAM Keys

const val KEY_DATA = "data"

/*
* Professional
* */
const val KEY_VALUE_USER_PROFESSIONAL = "user_object_professional"
const val KEY_VALUE_USER_TYPE_PROFESSIONAL = 2

const val KEY_VALUE_IS_FILTER_APPLIED = "is_filter_applied"
const val KEY_VALUE_SELECTED_DISTANCE = "selected_distance"
const val KEY_VALUE_FILTER_LIST = "filter_list"

const val PARAMS_OPEN_FROM = "open_from"
const val PARAMS_OPEN_FROM_REG = "open_from_reg"
const val PARAMS_OPEN_FROM_PROFILE = "open_from_profile"


/*
* Service Provider
* */
const val KEY_VALUE_USER_SERVICE_PROVIDER = "user_object_service_provider"
const val KEY_VALUE_USER_TYPE_SERVICE_PROVIDER = 1
const val KEY_POST_NEW_JOB = "post_new_job"

const val KEY_VIEW_TYPE_PROFESSIONAL = "view_type_professional"
const val KEY_VIEW_TYPE_SERVICE_PROVIDER = "view_type_service_provider"

const val KEY_VALUE_USER_PROFESSIONAL_FOR_PROFILE_REVIEW =
    "user_object_professional_for_profile_review"

fun logError(tag: String, msg: String) {
    Log.e(tag, msg)
}

fun logError(
    tag: String,
    msg: String,
    e: Exception
) {
    Log.e(tag, msg, e)
}

fun logInformation(tag: String, msg: String) {
    Log.i(tag, msg)
}

fun logDebug(tag: String, msg: String) {
    Log.d(tag, msg)
}

fun log(tag: String, msg: String) {
    Log.e(tag, msg)
}

fun isConnected(): Boolean {
    val connectivityManager =
        RemoteCareApp.appContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val ni = connectivityManager.activeNetworkInfo
    return ni != null && ni.isAvailable && ni.isConnected
}

fun shareMyApp(activity: Activity) {
    try {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "My application name")
        var shareMessage = "\nLet me recommend you this application\n\n"
        shareMessage =
            "${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}".trimIndent()
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
        activity.startActivity(
            Intent.createChooser(
                shareIntent,
                "choose one"
            )
        )
    } catch (e: java.lang.Exception) {
    }
}

fun generateString(length: Int): String {
    val rng = Random()
    val characters = "abcdefghijklmnopqrstuvwxyz"
    val text = CharArray(length)
    for (i in 0 until length) {
        text[i] = characters[rng.nextInt(characters.length)]
    }
    return String(text)
}

fun generateWords(length: Int): String {
    val rng = Random()
    val text: StringBuilder = StringBuilder()
    for (i in 0 until length) {
        if (text.isEmpty())
            text.append(generateString(rng.nextInt(10) + 2))
        else text.append(" ").append(generateString(rng.nextInt(10) + 2))
    }
    return text.toString()
}

fun generateData(length: Int): String {
    val rng = Random()
    val text: StringBuilder = StringBuilder()
    return if (length == 0)
        text.append(generateWords(rng.nextInt(50) + 2)).toString()
    else
        text.append(generateWords(rng.nextInt(length) + 2)).toString()
}


fun generateInt(): Int {
    val rng = Random()
    val characters = "1234"
    return rng.nextInt(characters.length)
}

/*
* Date time formats for SwyChat Module
* */
val dateTimeFormat_date_time: SimpleDateFormat =
    SimpleDateFormat("dd MMM, yyyy HH:mm a", Locale.ENGLISH)
val dateTimeFormat_time_dd: SimpleDateFormat = SimpleDateFormat("dd", Locale.ENGLISH)
val dateTimeFormat_time_mm: SimpleDateFormat = SimpleDateFormat("MM", Locale.ENGLISH)
val dateTimeFormat_time_yy: SimpleDateFormat = SimpleDateFormat("yyyy", Locale.ENGLISH)

val dateTimeFormat_date_month: SimpleDateFormat = SimpleDateFormat("dd MMM", Locale.ENGLISH)
val dateTimeFormat_date: SimpleDateFormat = SimpleDateFormat("dd MMM, yyyy", Locale.ENGLISH)

val dateTimeFormat_time: SimpleDateFormat = SimpleDateFormat("hh:mm a", Locale.ENGLISH)


/*
    1,else  -- dd MMM, yyyy
    2 -- dd MMM
    3 -- hh:mm a
    4 -- dd MMM, yyyy HH:mm a
    5 --
    6 --
    7 -- dd
    8 -- MM
    9 -- yyyy
    10--
*/
fun getFullDate(timeStamp: Long, type: Int): String? {
    return try {
        val netDate = Date(timeStamp)
        return when (type) {
            1 -> {
                dateTimeFormat_date.timeZone = TimeZone.getDefault()
                dateTimeFormat_date.format(netDate)
            }
            2 -> {
                dateTimeFormat_date_month.timeZone = TimeZone.getDefault()
                dateTimeFormat_date_month.format(netDate)
            }

            3 -> {
                dateTimeFormat_time.timeZone = TimeZone.getDefault()
                dateTimeFormat_time.format(netDate)
            }
            4 -> {
                dateTimeFormat_date_time.timeZone = TimeZone.getDefault()
                dateTimeFormat_date_time.format(netDate)
            }
            7 -> {
                dateTimeFormat_time_dd.timeZone = TimeZone.getDefault()
                dateTimeFormat_time_dd.format(netDate)
            }
            8 -> {
                dateTimeFormat_time_mm.timeZone = TimeZone.getDefault()
                dateTimeFormat_time_mm.format(netDate)
            }
            9 -> {
                dateTimeFormat_time_yy.timeZone = TimeZone.getDefault()
                dateTimeFormat_time_yy.format(netDate)
            }
            else -> {
                dateTimeFormat_date.timeZone = TimeZone.getDefault()
                dateTimeFormat_date.format(netDate)
            }
        }
    } catch (ex: java.lang.Exception) {
        "xx"
    }

}


