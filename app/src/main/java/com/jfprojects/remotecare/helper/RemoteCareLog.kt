package com.jfprojects.remotecare.helper

import android.util.Log

class RemoteCareLog {

    fun logError(tag: String, msg: String) {
        Log.e(tag, msg)
    }

    fun logError(
        tag: String,
        msg: String,
        e: Exception
    ) {
        Log.e(tag, msg, e)
    }

    fun logInformation(tag: String, msg: String) {
        Log.i(tag, msg)
    }

    fun logDebug(tag: String, msg: String) {
        Log.d(tag, msg)
    }

    fun log(tag: String, msg: String) {
        Log.e(tag, msg)
    }
}