package com.jfprojects.remotecare.helper

import android.app.Application
import android.content.Context
import net.danlew.android.joda.JodaTimeAndroid

@Suppress("UNUSED_PARAMETER")
class RemoteCareApp : Application() {

    companion object {

        var appContext: Context? = null

    }

    override fun onCreate() {
        super.onCreate()
        appContext = this
        JodaTimeAndroid.init(this)

//        Fabric.with(this, Crashlytics())

    }


}