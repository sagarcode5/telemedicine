package com.jfprojects.remotecare.helper.views;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatRadioButton;

public class AppRadioButton extends AppCompatRadioButton {

    public AppRadioButton(Context context) {
        super(context);
        init(context);
    }

    public AppRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AppRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    public void init(Context context) {
//        try {
//            Typeface myFont = Typeface.createFromAsset(getContext().getAssets(), "font/lato_regular.ttf");
//
//            setTypeface(myFont);
//        } catch (Exception e) {
//            AllyLog.logError("Tag", e.toString());
//        }
    }
}
