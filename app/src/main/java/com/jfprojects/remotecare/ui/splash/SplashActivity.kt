package com.jfprojects.remotecare.ui.splash

import android.os.Bundle
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.common.activity.ParentActivity

class SplashActivity : ParentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
    }
}
