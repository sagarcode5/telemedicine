package com.jfprojects.remotecare.ui.main.fragment

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.DefaultList
import com.jfprojects.remotecare.helper.SharedPrefConstants
import com.jfprojects.remotecare.helper.SharedPreference
import com.jfprojects.remotecare.ui.main.adapters.LanguageListAdapter
import kotlinx.android.synthetic.main.fragment_select_language.*

class SelectLanguageFragment : BottomSheetDialogFragment() {
    private lateinit var contentView: View
    private var selectedLang: String? = null
    private var languageListAdapter: LanguageListAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        contentView = inflater.inflate(R.layout.fragment_select_language, container, false)
        return contentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectedLang = SharedPreference.getInstance().getString(SharedPrefConstants.KEY_VALUE_LANG)
        listRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
//        dialog?.setCanceledOnTouchOutside(false)

        getLabTests(
            arrayListOf(
                DefaultList(
                    title = "English",
                    isSelected = SharedPreference.getInstance().getString("lang") == "en",
                    message = "en"
                ),
                DefaultList(
                    title = "Hindi",
                    isSelected = SharedPreference.getInstance().getString("lang") == "hi",
                    message = "hi"
                )
            )
        )

        nextTV.setOnClickListener {
            Handler().postDelayed({
                if (selectedLang != SharedPreference.getInstance()
                        .getString(SharedPrefConstants.KEY_VALUE_LANG)
                ) {
                    SharedPreference.getInstance()
                        .putString(SharedPrefConstants.KEY_VALUE_LANG, selectedLang)
//                    dismiss()
                    requireActivity().recreate()
                    dismiss()
                } else dismiss()
            }, 300)


        }
    }

    private fun getLabTests(it: List<DefaultList>?) {
        if (it?.size!! > 0) {
            initAdapter(it as ArrayList<DefaultList>)
        } else {
//            noDataTV.visibility = View.VISIBLE
//            listRV.visibility = View.INVISIBLE
        }
    }

    private fun initAdapter(arrayList: ArrayList<DefaultList>) {
        if (languageListAdapter == null)
            languageListAdapter = object : LanguageListAdapter() {

                override fun onItemClick(
                    pos: Int,
                    item: DefaultList
                ) {
                    super.onItemClick(pos, item)
                    selectedLang = item.message
                    arrayList.forEach { it.isSelected = it.title == item.title }
                    notifyDataSetChanged()
                }
            }

        listRV.adapter = languageListAdapter
        Handler().postDelayed(
            {
                languageListAdapter!!.setData(arrayList)
            }
            , 100
        )
        listRV.visibility = View.VISIBLE
//        noDataTV.visibility = View.GONE
    }

}
