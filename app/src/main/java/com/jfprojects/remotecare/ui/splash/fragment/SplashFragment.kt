package com.jfprojects.remotecare.ui.splash.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.jfprojects.remotecare.R
import kotlinx.android.synthetic.main.fragment_splash.*

class SplashFragment : Fragment() {

    private var layouts: IntArray? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layouts = intArrayOf(
            R.layout.layout_1,
            R.layout.layout_2,
            R.layout.layout_3,
            R.layout.layout_4
        )

        viewPager.adapter = MyPagerAdapter(requireActivity(), layouts!!)
        viewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                addBottomDots(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })

        addBottomDots(0)
        loginBtn.setOnClickListener {
            findNavController().navigate(SplashFragmentDirections.actionNavigationSplashToNavigationMobile())
        }
        signUpBtn.setOnClickListener {
            findNavController().navigate(SplashFragmentDirections.actionNavigationSplashToNavigationSignUp())
        }
    }

    private fun addBottomDots(currentPage: Int) {
        val dots =
            arrayOfNulls<ImageView>(layouts!!.size)
        layoutDots!!.removeAllViews()
        for (i in dots.indices) {
            dots[i] = ImageView(activity)
            dots[i]!!.setImageResource(R.drawable.nonselecteditem_dot)
            dots[i]!!.setPadding(10, 10, 10, 0)
            layoutDots.addView(dots[i])
        }
        if (dots.isNotEmpty()) dots[currentPage]!!.setImageResource(R.drawable.selecteditem_dot)
    }

    class MyPagerAdapter(
        private val requireActivity: FragmentActivity,
        private val layouts: IntArray
    ) : PagerAdapter() {
        private var layoutInflater: LayoutInflater? = null
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            layoutInflater =
                requireActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?
            val view: View =
                layoutInflater!!.inflate(layouts[position], container, false)
            container.addView(view)
            return view
        }

        override fun getCount(): Int {
            return layouts.size
        }

        override fun isViewFromObject(
            view: View,
            `object`: Any
        ): Boolean {
            return view === `object`
        }

        override fun destroyItem(
            container: ViewGroup,
            position: Int,
            `object`: Any
        ) {
            val view = `object` as View
            container.removeView(view)
        }
    }
}
