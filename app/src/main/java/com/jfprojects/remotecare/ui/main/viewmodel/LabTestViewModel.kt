package com.jfprojects.remotecare.ui.main.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.jfprojects.remotecare.database.models.Appointment
import com.jfprojects.remotecare.database.models.LabTest
import com.jfprojects.remotecare.database.repository.AppointmentRepository
import com.jfprojects.remotecare.database.repository.LabTestsRepository

class LabTestViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: LabTestsRepository? = null
    private var appointmentRepository: AppointmentRepository? = null

    init {
        repository = LabTestsRepository(application)
        appointmentRepository = AppointmentRepository(application)
    }

    fun getLabTests() = repository?.getLabTests()
    fun getAppointment(id: Int) = appointmentRepository?.getAppointment(id)

    fun addLabTests(labTest: LabTest) = repository?.addLabTests(labTest)


//    fun deleteAppointment(id: Int) = repository?.deleteAppointment(id)
    fun updateAppointmentStatus(appointment: Appointment) =
    appointmentRepository?.updateAppointment(appointment)
//
//    fun addAppointments(appointment: ArrayList<Appointment>) =
//        repository?.addAppointments(appointment, 1)


}