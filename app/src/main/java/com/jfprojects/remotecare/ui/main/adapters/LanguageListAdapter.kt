package com.jfprojects.remotecare.ui.main.adapters

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.DefaultList
import kotlinx.android.synthetic.main.single_item_test_item.view.*

open class LanguageListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var stringList: ArrayList<DefaultList> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        // create a new view
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.single_item_language_item, parent, false)
        return MyViewHolder(v)
    }

    open fun onItemClick(
        pos: Int,
        item: DefaultList
    ) {

    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        (viewHolder as MyViewHolder).bind(stringList[i])
    }

    override fun getItemCount(): Int {
        return stringList.size
    }

    fun setData(list: ArrayList<DefaultList>) {
        stringList = ArrayList()
        stringList = list
        notifyDataSetChanged()
    }

    private inner class MyViewHolder(v: View) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: DefaultList) = with(itemView) {
            setOnClickListener {
                onItemClick(adapterPosition,item)
            }

            titleTV.alpha = if (item.isSelected) 1.0f else 0.6f
            titleTV.typeface = if (item.isSelected) Typeface.DEFAULT_BOLD else Typeface.DEFAULT

            titleTV.text = if (item.title.isNullOrEmpty()) "" else item.title
        }

    }

}