package com.jfprojects.remotecare.ui.main.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.jfprojects.remotecare.database.models.User
import com.jfprojects.remotecare.database.repository.UserRepository
import com.jfprojects.remotecare.helper.getFullDate
import org.joda.time.DateTime
import java.util.*

class UserViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: UserRepository? = null
    var user = User()
    var selectedGender: String = "Male"
    var selectedDob: DateTime = DateTime.now()

    init {
        repository = UserRepository(application)
    }

    fun setSelectedDate(date: Date) {

        selectedDob = selectedDob.withYear(
            getFullDate(
                date.time,
                9
            )!!.toInt()
        )
        selectedDob =
            selectedDob.withMonthOfYear(
                getFullDate(
                    date.time,
                    8
                )!!.toInt()
            )
        selectedDob =
            selectedDob.withDayOfMonth(
                getFullDate(
                    date.time,
                    7
                )!!.toInt()
            )
        selectedDob =
            selectedDob.withHourOfDay(0)
        selectedDob =
            selectedDob.withMinuteOfHour(0)
        selectedDob =
            selectedDob.withSecondOfMinute(0)
    }

    fun getUsers() = repository?.getUsers()
    fun getUser() = repository?.getUser(user.id)

    fun addUser(user: User) {
        user.gender = selectedGender
        user.dob = selectedDob.millis
        repository?.addMember(user)
    }

    fun updateUser(user: User) {
        user.gender = selectedGender
        user.dob = selectedDob.millis
        repository?.updateMember(user)
    }


}