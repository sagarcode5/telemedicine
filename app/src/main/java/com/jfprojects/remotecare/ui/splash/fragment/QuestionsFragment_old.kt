package com.jfprojects.remotecare.ui.splash.fragment

//
//class QuestionsFragment_old : AbsNoNavParentFragment() {
//    private var questionsListAdapter: QuestionsListAdapter? = null
//    private var questionList: ArrayList<DefaultList> = ArrayList()
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        return inflater.inflate(R.layout.fragment_questions, container, false)
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        questionRV.layoutManager =
//            SpeedyLinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
//        getAdapterList()
//    }
//
//    fun addValue(pos: Int, item: DefaultList) {
//        questionList[pos] =
//            DefaultList(title = item.title, type = QuestionAIInteractionType.TYPE_UserAnswer)
//        questionList.add(
//            DefaultList(title = generateData(20), type = QuestionAIInteractionType.TYPE_AIQuestion)
//        )
//        questionList.add(
//            DefaultList(title = generateData(3), type = QuestionAIInteractionType.TYPE_AIOption)
//        )
//
//        questionsListAdapter?.notifyDataSetChanged()
//        questionRV.smoothScrollToPosition(questionList.size - 1)
//    }
//
//    private fun initAdapter(list: ArrayList<DefaultList>) {
//        if (questionsListAdapter == null) {
//            questionsListAdapter = object : QuestionsListAdapter() {
//                override fun onActionClick(
//                    pos: Int,
//                    item: DefaultList
//                ) {
//                    super.onActionClick(pos, item)
//                    if (questionList.size < 5) {
//                        addValue(pos, item)
//                    } else {
//
//                        findNavController().navigate(QuestionsFragmentDirections.actionNavigationQuestionsToNavigationHome())
//                    }
//                }
//            }
//
//            questionRV.adapter = questionsListAdapter
//
//        }
//        Handler().postDelayed({
//            questionsListAdapter!!.setData(list)
//        }, 300)
//
//    }
//
//    private fun getAdapterList() {
//        questionList.add(
//            DefaultList(title = generateData(20), type = QuestionAIInteractionType.TYPE_AIQuestion)
//        )
//        questionList.add(
//            DefaultList(title = generateData(2), type = QuestionAIInteractionType.TYPE_AIOption)
//        )
//
//        initAdapter(questionList)
//
//    }
//
//}
