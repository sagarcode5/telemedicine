package com.jfprojects.remotecare.ui.main.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jfprojects.remotecare.helper.Params
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.SearchList
import com.jfprojects.remotecare.ui.main.adapters.SearchListAdapter
import kotlinx.android.synthetic.main.search_list_dialog.*

class SearchListDialog : DialogFragment() {

    var adapterNew: SearchListAdapter? = null
    private var searchListList: List<SearchList> = ArrayList()

    /*
     * 1- Default List for Doc, Hospital
     * */
    private var screenType = 0
    private var handler = Handler()
    private var workRunnable: Runnable? = null

    private fun initView(v: View?) {
        filter("", screenType)
    }

    // this method create view for your Dialog
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.search_list_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments != null) {
            screenType = requireArguments().getInt(Params.DATA)
            searchListList =
                (requireArguments().getSerializable(Params.ITEMS) as List<SearchList>)
        }
        recyclerView.layoutManager = LinearLayoutManager(activity)

        adapterNew = object : SearchListAdapter() {
            override fun onObjectClick(item: SearchList) {
                super.onObjectClick(item)
                val intent = Intent()
                intent.putExtra(Params.DATA, item)
                targetFragment!!.onActivityResult(
                    targetRequestCode,
                    Activity.RESULT_OK,
                    intent
                )
                dialog!!.dismiss()
            }
        }
        parentRL.visibility = View.VISIBLE
        updateUI(searchListList)
        recyclerView.adapter = adapterNew
        searchET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
            }

            override fun onTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
            }

            override fun afterTextChanged(editable: Editable) {
                //after the change calling the method and passing the search input
//                if (workRunnable != null)
//                    handler.removeCallbacks(workRunnable!!)

//                if (workRunnable == null) {
//                    workRunnable = Runnable { filter(editable.toString(), screenType) }
//                    handler.postDelayed(workRunnable!!, 800 /*delay*/)
//                }
                filter(editable.toString(), screenType)
            }
        })
        initView(view)
    }

    override fun onResume() {
        super.onResume()
        val window = dialog!!.window ?: return
        val params = window.attributes
        params.width = resources.getDimension(R.dimen._300sdp).toInt()
        params.height = resources.getDimension(R.dimen._350sdp).toInt()
        window.attributes = params
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return Dialog(requireActivity())
    }

//    private fun getCostCenterList(list: List<SearchList>) {
//        if (list.isNotEmpty()) {
//            updateUI(list)
//        }
//    }


    fun filter(text: String, viewType: Int) {
        //new array list that will hold the filtered data
        val filteredList = ArrayList<SearchList>()
        if (searchListList.isNotEmpty()) {

            //looping through existing elements
            for (str in searchListList) {
                //if the existing elements contains the search input
                if (viewType == str.viewType && str.defaultList!!.title!!.toLowerCase().contains(
                        text.toLowerCase()
                    )
                ) {
                    //adding the element to filtered list
                    filteredList.add(str)
                }
            }
        }

        //calling a method of the adapter class and passing the filtered list
        if (filteredList.size > 0) {
            Handler().postDelayed(
                { adapterNew?.setData(filteredList) }
                , 200
            )
            recyclerView.visibility = View.VISIBLE
        } else {
            recyclerView.visibility = View.GONE
        }
    }

    private fun updateUI(list: List<SearchList>) {
        if (list.isNotEmpty()) {
            adapterNew!!.setData(list as ArrayList<SearchList>)
        }
    }
}