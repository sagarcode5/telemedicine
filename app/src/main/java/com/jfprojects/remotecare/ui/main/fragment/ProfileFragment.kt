package com.jfprojects.remotecare.ui.main.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.common.fragment.AbsNavParentFragment
import com.jfprojects.remotecare.database.models.APILogTable
import com.jfprojects.remotecare.helper.RequestCode
import com.jfprojects.remotecare.helper.getFullDate
import com.jfprojects.remotecare.ui.main.viewmodel.UserViewModel
import de.synexs.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import kotlinx.android.synthetic.main.fragment_profile.*
import org.joda.time.DateTime
import java.util.*

class ProfileFragment : AbsNavParentFragment(), AdapterView.OnItemSelectedListener {
    private val userViewModel by viewModels<UserViewModel>()
    private lateinit var datePicker: SingleDateAndTimePickerDialog.Builder

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun apiLogSuccess(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun apiLogFailure(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        datePicker = SingleDateAndTimePickerDialog.Builder(context)

        val genderList = ArrayList<String>()
        resources.getStringArray(R.array.gender)
            .forEach { genderList.add(it) }
        initSpinners(genderSpinner, genderList)

        updateTV.setOnClickListener {
            validate()
        }

        countryET.setOnClickListener { startAutocompleteActivity() }

        dateTV.setOnClickListener {
            datePicker
                .displayAmPm(false)
                .defaultDate(userViewModel.selectedDob.toDate())
                .title(resources.getString(R.string.date_picker_title))
                .curved()
                .defaultDate(userViewModel.selectedDob.toDate())
                .minDateRange(DateTime.now().minusYears(100).toDate())
                .customLocale(Locale.getDefault())
                .displayDays(false)
                .displayDaysOfMonth(true)
                .displayMonth(true)
                .displayYears(true)
                .displayHours(false)
                .displayMinutes(false)
                .setTimeZone(Calendar.getInstance().timeZone)
                .mainColor(resources.getColor(R.color.colorPrimary))
                .titleTextColor(resources.getColor(R.color.white))
                .backgroundColor(resources.getColor(R.color.colorGrey))
                .listener { date: Date ->

                    userViewModel.setSelectedDate(date)
                    dateTV.text = getFullDate(date.time, 1)

                }.display()
        }
    }

    private fun initSpinners(spinner: Spinner, list: ArrayList<String>) {
        val titles = ArrayList<String?>()
        for (cat in list) {
            titles.add(cat)
        }
        val listAdapter: ArrayAdapter<String?> = object : ArrayAdapter<String?>(
            requireContext(),
            android.R.layout.simple_spinner_item,
            android.R.id.text1, titles
        ) {
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                var v: View? = null
                v = super.getDropDownView(position, null, parent)
                // If this is the selected item position
                if (position == spinner.selectedItemPosition) {
                    v.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGrey))
                } else {
                    // for other views
                    v.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
                }
                return v
            }
        }
        listAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        with(spinner) {
            adapter = listAdapter
            isSelected = false
            setSelection(0, true)

            onItemSelectedListener = this@ProfileFragment
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RequestCode.COUNTRY && data != null) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data)
                    Log.e(
                        "TAG",
                        "Place: " + place.name + ", " + place.id + ", " + place.address + ", " + place.latLng
                    )
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    val status =
                        Autocomplete.getStatusFromIntent(data)
                    Log.e(
                        "TAG",
                        if (status.statusMessage != null) status.statusMessage!! else ""
                    )
                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                    Log.e(
                        "TAG",
                        "Activity.RESULT_CANCELED"
                    )
                }
            }
        }
    }

    private fun startAutocompleteActivity() {
        val autocompleteIntent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.FULLSCREEN,
            listOf(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.ADDRESS,
                Place.Field.LAT_LNG
            )
        )
            .build(requireActivity())
        startActivityForResult(autocompleteIntent, RequestCode.COUNTRY)
    }


    private fun validate() {
        var isDataChanged = false
        val user = userViewModel.user

        if (TextUtils.isEmpty(firstNameET.text)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "First Name"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        if (TextUtils.isEmpty(lastNameET.text)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "Last Name"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        if (TextUtils.isEmpty(emailET.text)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "Email"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        if (TextUtils.isEmpty(mobileET.text)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "Mobile"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        if (TextUtils.isEmpty(aadharET.text)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "Aadhar"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        if (TextUtils.isEmpty(userViewModel.selectedGender)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "Gender"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        if (TextUtils.isEmpty(dateTV.text)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "DOB"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }

//        if (TextUtils.isEmpty(countryET.text)) {
//            Toast.makeText(
//                context,
//                String.format(resources.getString(R.string.profile_error_profile), "Country"),
//                Toast.LENGTH_SHORT
//            ).show()
//            return
//        }
//        if (TextUtils.isEmpty(cityET.text)) {
//            Toast.makeText(
//                context,
//                String.format(resources.getString(R.string.profile_error_profile), "City"),
//                Toast.LENGTH_SHORT
//            ).show()
//            return
//        }

        if (TextUtils.isEmpty(addressET.text)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "Address"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        if (!user.firstName.isNullOrEmpty() && user.firstName?.equals(firstNameET.text)!!) {

            isDataChanged = true
        }

        if (!user.lastName.isNullOrEmpty() && user.lastName?.equals(lastNameET.text)!!) {

            isDataChanged = true
        }

        if (!user.email.isNullOrEmpty() && user.email?.equals(emailET.text)!!) {

            isDataChanged = true
        }

        if (!user.mobile.isNullOrEmpty() && user.mobile?.equals(mobileET.text)!!) {

            isDataChanged = true
        }

        if (!user.aadhar.isNullOrEmpty() && user.aadhar?.equals(aadharET.text)!!) {

            isDataChanged = true
        }

        if (!user.gender.isNullOrEmpty() && user.gender?.equals(userViewModel.selectedGender)!!) {

            isDataChanged = true
        }

        if (user.dob == userViewModel.selectedDob.millis) {

            isDataChanged = true
        }

//        if (!user.country.isNullOrEmpty() && user.country?.equals(countryET.text)!!) {
//
//
//            isDataChanged = true
//        }
//
//        if (user.city?.equals(cityET.text)!!) {
//
//            isDataChanged = true
//        }

        if (!user.address.isNullOrEmpty() && user.address?.equals(addressET.text)!!) {

            isDataChanged = true
        }

        if (isDataChanged) {
            user.firstName = firstNameET.text.toString()
            user.lastName = lastNameET.text.toString()
            user.email = emailET.text.toString()
            user.mobile = mobileET.text.toString()
            user.aadhar = aadharET.text.toString()

//            user.country = countryET.text.toString()
//            user.city = cityET.text.toString()
            user.address = addressET.text.toString()

            userViewModel.addUser(user = user)

            findNavController().navigateUp()
        }

    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        when (p0?.id) {

            R.id.genderSpinner -> {
                userViewModel.selectedGender = genderSpinner.getItemAtPosition(p3.toInt()) as String
            }
        }
    }
}
