package com.jfprojects.remotecare.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.core.view.GravityCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import com.google.android.libraries.places.api.Places
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.common.activity.ParentActivity
import com.jfprojects.remotecare.helper.APIConstants
import com.jfprojects.remotecare.helper.shareMyApp
import com.jfprojects.remotecare.ui.main.fragment.SelectLanguageFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : ParentActivity() {

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        nav_host_fragment.childFragmentManager.fragments[0]
            .onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        nav_host_fragment.childFragmentManager.fragments[0]
            .onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        val navController = findNavController(R.id.nav_host_fragment)

        appBarConfiguration = AppBarConfiguration(
            setOf(

                R.id.navigation_home,
                R.id.navigation_appointment,
                R.id.navigation_lab_pharmacy,
                R.id.navigation_profile
            ),
            drawerDL
        )

        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration)
        nav_view.setupWithNavController(navController)
        navigation.setupWithNavController(navController)

        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, APIConstants.GOOGLE_API_KEY)
        }

    }

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onSupportNavigateUp(): Boolean {
        val navController = this.findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration)
    }

    override fun onBackPressed() {
        if (drawerDL.isDrawerOpen(GravityCompat.START)) {
            drawerDL.closeDrawer(GravityCompat.START)
        } else super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.navigation_share -> {
                shareMyApp(this)
                true
            }
            R.id.navigation_language -> {
                val addBottomDialogFragment = SelectLanguageFragment()
                addBottomDialogFragment.show(
                    supportFragmentManager,
                    "BottomSheet"
                )
//
//                if (!SharedPreference.getInstance().getBoolean("isHindiSelected")) {
//                    SharedPreference.getInstance().putBoolean("isHindiSelected", true)
//                    SharedPreference.getInstance().putString("lang", "hi")
//                    recreate()
////                    updateResources(this, "hi")
//                } else {
//                    SharedPreference.getInstance().putBoolean("isHindiSelected", false)
//                    SharedPreference.getInstance().putString("lang", "en")
//                    recreate()
//
////                    updateResources(this, "en")
//                }
//                Toast.makeText(applicationContext, "click on share", Toast.LENGTH_LONG).show()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
