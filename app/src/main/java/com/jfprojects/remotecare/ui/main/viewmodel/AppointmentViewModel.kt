package com.jfprojects.remotecare.ui.main.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.jfprojects.remotecare.database.models.Appointment
import com.jfprojects.remotecare.database.repository.AppointmentRepository

class AppointmentViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: AppointmentRepository? = null

    init {
        repository = AppointmentRepository(application)
    }

    fun getAppointments() = repository?.getAppointments()
    fun getAppointment(id: Int) = repository?.getAppointment(id)

    fun addAppointment(appointment: Appointment) = repository?.addAppointment(appointment)


    fun deleteAppointment(id: Int) = repository?.deleteAppointment(id)
    fun updateAppointmentStatus(appointment: Appointment) =
        repository?.updateAppointment(appointment)

    fun addAppointments(appointment: ArrayList<Appointment>) =
        repository?.addAppointments(appointment, 1)


}