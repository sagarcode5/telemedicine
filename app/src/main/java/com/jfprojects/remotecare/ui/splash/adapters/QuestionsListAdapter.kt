package com.jfprojects.remotecare.ui.splash.adapters

import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.db.*
import kotlinx.android.synthetic.main.single_item_question_ai_question_item.view.*
import java.util.*

open class QuestionsListAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var questionList: ArrayList<Question> = ArrayList()

    open fun onActionClick(
        pos: Int,
        item: Question
    ) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        return when (i) {
            1 -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.single_item_question_ai_question_item, parent, false)
                MyViewHolderAIQuestion(v)
            }
            else -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.single_item_question_user_answer_item, parent, false)
                MyViewHolderUserAnswer(v)
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return when (questionList[position].viewType) {
            QuestionViewType.AIQuestion.name -> 1
//            QuestionViewType.Answer.name -> 2
            else -> 3
        }
    }

    fun setData(list: ArrayList<Question>) {
        questionList = ArrayList()
        questionList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        when (getItemViewType(i)) {
            1 -> (viewHolder as MyViewHolderAIQuestion).bind(questionList[i])
//            2 -> (viewHolder as MyViewHolderUserAnswer).bind(questionList[i])
        }
    }

    override fun getItemCount(): Int {
        return questionList.size
    }

    private inner class MyViewHolderAIQuestion(
        v: View,
        var optionListAdapter: OptionListAdapter? = null
    ) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: Question) = with(itemView) {
            titleTV.text = item.symptomsQuestion.question

            if (item.isQuestionAnswered) {
                listRV.visibility = View.GONE
                answerCL.visibility = View.VISIBLE
                val productNameList: List<String?> = item.symptomOptions.map { it.name }
                answerTV.text = productNameList.joinToString()
            } else {
                val flexLayout = FlexboxLayoutManager(itemView.context)
                flexLayout.justifyContent = JustifyContent.FLEX_START
                listRV.layoutManager = flexLayout
                if (!item.symptomsQuestion.options.isNullOrEmpty() &&
                    !item.symptomsQuestion.options.contains(
                        SymptomOptions(name = ActionName.NoneOfTheAbove.str)
                    ) &&
                    item.symptomsQuestion.questionDisplayType == QuestionDisplayType.MultiSelect.str
                ) {
                    item.symptomsQuestion.options.add(
                        SymptomOptions(
                            questionId = item.symptomsQuestion.id,
                            name = ActionName.NoneOfTheAbove.str,
                            isSelected = false,
                            credit = 0
                        )
                    )
                }

                updateUI(item, listRV, item.symptomsQuestion.options)

                listRV.visibility = View.VISIBLE
                answerCL.visibility = View.GONE
            }
        }

        private fun updateUI(
            question: Question,
            recyclerView: RecyclerView,
            list: ArrayList<SymptomOptions>
        ) {
            val optionListAdapter: OptionListAdapter =
                object : OptionListAdapter() {
                    override fun addInSelection(item: SymptomOptions) {
                        super.addInSelection(item)
                        if (question.symptomsQuestion.questionDisplayType == QuestionDisplayType.MultiSelect.str) {
                            if (question.symptomOptions.isEmpty()) {
                                list.last().name = ActionName.Next.str
                                question.symptomOptions.add(item)
                            } else question.symptomOptions.add(item)
                            notifyDataSetChanged()
                        } else {
                            question.symptomOptions.add(item)
                            onActionClick(adapterPosition, question)
                        }
                    }

                    override fun removeInSelection(item: SymptomOptions) {
                        super.removeInSelection(item)
                        question.symptomOptions.remove(item)

                        if (question.symptomOptions.isEmpty())
                            list.last().name = ActionName.NoneOfTheAbove.str

                        notifyDataSetChanged()
                    }

                    override fun onNextClick(item: SymptomOptions) {
                        super.onNextClick(item)
                        if (item.name == ActionName.NoneOfTheAbove.str) {
                            question.symptomOptions.add(item)
                        }
                        onActionClick(adapterPosition, question)
                    }
                }

            recyclerView.adapter = optionListAdapter
            Handler().postDelayed(
                {
                    optionListAdapter.setData(list)
                }
                , 100
            )
            recyclerView.visibility = View.VISIBLE
        }

    }

    private inner class MyViewHolderUserAnswer(v: View) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: Question) = with(itemView) {
            val productNameList: List<String?> = item.symptomOptions.map { it.name }
            titleTV.text = productNameList.joinToString()
        }
    }

}