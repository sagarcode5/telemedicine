package com.jfprojects.remotecare.ui.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.FamilyMember
import com.jfprojects.remotecare.database.models.RelationType
import kotlinx.android.synthetic.main.single_item_family_member_item.view.*
import kotlinx.android.synthetic.main.single_item_lab_test_item.view.deleteCL
import java.util.*

open class FamilyMemberListAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var familyList: ArrayList<FamilyMember> = ArrayList()

    open fun onActionClick(
        item: FamilyMember
    ) {

    }

    open fun onObjectClick(
        item: FamilyMember
    ) {

    }

    open fun onStatusUpdate(
        item: FamilyMember
    ) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.single_item_family_member_item, parent, false)
        return MyViewHolder(v)
    }


    fun setData(list: ArrayList<FamilyMember>) {
        familyList = ArrayList()
        familyList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        (viewHolder as MyViewHolder).bind(familyList[i])
    }

    override fun getItemCount(): Int {
        return familyList.size
    }

    private inner class MyViewHolder(v: View) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: FamilyMember) = with(itemView) {

            itemView.setOnClickListener { onObjectClick(item) }
            memberNameTV.text = String.format("${item.firstName} ${item.lastName}")
            relationTV.text =
                if (item.relation.isNullOrEmpty()) "" else RelationType.valueOf(item.relation!!)
                    .toString()

            deleteCL.setOnClickListener { onActionClick(item) }
            itemView.setOnClickListener { onObjectClick(item) }
        }

    }

}