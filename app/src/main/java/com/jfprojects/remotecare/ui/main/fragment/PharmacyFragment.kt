package com.jfprojects.remotecare.ui.main.fragment

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.common.fragment.AbsNavParentFragment
import com.jfprojects.remotecare.database.models.APILogTable
import com.jfprojects.remotecare.database.models.PharmacyOrder
import com.jfprojects.remotecare.ui.main.adapters.PharmacyOrderListAdapter
import com.jfprojects.remotecare.ui.main.viewmodel.PharmacyViewModel
import kotlinx.android.synthetic.main.fragment_lab_tests.listRV
import kotlinx.android.synthetic.main.fragment_lab_tests.noDataTV
import kotlinx.android.synthetic.main.fragment_pharmacy.*
import java.util.*

class PharmacyFragment : AbsNavParentFragment() {
    private var pharmacyViewModel: PharmacyViewModel? = null
    private var pharmacyOrderListAdapter: PharmacyOrderListAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pharmacy, container, false)
    }

    override fun apiLogSuccess(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun apiLogFailure(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pharmacyViewModel = ViewModelProviders.of(this).get(PharmacyViewModel::class.java)

        pharmacyViewModel?.getPharmacyOrders()
            ?.observe(viewLifecycleOwner, Observer { getPharmacyOrders(it) })

        listRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
//        bookNewAppointmentTV.visibility = View.VISIBLE

        addNewOrderTV.setOnClickListener {
            findNavController().navigate(LabPharmacyFragmentDirections.actionNavigationLabPharmacyToNavigationAddNewPharmacyOrder())
        }
    }


    private fun getPharmacyOrders(it: List<PharmacyOrder?>?) {
        if (it?.size!! > 0) {
            initPharmacyOrderAdapter(it as ArrayList<PharmacyOrder>)
        } else {
            noDataTV.visibility = View.VISIBLE
            listRV.visibility = View.INVISIBLE
        }
    }

    private fun initPharmacyOrderAdapter(arrayList: ArrayList<PharmacyOrder>) {
        if (pharmacyOrderListAdapter == null)
            pharmacyOrderListAdapter = object : PharmacyOrderListAdapter() {

                override fun onObjectClick(item: PharmacyOrder) {
                    super.onObjectClick(item)
//                    findNavController().navigate(
//                        AppointmentFragmentDirections.actionNavigationAppointmentToNavigationSingleAppointment(
//                            data = item.id
//                        )
//                    )
                }
            }

        listRV.adapter = pharmacyOrderListAdapter
        Handler().postDelayed(
            {
                pharmacyOrderListAdapter!!.setData(arrayList)
            }
            , 100
        )
        listRV.visibility = View.VISIBLE
        noDataTV.visibility = View.GONE
    }


}
