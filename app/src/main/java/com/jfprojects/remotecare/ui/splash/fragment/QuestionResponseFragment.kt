package com.jfprojects.remotecare.ui.splash.fragment

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jfprojects.remotecare.helper.Params
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.response.QuestionFinalResponse
import kotlinx.android.synthetic.main.single_item_questions_response.*

class QuestionResponseFragment : BottomSheetDialogFragment() {
    //    ViewTreeObserver.OnGlobalLayoutListener {
    private lateinit var contentView: View

    private var questionFinalResponse: QuestionFinalResponse? = null
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        contentView = inflater.inflate(R.layout.single_item_questions_response, container, false)
        return contentView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (requireView().parent as View).setBackgroundColor(Color.TRANSPARENT)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (requireArguments().containsKey(Params.DATA)) {
            questionFinalResponse = requireArguments().get(Params.DATA) as QuestionFinalResponse
        }
//        dialog?.window?.decorView?.findViewById<View>(R.id.design_bottom_sheet)
//            ?.setBackgroundResource(android.R.color.transparent)
        dialog?.setCanceledOnTouchOutside(false)
//        dialog?.setOnShowListener {
//            val dialog = it as BottomSheetDialog
//            val bottomSheet = dialog.findViewById<View>(R.id.design_bottom_sheet)
//            bottomSheet?.let { sheet ->
//                dialog.behavior.peekHeight = sheet.height
//                sheet.parent.parent.requestLayout()
//            }
//        }
        skipTV.setOnClickListener {
            dismiss()

        }

//        bottomSheetBehavior = BottomSheetBehavior.from(contentView.parent as View)
//        bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
//            override fun onSlide(bottomSheet: View, slideOffset: Float) {
//            }
//
//            override fun onStateChanged(bottomSheet: View, newState: Int) {
//                if (newState == BottomSheetBehavior.STATE_DRAGGING) bottomSheetBehavior.state =
//                    BottomSheetBehavior.STATE_EXPANDED
//            }
//
//        })
//        handleGlobalLayoutListener()

        this.bottomSheetBehavior = BottomSheetBehavior.from(questionResponseBS)
        this.bottomSheetBehavior.state = STATE_EXPANDED
        this.bottomSheetBehavior.isDraggable = false // disable dragging

        initViews()
    }

//    private fun handleGlobalLayoutListener() {
//        contentView.viewTreeObserver.addOnGlobalLayoutListener(this)
//    }

//    @SuppressLint("RestrictedApi")
//    override fun setupDialog(dialog: Dialog, style: Int) {
//        super.setupDialog(dialog, style)
//        val bottomSheetDialog = dialog as BottomSheetDialog
//        contentView = View.inflate(context, R.layout.single_item_questions_response, null)
//        bottomSheetDialog.setContentView(contentView)
//        try {
//            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            dialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
//            dialog.setCanceledOnTouchOutside(false)

//        } catch (e: Exception) {
//
//        }
//    }


    private fun initViews() {
        if (questionFinalResponse != null) {

            constraintLayout.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.bg_corner_top)
            val drawable = constraintLayout.background as GradientDrawable

            when (questionFinalResponse!!.colorStatus) {
                1 -> {
                    drawable.setColor(ContextCompat.getColor(requireContext(), R.color.colorUnsafe))
                }
                2 -> {
                    drawable.setColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.colorModerate
                        )
                    )
                }
                3 -> {
                    drawable.setColor(ContextCompat.getColor(requireContext(), R.color.colorSafe))

                }
            }

            constraintLayout.background = drawable

            healthStatusTV.text = questionFinalResponse!!.message
            advisoryTV.text = questionFinalResponse!!.suggestion

        }
    }


//    override fun onGlobalLayout() {
////        val rect = Rect()
////        contentView.getWindowVisibleDisplayFrame(rect)
////        val screenHeight = contentView.rootView.height
////        val heightDifference = screenHeight - (rect.bottom - rect.top)
////        bottomSheetBehavior.peekHeight = screenHeight + heightDifference
////        contentView.viewTreeObserver.removeOnGlobalLayoutListener(this)
//    }
}
