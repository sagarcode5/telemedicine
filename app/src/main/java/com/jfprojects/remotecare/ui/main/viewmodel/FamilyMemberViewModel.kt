package com.jfprojects.remotecare.ui.main.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.jfprojects.remotecare.database.models.FamilyMember
import com.jfprojects.remotecare.database.models.RelationType
import com.jfprojects.remotecare.database.repository.FamilyMembersRepository
import com.jfprojects.remotecare.helper.getFullDate
import org.joda.time.DateTime
import java.util.*

class FamilyMemberViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: FamilyMembersRepository? = null
    var selectedGender: String = "Male"
    var selectedRelation: String = RelationType.Spouse.name
    var selectedDob: DateTime = DateTime.now()
    var familyMember = FamilyMember()

    init {
        repository = FamilyMembersRepository(application)
    }

    fun setSelectedDate(date: Date) {

        selectedDob = selectedDob.withYear(
            getFullDate(
                date.time,
                9
            )!!.toInt()
        ).withMonthOfYear(
            getFullDate(
                date.time,
                8
            )!!.toInt()
        ).withDayOfMonth(
            getFullDate(
                date.time,
                7
            )!!.toInt()
        ).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0)
    }

    fun getFamilyMembers() = repository?.getFamilyMembers()
    fun getFamilyMember() = repository?.getFamilyMember(familyMember.id)

    fun addFamilyMembers(familyMember: FamilyMember) {
        familyMember.gender = selectedGender
        familyMember.relation = selectedRelation
        familyMember.dob = selectedDob.millis
        repository?.addMember(familyMember)
    }

    fun updateFamilyMember(familyMember: FamilyMember) {
        familyMember.gender = selectedGender
        familyMember.relation = selectedRelation
        familyMember.dob = selectedDob.millis
        repository?.updateMember(familyMember)
    }


//    fun deleteAppointment(id: Int) = repository?.deleteAppointment(id)
//    fun updateAppointmentStatus(appointment: Appointment) =
//    appointmentRepository?.updateAppointment(appointment)
//
//    fun addAppointments(appointment: ArrayList<Appointment>) =
//        repository?.addAppointments(appointment, 1)


}