package com.jfprojects.remotecare.ui.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jfprojects.remotecare.helper.getFullDate
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.LabTest
import kotlinx.android.synthetic.main.single_item_lab_test_item.view.*
import org.joda.time.DateTime
import java.util.*

open class LabTestListAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var labTestList: ArrayList<LabTest> = ArrayList()

    open fun onActionClick(
        item: LabTest
    ) {

    }

    open fun onObjectClick(
        item: LabTest
    ) {

    }

    open fun onStatusUpdate(
        item: LabTest
    ) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.single_item_lab_test_item, parent, false)
        return MyViewHolder(v)
    }


    fun setData(list: ArrayList<LabTest>) {
        labTestList = ArrayList()
        labTestList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        (viewHolder as MyViewHolder).bind(labTestList[i])
    }

    override fun getItemCount(): Int {
        return labTestList.size
    }

    private inner class MyViewHolder(v: View) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: LabTest) = with(itemView) {

            itemView.setOnClickListener { onObjectClick(item) }
            labNameTV.text = item.labName
            typeOfLabTestTV.text = when (item.testReportStatus) {
                1 -> itemView.resources.getString(R.string.lab_test_list_type_home_collection)
                else -> itemView.resources.getString(R.string.lab_test_list_type_visit_lab)
            }

            /*
            * 1- tests order placed
            * 2- samples taken, waiting for results
            * 3- reports provided
            *
            * */
            statusTV.apply {
                when (item.testReportStatus) {
                    1 -> {
                        text =
                            itemView.resources.getString(R.string.lab_test_list_status_test_booked)
                        setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorResponseReceived
                            )
                        )
                    }

                    2 -> {
                        text =
                            itemView.resources.getString(R.string.lab_test_list_status_waiting_for_report)
                        setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorWaitingForResponse
                            )
                        )
                    }

                    else -> {
                        text =
                            itemView.resources.getString(R.string.lab_test_list_status_reports_uploaded)
                        setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorResponseReceived
                            )
                        )
                    }
                }
            }
            var dateTime = DateTime.now()
            dateTime = dateTime.withMillis(item.dateTime!!)

            timeTV.visibility = View.GONE
            dateTV.text = getFullDate(dateTime.millis, 2)
//            timeTV.text = getFullDate(dateTime.millis, 3)
            tesNamesTV.text = item.testsName!!.joinToString(separator = ",")

            deleteCL.setOnClickListener { onActionClick(item) }
            imageView3.setOnClickListener { onStatusUpdate(item) }
        }

    }

}