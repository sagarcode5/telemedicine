package com.jfprojects.remotecare.ui.main.fragment

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.common.fragment.AbsNavParentFragment
import com.jfprojects.remotecare.database.models.APILogTable
import com.jfprojects.remotecare.database.models.LabTest
import com.jfprojects.remotecare.ui.main.adapters.LabTestListAdapter
import com.jfprojects.remotecare.ui.main.viewmodel.LabTestViewModel
import kotlinx.android.synthetic.main.fragment_lab_tests.*

class LabTestsFragment : AbsNavParentFragment() {
    private var labTestViewModel: LabTestViewModel? = null
    private var labTestListAdapter: LabTestListAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_lab_tests, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        labTestViewModel = ViewModelProviders.of(this).get(LabTestViewModel::class.java)

        labTestViewModel?.getLabTests()
            ?.observe(viewLifecycleOwner, Observer { getLabTests(it) })

        listRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        bookTestTV.setOnClickListener {
            findNavController().navigate(LabPharmacyFragmentDirections.actionNavigationLabPharmacyToNavigationAddNewTest())
        }
    }

    override fun apiLogSuccess(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun apiLogFailure(table: APILogTable?) {
        TODO("Not yet implemented")
    }


    private fun getLabTests(it: List<LabTest?>?) {
        if (it?.size!! > 0) {
            initLabTestAdapter(it as ArrayList<LabTest>)
        } else {
            noDataTV.visibility = View.VISIBLE
            listRV.visibility = View.INVISIBLE
        }
    }

    private fun initLabTestAdapter(arrayList: ArrayList<LabTest>) {
        if (labTestListAdapter == null)
            labTestListAdapter = object : LabTestListAdapter() {

                override fun onObjectClick(item: LabTest) {
                    super.onObjectClick(item)
//                    findNavController().navigate(
//                        AppointmentFragmentDirections.actionNavigationAppointmentToNavigationSingleAppointment(
//                            data = item.id
//                        )
//                    )
                }
            }

        listRV.adapter = labTestListAdapter
        Handler().postDelayed(
            {
                labTestListAdapter!!.setData(arrayList)
            }
            , 100
        )
        listRV.visibility = View.VISIBLE
        noDataTV.visibility = View.GONE
    }


}
