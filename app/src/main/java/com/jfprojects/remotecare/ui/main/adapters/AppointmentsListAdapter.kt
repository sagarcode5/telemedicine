package com.jfprojects.remotecare.ui.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jfprojects.remotecare.helper.getFullDate
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.Appointment
import kotlinx.android.synthetic.main.single_item_appointment_item.view.*
import org.joda.time.DateTime
import java.util.*

open class AppointmentsListAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var appointmentList: ArrayList<Appointment> = ArrayList()

    open fun onActionClick(
        item: Appointment
    ) {

    }

    open fun onObjectClick(
        item: Appointment
    ) {

    }

    open fun onStatusUpdate(
        item: Appointment
    ) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.single_item_appointment_item, parent, false)
        return MyViewHolder(v)
    }


    fun setData(list: ArrayList<Appointment>) {
        appointmentList = ArrayList()
        appointmentList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        (viewHolder as MyViewHolder).bind(appointmentList[i])
    }

    override fun getItemCount(): Int {
        return appointmentList.size
    }

    private inner class MyViewHolder(v: View) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: Appointment) = with(itemView) {

            itemView.setOnClickListener { onObjectClick(item) }
            docNameTV.text = item.professionalName
            typeOfAppointmentTV.text = when (item.appointmentType) {
                1 -> itemView.resources.getString(R.string.appointment_list_time_appointment_type_telehealth)
                2 -> itemView.resources.getString(R.string.appointment_list_time_appointment_type_follow_up_telehealth)
                3 -> itemView.resources.getString(R.string.appointment_list_time_appointment_type_video)
                4 -> itemView.resources.getString(R.string.appointment_list_time_appointment_type_home_care)
                else -> itemView.resources.getString(R.string.appointment_list_time_appointment_type_other)
            }

            /*
            * 1- appointment Fixed
            * 2- waiting for doctor Prescription
            * 3- Doctor Uploaded Prescription
            * 4- Testing Lab Appointment Fixed
            * 5- Samples for test sent
            * 6- Tests Done
            * 7- Pharmacy ordered
            * 8- Self- Assessment Pending
            *
            * */
            statusTV.apply {
                when (item.appointmentStatus) {
                    1 -> {
                        text =
                            itemView.resources.getString(R.string.appointment_list_status_appointment_fixed)
                        setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorResponseReceived
                            )
                        )
                    }

                    2 -> {
                        text =
                            itemView.resources.getString(R.string.appointment_list_status_waiting_for_doc)
                        setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorWaitingForResponse
                            )
                        )
                    }

                    3 -> {
                        text =
                            itemView.resources.getString(R.string.appointment_list_status_doctor_uploaded_prescription)
                        setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorResponseReceived
                            )
                        )
                    }

                    4 -> {
                        text =
                            itemView.resources.getString(R.string.appointment_list_status_test_appointment_booked)
                        setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorResponseReceived
                            )
                        )
                    }

                    5 -> {
                        text =
                            itemView.resources.getString(R.string.appointment_list_status_test_results_awaited)
                        setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorWaitingForResponse
                            )
                        )
                    }

                    6 -> {
                        text =
                            itemView.resources.getString(R.string.appointment_list_status_test_results_received)
                        setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorResponseReceived
                            )
                        )
                    }


                    7 -> {
                        text =
                            itemView.resources.getString(R.string.appointment_list_status_pharmacy_order_placed)
                        setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorWaitingForResponse
                            )
                        )
                    }
                    8 -> {
                        text =
                            itemView.resources.getString(R.string.appointment_list_status_self_assessment_pending)
                        setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorTabSelected
                            )
                        )
                    }


                }
            }

            var dateTime = DateTime.now()
            dateTime = dateTime.withMillis(item.dateTime!!)

            dateTV.text = getFullDate(dateTime.millis, 2)
            timeTV.text = getFullDate(dateTime.millis, 3)
            symptomsTV.text = item.commentSymptoms

            deleteCL.setOnClickListener { onActionClick(item) }
            imageView3.setOnClickListener { onStatusUpdate(item) }
        }

    }

}