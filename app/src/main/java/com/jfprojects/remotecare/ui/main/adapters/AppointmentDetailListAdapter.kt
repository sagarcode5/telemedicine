package com.jfprojects.remotecare.ui.main.adapters

import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexboxLayoutManager
import com.jfprojects.remotecare.helper.getFullDate
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.Appointment
import com.jfprojects.remotecare.database.models.AppointmentDetail
import com.jfprojects.remotecare.database.models.DefaultList
import kotlinx.android.synthetic.main.single_item_detail_appointment_view_ai_option.view.*
import kotlinx.android.synthetic.main.single_item_detail_appointment_view_ai_status_info.view.*

open class AppointmentDetailListAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var appointmentDetailList: ArrayList<AppointmentDetail> = ArrayList()

    open fun onActionClick(
        item: Appointment
    ) {

    }

    open fun onObjectClick(
        item: Appointment
    ) {

    }

    open fun onStatusUpdate(
        item: Appointment
    ) {

    }

    open fun onSingleOptionSelection(
        item: AppointmentDetail, selectedButton: String
    ) {

    }

    override fun getItemViewType(position: Int): Int {
        return appointmentDetailList[position].viewType!!
    }


    override fun onCreateViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        return when (i) {
            1 -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(
                        R.layout.single_item_detail_appointment_view_ai_status_info,
                        parent,
                        false
                    )
                MyViewHolderAIStatus(v)
            }
            2, 3 -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(
                        R.layout.single_item_detail_appointment_view_ai_option,
                        parent,
                        false
                    )
                MyViewHolderAIOption(v)
            }
            else -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(
                        R.layout.single_item_detail_appointment_view_user_answer,
                        parent,
                        false
                    )
                MyViewHolderUserAnswer(v)
            }
        }

    }

    fun setData(list: ArrayList<AppointmentDetail>) {
        appointmentDetailList = ArrayList()
        appointmentDetailList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        when (getItemViewType(i)) {
            1 -> (viewHolder as MyViewHolderAIStatus).bind(appointmentDetailList[i])
            2, 3 -> (viewHolder as MyViewHolderAIOption).bind(appointmentDetailList[i])
            else -> (viewHolder as MyViewHolderUserAnswer).bind(appointmentDetailList[i])
        }
    }


    override fun getItemCount(): Int {
        return appointmentDetailList.size
    }

    private inner class MyViewHolderAIStatus(v: View) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: AppointmentDetail) = with(itemView) {

            titleTV.text = item.title
            dateTimeTV.text = getFullDate(item.dateTime!!, 4)
            descriptionTV.text = item.description
        }

    }

    private inner class MyViewHolderAIOption(
        v: View,
        var aiOptionListAdapter: AIOptionListAdapter? = null
    ) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: AppointmentDetail) = with(itemView) {

            listRV.layoutManager = FlexboxLayoutManager(itemView.context)

            if (!item.defaultList.isNullOrEmpty())
                updateUI(item, listRV, item.defaultList!!)
        }

        private fun updateUI(
            appointmentDetail: AppointmentDetail,
            recyclerView: RecyclerView,
            defaultList: ArrayList<DefaultList>
        ) {
            if (aiOptionListAdapter == null)
                aiOptionListAdapter = object : AIOptionListAdapter() {
                    override fun onObjectClick(item: DefaultList) {
                        super.onObjectClick(item)
                        onSingleOptionSelection(appointmentDetail, item.title!!)
                    }
                }

            recyclerView.adapter = aiOptionListAdapter
            Handler().postDelayed(
                {
                    aiOptionListAdapter!!.setData(defaultList)
                }
                , 100
            )
            recyclerView.visibility = View.VISIBLE
        }

    }

    private inner class MyViewHolderUserAnswer(v: View) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: AppointmentDetail) = with(itemView) {

            titleTV.text = item.title
            dateTimeTV.text = getFullDate(item.dateTime!!, 4)
            descriptionTV.text = item.description
        }

    }

}