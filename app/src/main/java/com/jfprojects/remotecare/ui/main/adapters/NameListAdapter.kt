package com.jfprojects.remotecare.ui.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jfprojects.remotecare.R
import kotlinx.android.synthetic.main.single_item_test_item.view.*

open class NameListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var stringList: ArrayList<String> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        // create a new view
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.single_item_test_item, parent, false)
        return MyViewHolder(v)
    }

    open fun onItemClick(item: String) {

    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        (viewHolder as MyViewHolder).bind(stringList[i])
    }

    override fun getItemCount(): Int {
        return stringList.size
    }

    fun setData(list: ArrayList<String>) {
        stringList = ArrayList()
        stringList = list
        notifyDataSetChanged()
    }

    private inner class MyViewHolder(v: View) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: String) = with(itemView) {
            setOnClickListener {
                onItemClick(item)
            }
            titleTV.text = if (item.isEmpty()) "" else item
        }

    }

}