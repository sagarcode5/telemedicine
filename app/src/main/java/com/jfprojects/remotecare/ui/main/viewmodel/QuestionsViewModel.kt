package com.jfprojects.remotecare.ui.main.viewmodel

import android.app.Application
import android.os.Handler
import androidx.lifecycle.AndroidViewModel
import com.google.gson.Gson
import com.jfprojects.remotecare.helper.logDebug
import com.jfprojects.remotecare.common.fragment.AbsParentFragment
import com.jfprojects.remotecare.database.models.db.ActionName
import com.jfprojects.remotecare.database.models.db.Question
import com.jfprojects.remotecare.database.models.db.SymptomOptions
import com.jfprojects.remotecare.database.models.db.SymptomsQuestion
import com.jfprojects.remotecare.database.models.request.LastQuestionRequest
import com.jfprojects.remotecare.database.repository.QuestionsRepository
import java.lang.Thread.sleep

class QuestionsViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: QuestionsRepository? = null
    private var totalCredit: Int = 0

    companion object {
        var localQuestions: HashMap<Int, ArrayList<Question>> = HashMap()
        var selectedCatId: Int = 0
    }

    init {
        selectedCatId = 0
        totalCredit = 0
        localQuestions = HashMap()
        repository = QuestionsRepository(application)
    }

    fun getAllQuestions() = repository?.getAllQuestions()
    fun getQuestions() = repository?.getQuestions()

    fun deleteQuestionFromDb() {
        repository?.deleteQuestionFromDb()
    }


    fun getAllQuestionsFromServer() {
        repository?.getQuestionsFromServer()
    }

    fun addValue(pos: Int, item: Question) {
        val list = repository?.getQuestions() as ArrayList
        list[pos].isQuestionAnswered = true
        list[pos].symptomOptions = item.symptomOptions

        // updating the local list, that this question is answered.
        if (localQuestions.containsKey(0) && selectedCatId == 0) {
            val questions = localQuestions[0]
            if (questions != null) {
                for (i in 0 until questions.size) {
                    if (questions[i].localId == item.localId) {
                        questions[i].isQuestionAnswered = true
                        questions[i].symptomOptions = item.symptomOptions
                    }
                }
                localQuestions[0] = questions
            }
        } else if (localQuestions.containsKey(selectedCatId)) {
            val questions = localQuestions[selectedCatId]
            if (questions != null) {
                for (i in 0 until questions.size) {
                    if (questions[i].localId == item.localId) {
                        questions[i].isQuestionAnswered = true
                        questions[i].symptomOptions = item.symptomOptions

                    }
                }
                localQuestions[selectedCatId] = questions

            }
        }
        for (opt in item.symptomOptions) {
            totalCredit += opt.credit
        }
        // updating the answer to the db, to show user the response.
        if (list.isNotEmpty()) {
            repository?.updateQuestions(list)
        }

        sleep(300)

//---------------------------------------------------------------------------------------------------
        // from here working on to populate the next question
        // if any sub category is selected by user
        if (selectedCatId > 0 && localQuestions[selectedCatId] != null) {

            //show the next question from sub category.
            if (parseAndFetchAnotherQuestion(selectedCatId))
                return

            // if this question is last question of that sub category
            // if this is marked as last question by server.
            if (item.symptomsQuestion.responseType) {
                sendQuestionResponsesToServer()
            } else if (item.symptomOptions.contains(SymptomOptions(name = ActionName.NoneOfTheAbove.str))) {
                //show the next question from main questions
                if (parseAndFetchAnotherQuestion(0))
                    return
            }
            // fetch other questions from the questionId
            else {
                getNextQuestionById(item = item)
            }
        } else {
            if (item.symptomOptions.contains(SymptomOptions(name = ActionName.NoneOfTheAbove.str))) {
                //show the next question from main questions
                if (parseAndFetchAnotherQuestion(0))
                    return
            }

            // if this question is last question
            // if this is marked as last question by server.
            if (item.symptomsQuestion.responseType) {
                sendQuestionResponsesToServer()
            }

            // fetch other questions from the questionId
            else {
                getNextQuestionById(item = item)
            }

        }

/*
        // if this is the last question.
        if (item.symptomsQuestion.responseType) {
            Handler().postDelayed({
                val lastQuestionRequest =
                    LastQuestionRequest(userid = "sagar@code5.org", totalCredit = totalCredit)
                val questions: ArrayList<SymptomsQuestion> = ArrayList()
                for (quest in repository?.getQuestions() as ArrayList) {
                    questions.add(
                        SymptomsQuestion(
                            id = quest.symptomsQuestion.id,
                            question = quest.symptomsQuestion.question,
                            options = quest.symptomOptions
                        )
                    )
                }

                if (questions.isNotEmpty()) {
                    lastQuestionRequest.questions = questions
                    logDebug("TAG", Gson().toJson(lastQuestionRequest))
                    repository?.updateQuestionsToServer(lastQuestionRequest)

                }
            }, 200)

        } else {
            // some Cat is selected.
            if (selectedCatId > 0 && localQuestions[selectedCatId] != null) {
                for (ques in localQuestions[selectedCatId]!!) {
                    if (!ques.isQuestionAnswered) {
                        Handler().postDelayed({
                            repository?.addQuestion(arrayListOf(ques))
                            AbsParentFragment.isApiInProcess = false
                        }, 200)
                        return
                    }
                }
                selectedCatId = 0
            }

            // is user selects none of the above get next question from local cat list
            if (item.symptomOptions.contains(SymptomOptions(name = ActionName.NoneOfTheAbove.str))) {

                if (selectedCatId == 0) {
                    for (ques in localQuestions[0]!!) {
                        if (!ques.isQuestionAnswered) {
                            Handler().postDelayed({
                                repository?.addQuestion(arrayListOf(ques))
                                AbsParentFragment.isApiInProcess = false

                            }, 200)
                            return
                        }
                    }
                    selectedCatId = 0
                } else if (localQuestions.containsKey(selectedCatId) && localQuestions[selectedCatId] != null) {
                    for (ques in localQuestions[selectedCatId]!!) {
                        if (!ques.isQuestionAnswered) {
                            Handler().postDelayed({
                                repository?.addQuestion(arrayListOf(ques))
                                AbsParentFragment.isApiInProcess = false

                            }, 200)
                            return
                        }
                    }
                    selectedCatId = 0

                } else {
                    // fetching the sub category questions.
                    val ids: List<Int?> = item.symptomOptions.map { it.id }
                    selectedCatId = item.id
                    repository?.getQuestionsFromServerByOptionId(ids.joinToString())
                }
            } else {

                // fetching the sub category questions.
                val ids: List<Int?> = item.symptomOptions.map { it.id }
                selectedCatId = item.id
                repository?.getQuestionsFromServerByOptionId(ids.joinToString())
            }
        }*/

    }

    private fun parseAndFetchAnotherQuestion(i: Int): Boolean {
        for (ques in localQuestions[i]!!) {
            if (!ques.isQuestionAnswered) {
                Handler().postDelayed({
                    repository?.addQuestion(arrayListOf(ques))
                    AbsParentFragment.isApiInProcess = false
                }, 200)
                return true
            }
        }
        selectedCatId = 0
        return false
    }

    private fun getNextQuestionById(item: Question) {
        // fetching the sub category questions.
        val ids: List<Int?> = item.symptomOptions.map { it.id }
        selectedCatId = item.id
        repository?.getQuestionsFromServerByOptionId(ids.joinToString())
    }

    fun sendQuestionResponsesToServer() {
        Handler().postDelayed({
            val lastQuestionRequest =
                LastQuestionRequest(
                    userid = "sagar@code5.org",
                    totalCredit = totalCredit
                )
            val questions: ArrayList<SymptomsQuestion> = ArrayList()
            for (quest in repository?.getQuestions() as ArrayList) {
                questions.add(
                    SymptomsQuestion(
                        id = quest.symptomsQuestion.id,
                        question = quest.symptomsQuestion.question,
                        options = quest.symptomOptions
                    )
                )
            }

            if (questions.isNotEmpty()) {
                lastQuestionRequest.questions = questions
                logDebug("TAG", Gson().toJson(lastQuestionRequest))
                repository?.updateQuestionsToServer(lastQuestionRequest)

            }
        }, 200)
    }

}