package com.jfprojects.remotecare.ui.main.fragment

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.gson.Gson
import com.jfprojects.remotecare.helper.Params
import com.jfprojects.remotecare.helper.RequestCode
import com.jfprojects.remotecare.helper.getFullDate
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.common.fragment.AbsNoNavParentFragment
import com.jfprojects.remotecare.database.models.*
import com.jfprojects.remotecare.ui.main.adapters.NameListAdapter
import com.jfprojects.remotecare.ui.main.adapters.SelectedNameListAdapter
import com.jfprojects.remotecare.ui.main.viewmodel.LabTestViewModel
import de.synexs.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import kotlinx.android.synthetic.main.fragment_add_new_lab_test.*
import org.joda.time.DateTime
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AddNewLabTestFragment : AbsNoNavParentFragment(), AdapterView.OnItemSelectedListener {
    private val labTestViewModel by viewModels<LabTestViewModel>()
    private var selectedDate = DateTime.now()
    private lateinit var datePicker: SingleDateAndTimePickerDialog.Builder
    private var labTest = LabTest()
    private var nameListAdapter: NameListAdapter? = null
    private var selectedTestListAdapter: SelectedNameListAdapter? = null
    private var selectedTestsList: ArrayList<String> = ArrayList()
    private var testNamesList: ArrayList<String> = ArrayList()

    private var imagePickedFrom: Int? = 0
    private var imageUri: Uri? = null

    private var selectedAppointmentDetailList: ArrayList<AppointmentDetail> = ArrayList()
    private var selectedAppointment: Appointment? = null
    private var selectedAppointmentId: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_new_lab_test, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        datePicker = SingleDateAndTimePickerDialog.Builder(context)
        selectedAppointmentId = requireArguments().getInt(Params.APPOINTMENT_ID)

        if (selectedAppointmentId > 0) {
            labTestViewModel.getAppointment(selectedAppointmentId)
                ?.observe(
                    viewLifecycleOwner,
                    androidx.lifecycle.Observer { getSelectedAppointment(it) })
            appointmentInfoTV.visibility = View.VISIBLE
        } else appointmentInfoTV.visibility = View.GONE
//        val labsList = ArrayList<String>()
//        resources.getStringArray(R.array.labs_names).forEach { labsList.add(it) }
//        initSpinners(labSpinner, labsList)

        val autoAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_list_item_1, resources.getStringArray(R.array.test_names)
        )
        labTextTV.setOnClickListener { openSearchLabsDialog() }

        searchTestET.setAdapter(autoAdapter)
        searchTestET.setOnItemClickListener { adapterView: AdapterView<*>, view1: View, i: Int, l: Long ->
            addNewValue((view1 as TextView).text.toString())
            searchTestET.text.clear()
        }
        labTestRV.layoutManager =
            FlexboxLayoutManager(requireContext())

        selectedTestsRV.layoutManager =
            FlexboxLayoutManager(requireContext())

        val labTestTypeList = ArrayList<String>()
        resources.getStringArray(R.array.lab_test_type_list).forEach { labTestTypeList.add(it) }
        initSpinners(labTestTypeSpinner, labTestTypeList)

        val patientList = ArrayList<String>()
        resources.getStringArray(R.array.patient_names).forEach { patientList.add(it) }
        initSpinners(patientSpinner, patientList)

        testNamesList = ArrayList()
        resources.getStringArray(R.array.test_names)
            .forEach { testNamesList.add(it) }

        initAdapter(testNamesList)
        initSelectedAdapter(selectedTestsList)

        dateTextTV.text = getFullDate(selectedDate.millis, 1)

        dateTextTV.setOnClickListener {
            datePicker
                .displayAmPm(false)
                .defaultDate(selectedDate.toDate())
                .title(resources.getString(R.string.date_picker_title))
                .curved()
                .defaultDate(selectedDate.toDate())
                .minDateRange(DateTime.now().toDate())
                .customLocale(Locale.getDefault())
                .displayDays(false)
                .displayDaysOfMonth(true)
                .displayMonth(true)
                .displayYears(true)
                .displayHours(false)
                .displayMinutes(false)
                .setTimeZone(Calendar.getInstance().timeZone)
                .mainColor(resources.getColor(R.color.colorPrimary))
                .titleTextColor(resources.getColor(R.color.white))
                .backgroundColor(resources.getColor(R.color.colorGrey))
                .listener { date: Date ->

                    selectedDate = selectedDate.withYear(getFullDate(date.time, 9)!!.toInt())
                    selectedDate = selectedDate.withMonthOfYear(getFullDate(date.time, 8)!!.toInt())
                    selectedDate = selectedDate.withDayOfMonth(getFullDate(date.time, 7)!!.toInt())
                    dateTextTV.text = getFullDate(date.time, 1)
                }.display()
        }

        imageIV.setOnClickListener {
            getImagePickerDialog(resources.getString(R.string.select_image_heading), "")
        }

        bookTV.setOnClickListener {
            validate()
        }
    }

    override fun apiLogSuccess(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun apiLogFailure(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    private fun getSelectedAppointment(it: Appointment) {
        selectedAppointment = it
        appointmentInfoTV.text = String.format(
            resources.getString(R.string.lab_test_appointment_info), it.professionalName,
            getFullDate(it.dateTime, 4)
        )
    }

    private fun openSearchLabsDialog() {

        val list: ArrayList<SearchList> = ArrayList()

        resources.getStringArray(R.array.labs_names).forEach {
            list.apply {
                add(SearchList(viewType = 1, defaultList = DefaultList(title = it)))
            }
        }

        SearchListDialog().apply {
            arguments = Bundle().apply {
                putInt(Params.DATA, 1)
                putSerializable(
                    Params.ITEMS, list
                )
                setTargetFragment(this@AddNewLabTestFragment, RequestCode.Labs)
                show(this@AddNewLabTestFragment.parentFragmentManager, RequestCode.LabsTag)
            }
        }
    }
//
//    private fun openSearchPatientDialog() {
//
//        val list: ArrayList<SearchList> = ArrayList()
//
//        resources.getStringArray(R.array.pharmacy_names).forEach {
//            list.apply {
//                add(SearchList(viewType = 1, defaultList = DefaultList(title = it)))
//            }
//        }
//
//        SearchListDialog().apply {
//            arguments = Bundle().apply {
//                putInt(Params.DATA, 1)
//                putSerializable(
//                    Params.ITEMS, list
//                )
//                setTargetFragment(this@AddNewLabTestFragment, RequestCode.Labs)
//                show(this@AddNewLabTestFragment.parentFragmentManager, RequestCode.LabsTag)
//            }
//        }
//    }

    private fun getImagePickerDialog(
        title: String?,
        message: String?
    ) {
        val alertBuild = AlertDialog.Builder(context)
        alertBuild
            .setTitle(title)
            .setMessage(message)
            .setCancelable(true)
            .setPositiveButton(
                resources.getString(R.string.camera_heading)
            ) { dialog: DialogInterface, whichButton: Int ->
                dialog.dismiss()
                checkPermissionAndOpenCamera()
            }
            .setNegativeButton(
                resources.getString(R.string.gallery_heading)
            ) { dialog: DialogInterface, whichButton: Int ->
                dialog.dismiss()
                try {
                    if (ActivityCompat.checkSelfPermission(
                            requireActivity(),
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ) != PackageManager.PERMISSION_GRANTED ||
                        ActivityCompat.checkSelfPermission(
                            requireActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        ActivityCompat.requestPermissions(
                            requireActivity(),
                            arrayOf(
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ),
                            RequestCode.Gallery
                        )
                    } else {
                        pickupFromGallery()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        val dialog = alertBuild.create()
        dialog.show()
        val alertTitle = resources.getIdentifier("alertTitle", "id", "android")
        (dialog.findViewById<View>(alertTitle) as TextView).gravity = Gravity.CENTER
        (dialog.findViewById<View>(alertTitle) as TextView).gravity = Gravity.CENTER
    }

    private fun checkPermissionAndOpenCamera() {
        try {

            when {
                ActivityCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED -> {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        arrayOf(Manifest.permission.CAMERA),
                        RequestCode.TakePicture
                    )
                }
                ActivityCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED -> {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        RequestCode.TakePicture
                    )
                }
                ActivityCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED -> {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        RequestCode.TakePicture
                    )
                }
                else -> {
                    takePicture()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private var mFileName: String? = null

    private fun pickupFromGallery() {
        val getIntent = Intent(Intent.ACTION_GET_CONTENT)
        getIntent.type = "image/*"

        val pickIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        pickIntent.type = "image/*"

        val chooserIntent =
            Intent.createChooser(getIntent, resources.getString(R.string.select_image_heading))
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))
        startActivityForResult(chooserIntent, RequestCode.Gallery)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestCode.TakePicture && resultCode == Activity.RESULT_OK) {
            imageIV.setImageURI(imageUri)
            imagePickedFrom = 2
        } else if (requestCode == RequestCode.Gallery && resultCode == Activity.RESULT_OK) {
            if (data != null && data.data != null) {
                imageUri = data.data
                imagePickedFrom = 1
                imageIV.setImageURI(data.data)
            }
        } else if (requestCode == RequestCode.Labs) {
            val searchListResult = data?.getSerializableExtra(Params.DATA) as SearchList

            searchListResult.defaultList!!.title.let {
                labTextTV.text = it
                labTest.labName = it
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date())
        val storageDir: File =
            requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
            mFileName = name
        }
    }

    private lateinit var currentPhotoPath: String

    private fun takePicture() {

        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        requireActivity(),
                        "com.jfprojects.remotecare.fileprovider",
                        it
                    )
                    imageUri = photoURI
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                    requireActivity().startActivityForResult(
                        takePictureIntent,
                        RequestCode.TakePicture
                    )
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            RequestCode.Gallery ->                 // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickupFromGallery()
                } else {
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                    Toast.makeText(
                        context,
                        "Gallery Permission not granted!!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            RequestCode.TakePicture -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkPermissionAndOpenCamera()
                } else {
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                    Toast.makeText(
                        context,
                        "Camera Permission not granted!!",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            }
        }
    }

    private fun initAdapter(list: ArrayList<String>) {
        if (nameListAdapter == null) {
            nameListAdapter = object : NameListAdapter() {
                override fun onItemClick(item: String) {
                    super.onItemClick(item)
                    addNewValue(item)
                }
            }
        }
        labTestRV.adapter = nameListAdapter
        Handler().postDelayed({
            nameListAdapter!!.setData(list)
        }, 300)

    }

    private fun initSelectedAdapter(list: ArrayList<String>) {
        if (selectedTestListAdapter == null) {
            selectedTestListAdapter = object : SelectedNameListAdapter() {
                override fun removeItem(item: String) {
                    super.removeItem(item)
                    removeValue(item)
                }
            }
        }

        Handler().postDelayed({
            selectedTestListAdapter!!.setData(list)
            hintTV.text =
                if (list.size < 1) resources.getString(R.string.pharmacy_order_select_med_hint_2) else resources.getString(
                    R.string.pharmacy_order_select_med_hint_3
                )
        }, 300)

        selectedTestsRV.adapter = selectedTestListAdapter
    }

    private fun addNewValue(item: String) {
        selectedTestsList.add(item)
        testNamesList.remove(item)

        initAdapter(testNamesList)
        initSelectedAdapter(selectedTestsList)
    }

    private fun removeValue(item: String) {
        selectedTestsList.remove(item)
        testNamesList.add(item)

        initAdapter(testNamesList)
        initSelectedAdapter(selectedTestsList)
    }

    private fun validate() {
        var isDataValid = true

//        if (labTest.labName.isNullOrEmpty()) {
//            labTest.labName = labSpinner.selectedItem as String?
//        }

        if (labTest.patientName.isNullOrEmpty()) {
            labTest.patientName = patientSpinner.selectedItem as String?
        }

        if (labTest.labTestType == 0) {
            labTest.labTestType = labTestTypeSpinner.selectedItemPosition + 1
        }

        labTest.comments = commentsET.text.toString()
        labTest.dateTime = selectedDate.millis
        labTest.testReportStatus = 1

        if (labTest.labName.isNullOrEmpty()) {
            isDataValid = false
            Toast.makeText(context, "Kindly select the Lab", Toast.LENGTH_SHORT).show()
        }

        if (selectedTestsList.isNullOrEmpty()) {
            isDataValid = false
            Toast.makeText(context, "Kindly select the tests", Toast.LENGTH_SHORT).show()
        }

        if (isDataValid) {
            labTest.testsName = selectedTestsList

            Log.e("TAG", Gson().toJson(labTest))
            labTestViewModel?.addLabTests(labTest)
            if (selectedAppointment != null)
                updateAppointment()
            else findNavController().navigateUp()
//            findNavController().navigate(AddNewLabTestFragmentDirections.actionNavigationAddNewTestToNavigationLabPharmacy())


        }

    }

    private fun updateAppointment() {
        //4- Testing Lab Appointment Fixed
        selectedAppointment!!.appointmentStatus = 4

        // removing the user action to be performed.
        if (!selectedAppointment!!.appointmentDetailList.isNullOrEmpty()) {
            selectedAppointmentDetailList = selectedAppointment!!.appointmentDetailList!!
            for (obj in selectedAppointment!!.appointmentDetailList!!) {
                if (obj.viewType == 2) {
                    selectedAppointment!!.appointmentDetailList!!.remove(obj)
                }
            }
            selectedAppointment!!.appointmentDetailList = selectedAppointmentDetailList
        }

        addAppointmentDetail(selectedAppointment!!)

        labTestViewModel?.updateAppointmentStatus(appointment = selectedAppointment!!)
        findNavController().navigateUp()

//        findNavController().navigate(AddNewLabTestFragmentDirections.actionNavigationAddNewTestToNavigationSingleAppointment())

    }

    /*
    * 1- appointment Fixed
    * 2- waiting for doctor Prescription
    * 3- Doctor Uploaded Prescription
    * 4- Testing Lab Appointment Fixed
    * 5- Samples for test sent
    * 6- Tests Done
    * 7- Pharmacy ordered
    * 8- Self- Assessment Pending
    *
    * */
    private fun addAppointmentDetail(item: Appointment) {
        when (item.appointmentStatus) {
            4 -> {

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 4,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_tests_order_placed_title_by_user),
                        description = resources.getString(R.string.appointment_detail_status_tests_order_placed_desc_by_user)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 2,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        defaultList = arrayListOf(
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_order_medicines_option)),
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_self_assessment_option))
                        )

                    )
                )

            }
        }
        item.appointmentDetailList = selectedAppointmentDetailList

    }

    private fun initSpinners(spinner: Spinner, list: ArrayList<String>) {
        val titles = ArrayList<String?>()
        for (cat in list) {
            titles.add(cat)
        }
        val listAdapter: ArrayAdapter<String?> = object : ArrayAdapter<String?>(
            requireContext(),
            android.R.layout.simple_spinner_item,
            android.R.id.text1, titles
        ) {
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                var v: View? = null
                v = super.getDropDownView(position, null, parent)
                // If this is the selected item position
                if (position == spinner.selectedItemPosition) {
                    v.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGrey))
                } else {
                    // for other views
                    v.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
                }
                return v
            }
        }
        listAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        with(spinner) {
            adapter = listAdapter
            isSelected = false
            setSelection(0, true)

            onItemSelectedListener = this@AddNewLabTestFragment
        }
    }


    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        when (p0?.id) {

//            R.id.labSpinner -> {
//                labTest.labName = labSpinner.getItemAtPosition(p3.toInt()) as String?
//            }

            R.id.patientSpinner -> {
                labTest.patientName = patientSpinner.getItemAtPosition(p3.toInt()) as String?
            }

            R.id.labTestTypeSpinner -> {
                labTest.labTestType = p3.toInt() + 1
            }
        }
    }
}


