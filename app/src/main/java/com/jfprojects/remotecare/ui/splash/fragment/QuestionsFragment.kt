package com.jfprojects.remotecare.ui.splash.fragment

import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import com.jfprojects.remotecare.helper.APIConstants
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.common.fragment.AbsNoNavParentFragment
import com.jfprojects.remotecare.database.models.APILogTable
import com.jfprojects.remotecare.database.models.db.Question
import com.jfprojects.remotecare.database.models.response.QuestionFinalResponse
import com.jfprojects.remotecare.helper.views.SpeedyLinearLayoutManager
import com.jfprojects.remotecare.ui.main.viewmodel.QuestionsViewModel
import com.jfprojects.remotecare.ui.splash.adapters.QuestionsListAdapter
import kotlinx.android.synthetic.main.fragment_questions.*
import kotlinx.android.synthetic.main.single_item_questions_response.*

class QuestionsFragment : AbsNoNavParentFragment() {
    private var questionsListAdapter: QuestionsListAdapter? = null
    private var behavior: BottomSheetBehavior<ConstraintLayout>? = null

    val questionsViewModel by viewModels<QuestionsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_questions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        behavior = BottomSheetBehavior.from(questionResponseBS)

        questionRV.layoutManager =
            SpeedyLinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        showProgress()
        isApiInProcess = true
        questionsViewModel.deleteQuestionFromDb()

        Handler().postDelayed({
            questionsViewModel.getAllQuestionsFromServer()
        }, 100)
        questionsViewModel.getAllQuestions()!!
            .observe(viewLifecycleOwner, Observer { updateListOfQuestions(it) })

        skipTV.setOnClickListener {
            behavior?.state = BottomSheetBehavior.STATE_HIDDEN

            Handler().postDelayed({
                findNavController().navigate(
                    QuestionsFragmentDirections.actionNavigationQuestionsToNavigationHome(
                        true, null
                    )
                )
            }, 300)

        }
    }

    private fun openBottomSheetDialog(response: QuestionFinalResponse?) {
        if (response != null) {
            behavior?.state = BottomSheetBehavior.STATE_EXPANDED
            behavior?.isDraggable = false // disable dragging

            initViews(response)


            //            val addBottomDialogFragment = QuestionResponseFragment()
//            val bun = Bundle()
//            bun.putParcelable(Params.DATA, response)
//            addBottomDialogFragment.arguments = bun
//            addBottomDialogFragment.show(
//                childFragmentManager,
//                "BottomSheet"
//            )
        }
//        findNavController().navigate(
//            QuestionsFragmentDirections.actionNavigationQuestionsToNavigationHome(
//                true, response
//            )
//        )

    }

    private fun initViews(response: QuestionFinalResponse) {

        constraintLayout.background =
            ContextCompat.getDrawable(requireContext(), R.drawable.bg_corner_top)
        val drawable = constraintLayout.background as GradientDrawable

        when (response.colorStatus) {
            1 -> {
                drawable.setColor(ContextCompat.getColor(requireContext(), R.color.colorUnsafe))
            }
            2 -> {
                drawable.setColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.colorModerate
                    )
                )
            }
            3 -> {
                drawable.setColor(ContextCompat.getColor(requireContext(), R.color.colorSafe))

            }
        }

        constraintLayout.background = drawable

        healthStatusTV.text = response.message
        advisoryTV.text = response.suggestion

    }


    override fun apiLogSuccess(table: APILogTable?) {
        when (table?.apiType) {
            APIConstants.updateQuestionsToServer -> {
                if (table.body != null) {
                    val response: QuestionFinalResponse =
                        Gson().fromJson(table.body.toString(), QuestionFinalResponse::class.java)
                    openBottomSheetDialog(response)
                } else {
                    showMessage("Something went wrong")
                }
            }

        }
    }

    override fun apiLogFailure(table: APILogTable?) {
        when (table?.apiType) {

            APIConstants.updateQuestionsToServer -> {
                if (table.body != null) {
                    val response: QuestionFinalResponse =
                        Gson().fromJson(table.body.toString(), QuestionFinalResponse::class.java)
                    openBottomSheetDialog(response)
                }
            }

            APIConstants.getAllQuestionsByOptionId -> {
                if (table.body != null) {
                    isApiInProcess = true
                    showProgress()
                    questionsViewModel.sendQuestionResponsesToServer()
                } else {
                    showMessage("Something went wrong")
                }
            }
        }
    }

    private fun initAdapter(list: ArrayList<Question>) {
        if (questionsListAdapter == null) {
            questionsListAdapter = object : QuestionsListAdapter() {
                override fun onActionClick(
                    pos: Int,
                    item: Question
                ) {
                    super.onActionClick(pos, item)
                    showProgress()
                    isApiInProcess = true
                    Handler().postDelayed({
                        questionsViewModel.addValue(pos, item)
                    }, 200)
                }
            }

            questionRV.adapter = questionsListAdapter

        }

        Handler().postDelayed({
            questionsListAdapter!!.setData(list)
        }, 300)

    }

    private fun updateListOfQuestions(it: List<Question>?) {
        if (it != null) {
            if (!isApiInProcess) hideProgress()

            initAdapter(it as ArrayList<Question>)
            questionsListAdapter?.notifyDataSetChanged()
            if (it.isNotEmpty())
                Handler().postDelayed({ questionRV.smoothScrollToPosition(it.lastIndex) }, 300)
        }
    }

}
