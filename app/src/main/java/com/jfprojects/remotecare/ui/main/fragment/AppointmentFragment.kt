package com.jfprojects.remotecare.ui.main.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.common.fragment.AbsNavParentFragment
import com.jfprojects.remotecare.database.models.APILogTable
import kotlinx.android.synthetic.main.fragment_appointment.*
import java.util.*
import kotlin.math.abs

class AppointmentFragment : AbsNavParentFragment() {

    private val fragmentList: ArrayList<Fragment> = ArrayList<Fragment>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_appointment, container, false)
    }

    override fun apiLogSuccess(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun apiLogFailure(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentList.clear()
        fragmentList.add(FutureAppointmentsFragment())
        fragmentList.add(AppointmentsHistoryFragment())

        viewPager.apply {
            adapter =
                MyPagerAdapter(
                    requireContext(),
                    childFragmentManager,
                    fragmentList
                )
            viewPager.setPageTransformer(
                false
            ) { page: View, position: Float ->
                // do transformation here
                val normalizedposition =
                    abs(abs(position) - 1)
                page.scaleX = normalizedposition / 2 + 0.5f
                page.scaleY = normalizedposition / 2 + 0.5f
            }

            tabs.setupWithViewPager(this)
        }
    }


    class MyPagerAdapter(
        context: Context,
        fm: FragmentManager?,
        fragmentList: List<Fragment>?
    ) :
        FragmentPagerAdapter(fm!!) {
        private var fragmentLis: List<Fragment>? = fragmentList
        private var context: Context = context

        override fun getItem(i: Int): Fragment {
            return fragmentLis!![i]
        }

        override fun getCount(): Int {
            return fragmentLis!!.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return when (position) {
                0 -> context.resources.getString(R.string.title_future_appointment)
                else ->
                    context.resources.getString(R.string.title_appointment_history)
            }
        }
    }

}
