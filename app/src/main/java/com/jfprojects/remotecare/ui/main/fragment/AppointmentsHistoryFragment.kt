package com.jfprojects.remotecare.ui.main.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jfprojects.remotecare.R
import kotlinx.android.synthetic.main.fragment_appointment_list.*

class AppointmentsHistoryFragment : Fragment() {
//    private var jobListArrayList: ArrayList<JobList> = ArrayList()
//    private lateinit var selectedProfUser: ProfessionalUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        selectedProfUser = SharedPreference.getInstance()
//            .getObject(KEY_VALUE_USER_PROFESSIONAL, ProfessionalUser::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_appointment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        appointmentListRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
//        resetAdapter()
        bookNewAppointmentTV.visibility = View.GONE
    }

//    override fun onStop() {
//        super.onStop()
//        if (docRef != null) {
//            docRef!!.remove()
//        }
//    }
//
//    private fun resetAdapter() {
//        val queryForIncidents =
//            db
//                .collection("shifts")
//
//        docRef = queryForIncidents.addSnapshotListener { snapshot, e ->
//            if (e != null) {
//                return@addSnapshotListener
//            }
//            jobListArrayList.clear()
//            if (snapshot != null && !snapshot.isEmpty) {
//                for (docs in snapshot.documents) {
//                    val obj: JobList = docs.toObject(JobList::class.java)!!.withId(docs.id)
//
//                    if ((obj.responses != null && obj.responses!!.size > 0 && !obj.responses!!.contains(
//                            selectedProfUser
//                        )) || obj.responses == null
//                    ) {
//                        jobListArrayList.add(obj)
//                    }
//                }
//
//                getAllJobList(jobListArrayList)
//
//            }
//        }
//    }
//
//    private fun getAllJobList(it: ArrayList<JobList>) {
//        if (it.size > 0) {
//            initAdapter(it)
//        } else {
//            noDataTV.visibility = View.VISIBLE
//            myJobRV.visibility = View.INVISIBLE
//        }
//    }
//
//    private val db = Firebase.firestore
//    private var jobListAdapter: ShiftsAdapterNew? = null
//
//    private fun initAdapter(jobList: ArrayList<JobList>) {
//        if (jobListAdapter == null) {
//        jobListAdapter = object : ShiftsAdapterNew() {
//
//            override fun onItemClick(item: JobList?) {
//                super.onItemClick(item)
//                findNavController().navigate(
//                    CategoryFragmentDirections.actionNavigationCategoryToNavigationSingleJobDetail(
//                        item!!.id
//                    )
//                )
//            }
//
//        }
//        myJobRV.adapter = jobListAdapter
//}
//        Handler().postDelayed({
//            jobList.reverse()
//            jobListAdapter!!.setData(jobList)
//        }, 300)
//
//        myJobRV.visibility = View.VISIBLE
//        noDataTV.visibility = View.GONE
//    }
//
//
}

