package com.jfprojects.remotecare.ui.main.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import com.google.mlkit.common.model.DownloadConditions
import com.google.mlkit.nl.translate.TranslateLanguage
import com.google.mlkit.nl.translate.Translation.getClient
import com.google.mlkit.nl.translate.TranslatorOptions
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.common.fragment.AbsNavParentFragment
import com.jfprojects.remotecare.database.models.APILogTable
import com.jfprojects.remotecare.helper.log
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : AbsNavParentFragment() {
    private val englishHindiTranslator = getClient(
        TranslatorOptions.Builder()
            .setSourceLanguage(TranslateLanguage.ENGLISH)
            .setTargetLanguage(TranslateLanguage.HINDI)
            .build()
    )

    private val hindiEnglishTranslator = getClient(
        TranslatorOptions.Builder()
            .setSourceLanguage(TranslateLanguage.HINDI)
            .setTargetLanguage(TranslateLanguage.ENGLISH)
            .build()
    )

    var isHindiSelected = false
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)

    }

    override fun apiLogSuccess(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun apiLogFailure(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        behavior = BottomSheetBehavior.from(questionResponseBS)
        checkSymptomsCL.setOnClickListener {
//            if (isHindiSelected) {
//                isHindiSelected = false
//                changeHindiToEng(textView2.text.toString(), textView2)
//            } else {
//                isHindiSelected = true
//                changeEngToHindi(textView2.text.toString(), textView2)
//            }
            findNavController().navigate(HomeFragmentDirections.actionNavigationHomeToNavigationQuestions())
        }

    }

    private fun changeHindiToEng(str: String, id: TextView) {

        hindiEnglishTranslator.downloadModelIfNeeded(
            DownloadConditions.Builder()
                .build()
        )
            .addOnSuccessListener {
                hindiEnglishTranslator.translate(str)
                    .addOnSuccessListener { translatedText ->
                        // Translation successful.
                        id.text = translatedText
                    }
                    .addOnFailureListener { exception ->
                        // Error.
                        // ...
                    }

                // Model downloaded successfully. Okay to start translating.
                // (Set a flag, unhide the translation UI, etc.)
            }
            .addOnFailureListener { exception ->
                // Model couldn’t be downloaded or other internal error.
                // ...
            }

    }

    private fun changeEngToHindi(str: String, id: TextView) {

        englishHindiTranslator.downloadModelIfNeeded(
            DownloadConditions.Builder()
                .build()
        )
            .addOnSuccessListener {
                englishHindiTranslator.translate(str)
                    .addOnSuccessListener { translatedText ->
                        // Translation successful.
                        id.text = translatedText
                    }
                    .addOnFailureListener { exception ->
                        // Error.
                        // ...
                    }

                // Model downloaded successfully. Okay to start translating.
                // (Set a flag, unhide the translation UI, etc.)
            }
            .addOnFailureListener { exception ->
                log("TAG", exception.localizedMessage)
                // Model couldn’t be downloaded or other internal error.
                // ...
            }

    }

}
