package com.jfprojects.remotecare.ui.main.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.common.fragment.AbsNoNavParentFragment
import com.jfprojects.remotecare.database.models.*
import com.jfprojects.remotecare.helper.Params
import com.jfprojects.remotecare.helper.RequestCode
import com.jfprojects.remotecare.helper.getFullDate
import com.jfprojects.remotecare.ui.main.viewmodel.AppointmentViewModel
import de.synexs.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import kotlinx.android.synthetic.main.fragment_add_new_appointment.*
import org.joda.time.DateTime
import java.util.*

class AddNewAppointmentFragment : AbsNoNavParentFragment(), AdapterView.OnItemSelectedListener {
    private lateinit var appointmentViewModel: AppointmentViewModel
    private var selectedDate = DateTime.now()

    private lateinit var datePicker: SingleDateAndTimePickerDialog.Builder
    private var appointment = Appointment()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_new_appointment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        datePicker = SingleDateAndTimePickerDialog.Builder(context)

        appointmentViewModel = ViewModelProviders.of(this).get(AppointmentViewModel::class.java)

        hospitalTextTV.setOnClickListener { openSearchHospitalDialog() }
        medicalDepartmentTextTV.setOnClickListener { openSearchDepartmentDialog() }
        healthCareTextTV.setOnClickListener { openSearchHealthCareProfessionalDialog() }

//        val hospitalList = ArrayList<String>()
//        resources.getStringArray(R.array.hospital_names).forEach { hospitalList.add(it) }
//        initSpinners(hospitalSpinner, hospitalList)
//
//        val professionalList = ArrayList<String>()
//        resources.getStringArray(R.array.professional_names).forEach { professionalList.add(it) }
//        initSpinners(healthCareSpinner, professionalList)

        val appointmentTypeList = ArrayList<String>()
        resources.getStringArray(R.array.appointment_type_list)
            .forEach { appointmentTypeList.add(it) }
        initSpinners(appointmentTypeSpinner, appointmentTypeList)

        val patientList = ArrayList<String>()
        resources.getStringArray(R.array.patient_names).forEach { patientList.add(it) }
        initSpinners(patientSpinner, patientList)

        val timeList = ArrayList<String>()
        resources.getStringArray(R.array.time_list).forEach { timeList.add(it) }
        initSpinners(timeSpinner, timeList)

        dateTextTV.text = getFullDate(selectedDate.millis, 1)

        dateTextTV.setOnClickListener {
            datePicker
                .displayAmPm(false)
                .defaultDate(selectedDate.toDate())
                .title(resources.getString(R.string.date_picker_title))
                .curved()
                .defaultDate(selectedDate.toDate())
                .minDateRange(DateTime.now().toDate())
                .customLocale(Locale.getDefault())
                .displayDays(false)
                .displayDaysOfMonth(true)
                .displayMonth(true)
                .displayYears(true)
                .displayHours(false)
                .displayMinutes(false)
                .setTimeZone(Calendar.getInstance().timeZone)
                .mainColor(resources.getColor(R.color.colorPrimary))
                .titleTextColor(resources.getColor(R.color.white))
                .backgroundColor(resources.getColor(R.color.colorGrey))
                .listener { date: Date ->

                    selectedDate = selectedDate.withYear(getFullDate(date.time, 9)!!.toInt())
                    selectedDate = selectedDate.withMonthOfYear(getFullDate(date.time, 8)!!.toInt())
                    selectedDate = selectedDate.withDayOfMonth(getFullDate(date.time, 7)!!.toInt())
                    dateTextTV.text = getFullDate(date.time, 1)
                }.display()
        }
        bookTV.setOnClickListener {
            validate()
        }
    }

    override fun apiLogSuccess(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun apiLogFailure(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    private fun openSearchHospitalDialog() {
        val list: ArrayList<SearchList> = ArrayList()

        list.add(
            SearchList(
                viewType = 2,
                defaultList = DefaultList(
                    title = "Max Hospital",
                    message = "Patparganj, New Delhi"
                )
            )
        )

        list.add(
            SearchList(
                viewType = 2,
                defaultList = DefaultList(
                    title = "Ganga Ram Hospital",
                    message = "New Delhi"
                )
            )
        )

        list.add(
            SearchList(
                viewType = 2,
                defaultList = DefaultList(title = "AIMS", message = "New Delhi")
            )
        )

        list.add(
            SearchList(
                viewType = 2,
                defaultList = DefaultList(title = "Fortis", message = "New Delhi")
            )
        )

        list.add(
            SearchList(
                viewType = 2,
                defaultList = DefaultList(
                    title = "BLK Hospital",
                    message = "Karol Bagh, New Delhi"
                )
            )
        )

        SearchListDialog().apply {
            arguments = Bundle().apply {
                putInt(Params.DATA, 2)
                putSerializable(
                    Params.ITEMS, list
                )
                setTargetFragment(this@AddNewAppointmentFragment, RequestCode.Hospital)
                show(
                    this@AddNewAppointmentFragment.parentFragmentManager,
                    RequestCode.HospitalTag
                )
            }
        }
    }

    private fun openSearchHealthCareProfessionalDialog() {
        val list: ArrayList<SearchList> = ArrayList()

        list.add(
            SearchList(
                viewType = 3,
                defaultList = DefaultList(
                    title = "Dr. BK Gupta",
                    message = "10",
                    subMessage = "Cardiologist",
                    count = 500
                )
            )
        )

        list.add(
            SearchList(
                viewType = 3,
                defaultList = DefaultList(
                    title = "Dr. AK Chaudhary",
                    message = "12",
                    subMessage = "Dermatologists",
                    count = 700
                )
            )
        )

        list.add(
            SearchList(
                viewType = 3,
                defaultList = DefaultList(
                    title = "Dr. Sharma",
                    message = "20",
                    subMessage = "Endocrinologists",
                    count = 400
                )
            )
        )

        list.add(
            SearchList(
                viewType = 3,
                defaultList = DefaultList(
                    title = "Dr. Kumar",
                    message = "10",
                    subMessage = "Family Physicians",
                    count = 800
                )
            )
        )

        SearchListDialog().apply {
            arguments = Bundle().apply {
                putInt(Params.DATA, 3)
                putSerializable(
                    Params.ITEMS, list
                )
                setTargetFragment(this@AddNewAppointmentFragment, RequestCode.HealthCare)
                show(
                    this@AddNewAppointmentFragment.parentFragmentManager,
                    RequestCode.HealthCareTag
                )
            }
        }
    }

    private fun openSearchDepartmentDialog() {
        val list: ArrayList<SearchList> = ArrayList()

        list.add(
            SearchList(
                viewType = 4,
                defaultList = DefaultList(
                    title = "Cardiologist",
                    message = "They’re experts on the heart and blood vessels.",
                    count = 50
                )
            )
        )

        list.add(
            SearchList(
                viewType = 4,
                defaultList = DefaultList(
                    title = "Dermatologists",
                    message = "Have problems with your skin, hair, nails? Do you have moles, scars, acne, or skin allergies? Dermatologists can help.",
                    count = 10
                )
            )
        )

        list.add(
            SearchList(
                viewType = 4,
                defaultList = DefaultList(
                    title = "Endocrinologists",
                    message = "These are experts on hormones and metabolism.",
                    count = 4
                )
            )
        )

        list.add(
            SearchList(
                viewType = 4,
                defaultList = DefaultList(
                    title = " Family Physicians",
                    message = "They care for the whole family, including children, adults, and the elderly.",
                    count = 4
                )
            )
        )

        SearchListDialog().apply {
            arguments = Bundle().apply {
                putInt(Params.DATA, 4)
                putSerializable(
                    Params.ITEMS, list
                )
                setTargetFragment(this@AddNewAppointmentFragment, RequestCode.Department)
                show(
                    this@AddNewAppointmentFragment.parentFragmentManager,
                    RequestCode.DepartmentTag
                )
            }
        }
    }

    private fun validate() {
        var isDataValid = true


        if (appointment.patientName.isNullOrEmpty()) {
            appointment.patientName = patientSpinner.selectedItem as String?
        }
        if (appointment.appointmentType == 0) {
            appointment.appointmentType = patientSpinner.selectedItemPosition + 1
        }

        if (appointment.commentSymptoms.isNullOrEmpty())
            appointment.commentSymptoms = commentsET.text.toString()

        appointment.dateTime = selectedDate.millis
        appointment.appointmentStatus = 1

        if (appointment.commentSymptoms.isNullOrEmpty()) {
            isDataValid = false
            Toast.makeText(context, "Kindly provide the symptoms", Toast.LENGTH_SHORT).show()
        }

        if (appointment.hospitalName.isNullOrEmpty()) {
            isDataValid = false
            Toast.makeText(context, "Kindly select the Hospital", Toast.LENGTH_SHORT).show()
        }

        if (appointment.medicalDepartment.isNullOrEmpty()) {
            isDataValid = false
            Toast.makeText(context, "Kindly select the Medical Department", Toast.LENGTH_SHORT)
                .show()
        }

        if (appointment.professionalName.isNullOrEmpty()) {
            isDataValid = false
            Toast.makeText(context, "Kindly select the HealthCare Professional", Toast.LENGTH_SHORT)
                .show()
        }

        if (isDataValid) {

            appointment.appointmentDetailList = arrayListOf(
                AppointmentDetail(
                    viewType = 1,
                    dateTime = DateTime.now().millis,
                    status = appointment.appointmentStatus,
                    title = resources.getString(R.string.appointment_detail_status_appointment_fixed_title),
                    description = resources.getString(R.string.appointment_detail_status_appointment_fixed_desc)
                )
            )
            Log.e("TAG", Gson().toJson(appointment))
            appointmentViewModel.addAppointment(appointment)
            findNavController().navigate(AddNewAppointmentFragmentDirections.actionNavigationAddNewAppointmentToNavigationAppointment())
        }

    }

    private fun initSpinners(spinner: Spinner, list: ArrayList<String>) {
        val titles = ArrayList<String?>()
        for (cat in list) {
            titles.add(cat)
        }
        val listAdapter: ArrayAdapter<String?> = object : ArrayAdapter<String?>(
            requireContext(),
            android.R.layout.simple_spinner_item,
            android.R.id.text1, titles
        ) {
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                var v: View? = null
                v = super.getDropDownView(position, null, parent)
                // If this is the selected item position
                if (position == spinner.selectedItemPosition) {
                    v.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGrey))
                } else {
                    // for other views
                    v.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
                }
                return v
            }
        }
        listAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        with(spinner) {
            adapter = listAdapter
            isSelected = false
            setSelection(0, true)

            onItemSelectedListener = this@AddNewAppointmentFragment
        }
    }


    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RequestCode.Hospital -> {
                val searchListResult = data?.getSerializableExtra(Params.DATA) as SearchList

                searchListResult.defaultList!!.title.let {
                    hospitalTextTV.text = it
                    appointment.hospitalName = it
                }
            }
            RequestCode.Department -> {
                val searchListResult = data?.getSerializableExtra(Params.DATA) as SearchList

                searchListResult.defaultList!!.title.let {
                    medicalDepartmentTextTV.text = it
                    appointment.medicalDepartment = it
                }
            }
            RequestCode.HealthCare -> {
                val searchListResult = data?.getSerializableExtra(Params.DATA) as SearchList

                searchListResult.defaultList!!.title.let {
                    healthCareTextTV.text = it
                    appointment.professionalName = it
                }
            }
        }
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        when (p0?.id) {

//            R.id.hospitalSpinner -> {
//                appointment.hospitalName = hospitalSpinner.getItemAtPosition(p3.toInt()) as String?
//            }

            R.id.patientSpinner -> {
                appointment.patientName = patientSpinner.getItemAtPosition(p3.toInt()) as String?
            }
//            R.id.healthCareSpinner -> {
//                appointment.professionalName =
//                    healthCareSpinner.getItemAtPosition(p3.toInt()) as String?
//            }

            R.id.appointmentTypeSpinner -> {
                appointment.appointmentType = p3.toInt() + 1
            }
        }
    }
}


