package com.jfprojects.remotecare.ui.main.fragment

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.Appointment
import com.jfprojects.remotecare.database.models.AppointmentDetail
import com.jfprojects.remotecare.database.models.DefaultList
import com.jfprojects.remotecare.ui.main.adapters.AppointmentsListAdapter
import com.jfprojects.remotecare.ui.main.viewmodel.AppointmentViewModel
import kotlinx.android.synthetic.main.fragment_appointment_list.*
import org.joda.time.DateTime

class FutureAppointmentsFragment : Fragment() {
    private var selectedAppointmentDetailList: ArrayList<AppointmentDetail> = ArrayList()
    private var appointmentViewModel: AppointmentViewModel? = null
    private var appointmentsListAdapter: AppointmentsListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_appointment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appointmentViewModel = ViewModelProviders.of(this).get(AppointmentViewModel::class.java)

        appointmentViewModel?.getAppointments()
            ?.observe(viewLifecycleOwner, Observer { getAppointments(it) })

        appointmentListRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        bookNewAppointmentTV.visibility = View.VISIBLE

        bookNewAppointmentTV.setOnClickListener {
            findNavController().navigate(AppointmentFragmentDirections.actionNavigationAppointmentToNavigationAddNewAppointment())
        }
    }

    private fun getAppointments(it: List<Appointment?>?) {
        if (it?.size!! > 0) {
            initAppointmentAdapter(it as ArrayList<Appointment>)
        } else {
            noDataTV.visibility = View.VISIBLE
            appointmentListRV.visibility = View.INVISIBLE
        }
    }

    private fun initAppointmentAdapter(arrayList: ArrayList<Appointment>) {
        if (appointmentsListAdapter == null)
            appointmentsListAdapter = object : AppointmentsListAdapter() {
                override fun onActionClick(item: Appointment) {
                    super.onActionClick(item)
                    appointmentViewModel?.deleteAppointment(item.id)
                }

                override fun onStatusUpdate(item: Appointment) {
                    super.onStatusUpdate(item)
                    updateAppointment(item)
                }

                override fun onObjectClick(item: Appointment) {
                    super.onObjectClick(item)
                    findNavController().navigate(
                        AppointmentFragmentDirections.actionNavigationAppointmentToNavigationSingleAppointment(
                            data = item.id
                        )
                    )
                }
            }

        appointmentListRV.adapter = appointmentsListAdapter
        Handler().postDelayed(
            {
                appointmentsListAdapter!!.setData(arrayList)
            }
            , 100
        )
        appointmentListRV.visibility = View.VISIBLE
        noDataTV.visibility = View.GONE
    }

    private fun updateAppointment(item: Appointment) {
        if (item.appointmentStatus!! >= 7) {
            item.appointmentStatus = 1
        } else
            item.appointmentStatus = item.appointmentStatus?.plus(1)


        if (!item.appointmentDetailList.isNullOrEmpty()) {
            selectedAppointmentDetailList = ArrayList(item.appointmentDetailList!!)
            for (obj in item.appointmentDetailList!!) {
                if (obj.viewType == 2) {
                    selectedAppointmentDetailList.remove(obj)
                }
            }
            item.appointmentDetailList = selectedAppointmentDetailList
        }
        addAppointmentDetail(item)

        appointmentViewModel?.updateAppointmentStatus(appointment = item)
    }

    /*
    * 1- appointment Fixed
    * 2- waiting for doctor Prescription
    * 3- Doctor Uploaded Prescription
    * 4- Testing Lab Appointment Fixed
    * 5- Samples for test sent
    * 6- Tests Done
    * 7- Pharmacy ordered
    * 8- Self- Assessment Pending
    *
    * */
    private fun addAppointmentDetail(item: Appointment) {
        when (item.appointmentStatus) {
            1 -> {
                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_appointment_fixed_title),
                        description = resources.getString(R.string.appointment_detail_status_appointment_fixed_desc)
                    )
                )

            }
            2 -> {
                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_waiting_for_doc_title),
                        description = resources.getString(R.string.appointment_detail_status_waiting_for_doc_desc)
                    )
                )
            }
            3 -> {
                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_doctor_uploaded_prescription_title),
                        description = resources.getString(R.string.appointment_detail_status_doctor_uploaded_prescription_desc)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 2,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        defaultList = arrayListOf(
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_book_tests_option)),
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_order_medicines_option))
                        )

                    )
                )

            }
            4 -> {

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 4,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_tests_submitted_title_by_user),
                        description = resources.getString(R.string.appointment_detail_status_tests_submitted_desc_by_user)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_test_results_awaited_title),
                        description = resources.getString(R.string.appointment_detail_status_test_results_awaited_desc)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 2,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        defaultList = arrayListOf(
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_order_medicines_option)),
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_self_assessment_option))
                        )

                    )
                )

            }
            5 -> {

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 4,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_tests_submitted_title_by_user),
                        description = resources.getString(R.string.appointment_detail_status_tests_submitted_desc_by_user)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_test_results_awaited_title),
                        description = resources.getString(R.string.appointment_detail_status_test_results_awaited_desc)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 2,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        defaultList = arrayListOf(
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_order_medicines_option)),
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_self_assessment_option))
                        )

                    )
                )

            }
            6 -> {
                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_test_results_received_title),
                        description = resources.getString(R.string.appointment_detail_status_test_results_received_desc)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 2,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        defaultList = arrayListOf(
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_order_medicines_option))
                        )

                    )
                )

            }
            7 -> {

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 4,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_pharmacy_order_placed_title_by_user),
                        description = resources.getString(R.string.appointment_detail_status_pharmacy_order_placed_desc_by_user)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_pharmacy_order_placed_title),
                        description = resources.getString(R.string.appointment_detail_status_pharmacy_order_placed_desc)
                    )
                )
            }
            else -> {
                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_self_assessment_pending_title),
                        description = resources.getString(R.string.appointment_detail_status_self_assessment_pending_desc)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 2,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        defaultList = arrayListOf(
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_self_assessment_option))
                        )

                    )
                )


            }

        }
        item.appointmentDetailList = selectedAppointmentDetailList

    }


}

