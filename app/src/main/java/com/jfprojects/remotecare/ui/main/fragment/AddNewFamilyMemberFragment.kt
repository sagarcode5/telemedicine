package com.jfprojects.remotecare.ui.main.fragment

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.common.fragment.AbsNoNavParentFragment
import com.jfprojects.remotecare.database.models.APILogTable
import com.jfprojects.remotecare.database.models.FamilyMember
import com.jfprojects.remotecare.database.models.RelationType
import com.jfprojects.remotecare.helper.Params
import com.jfprojects.remotecare.helper.getFullDate
import com.jfprojects.remotecare.ui.main.viewmodel.FamilyMemberViewModel
import de.synexs.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import kotlinx.android.synthetic.main.fragment_add_new_family_member.*
import org.joda.time.DateTime
import java.util.*

class AddNewFamilyMemberFragment : AbsNoNavParentFragment(), AdapterView.OnItemSelectedListener {

    private val familyMemberViewModel by viewModels<FamilyMemberViewModel>()
    private lateinit var datePicker: SingleDateAndTimePickerDialog.Builder
    private var isExistingMember: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_new_family_member, container, false)
    }

    override fun apiLogSuccess(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun apiLogFailure(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        datePicker = SingleDateAndTimePickerDialog.Builder(context)

        if (requireArguments().containsKey(Params.DATA)) {
            isExistingMember = requireArguments().getBoolean(Params.DATA)
            familyMemberViewModel.familyMember.id = requireArguments().getInt(Params.ITEMS)
        }

        updateTV.setOnClickListener {
            validate()
        }

        dateTV.setOnClickListener {
            datePicker
                .displayAmPm(false)
                .defaultDate(familyMemberViewModel.selectedDob.toDate())
                .title(resources.getString(R.string.date_picker_title))
                .curved()
                .defaultDate(familyMemberViewModel.selectedDob.toDate())
                .minDateRange(DateTime.now().minusYears(100).toDate())
                .customLocale(Locale.getDefault())
                .displayDays(false)
                .displayDaysOfMonth(true)
                .displayMonth(true)
                .displayYears(true)
                .displayHours(false)
                .displayMinutes(false)
                .setTimeZone(Calendar.getInstance().timeZone)
                .mainColor(resources.getColor(R.color.colorPrimary))
                .titleTextColor(resources.getColor(R.color.white))
                .backgroundColor(resources.getColor(R.color.colorGrey))
                .listener { date: Date ->

                    familyMemberViewModel.setSelectedDate(date)
                    dateTV.text = getFullDate(date.time, 1)

                }.display()
        }

        if (familyMemberViewModel.familyMember.id > 0) {
            familyMemberViewModel.getFamilyMember()
                ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer { updateUI(it) })

        } else {

            val genderList = ArrayList<String>()
            resources.getStringArray(R.array.gender)
                .forEach { genderList.add(it) }
            initSpinners(genderSpinner, genderList)

            val relationList = ArrayList<String>()
            RelationType.values().forEach { relationList.add(it.name) }
            initSpinners(relationSpinner, relationList)

        }
    }

    private fun updateUI(it: FamilyMember) {
        familyMemberViewModel.familyMember = it
        val relationList = ArrayList<String>()
        RelationType.values().forEach { relationList.add(it.name) }

        val genderList = ArrayList<String>()
        resources.getStringArray(R.array.gender)
            .forEach { genderList.add(it) }

        if (!it.firstName.isNullOrEmpty()) {
            firstNameET.setText(it.firstName)
        }

        if (!it.lastName.isNullOrEmpty()) {
            lastNameET.setText(it.lastName)
        }

        if (!it.mobile.isNullOrEmpty()) {
            mobileET.setText(it.mobile)
        }

        if (!it.gender.isNullOrEmpty()) {
            familyMemberViewModel.selectedGender = it.gender!!
            initSpinners(genderSpinner, genderList, it.gender)
        } else {
            initSpinners(genderSpinner, genderList)
        }

        if (!it.relation.isNullOrEmpty()) {
            familyMemberViewModel.selectedRelation = it.relation!!
            initSpinners(relationSpinner, relationList, it.relation)
        } else {
            initSpinners(relationSpinner, relationList)
        }

        if (!it.aadhar.isNullOrEmpty()) {
            aadharET.setText(it.aadhar)
        }

        if (it.dob > 0) {
            familyMemberViewModel.selectedDob = familyMemberViewModel.selectedDob.withMillis(it.dob)
            dateTV.text = getFullDate(familyMemberViewModel.selectedDob.millis, 1)
        }
    }

    private fun initSpinners(spinner: Spinner, list: ArrayList<String>) {
        initSpinners(spinner, list, null)
    }

    private fun initSpinners(spinner: Spinner, list: ArrayList<String>, selectedItem: String?) {
        val titles = ArrayList<String?>()
        var selectedPos = 0

        for (i in 0 until list.size) {
            titles.add(list[i])
            if (selectedItem == list[i]) {
                selectedPos = i
            }
        }
        val listAdapter: ArrayAdapter<String?> = object : ArrayAdapter<String?>(
            requireContext(),
            android.R.layout.simple_spinner_item,
            android.R.id.text1, titles
        ) {
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                var v: View? = null
                v = super.getDropDownView(position, null, parent)
                // If this is the selected item position
                if (position == spinner.selectedItemPosition) {
                    v.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGrey))
                } else {
                    // for other views
                    v.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
                }
                return v
            }
        }
        listAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        with(spinner) {
            adapter = listAdapter
            isSelected = false
            setSelection(selectedPos, true)

            onItemSelectedListener = this@AddNewFamilyMemberFragment
        }
    }


    private fun validate() {
        var isDataChanged = false
        val familyMember = familyMemberViewModel.familyMember

        if (TextUtils.isEmpty(firstNameET.text)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "First Name"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        if (TextUtils.isEmpty(lastNameET.text)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "Last Name"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        if (TextUtils.isEmpty(mobileET.text)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "Mobile"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        if (TextUtils.isEmpty(aadharET.text)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "Aadhar"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        if (TextUtils.isEmpty(familyMemberViewModel.selectedGender)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "Gender"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        if (TextUtils.isEmpty(dateTV.text)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "DOB"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        if (TextUtils.isEmpty(familyMemberViewModel.selectedRelation)) {
            Toast.makeText(
                context,
                String.format(resources.getString(R.string.profile_error_profile), "Relation"),
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        if (isExistingMember) {
            if (!familyMember.firstName.isNullOrEmpty() && (familyMember.firstName!! != firstNameET.text.toString())) {

                isDataChanged = true
            }

            if (!familyMember.lastName.isNullOrEmpty() && familyMember.lastName!! != lastNameET.text.toString()) {

                isDataChanged = true
            }

            if (!familyMember.mobile.isNullOrEmpty() && familyMember.mobile!! != mobileET.text.toString()) {

                isDataChanged = true
            }

            if (!familyMember.aadhar.isNullOrEmpty() && familyMember.aadhar!! != aadharET.text.toString()) {

                isDataChanged = true
            }

            if (!familyMember.gender.isNullOrEmpty() && familyMember.gender!! != familyMemberViewModel.selectedGender) {

                isDataChanged = true
            }

            if (!familyMember.relation.isNullOrEmpty() && familyMember.relation!! != familyMemberViewModel.selectedRelation) {

                isDataChanged = true
            }

            if (familyMember.dob > 0 && familyMember.dob != familyMemberViewModel.selectedDob.millis) {

                isDataChanged = true
            }

            if (isDataChanged) {
                familyMember.firstName = firstNameET.text.toString()
                familyMember.lastName = lastNameET.text.toString()
                familyMember.mobile = mobileET.text.toString()
                familyMember.aadhar = aadharET.text.toString()

                familyMemberViewModel.updateFamilyMember(familyMember)

                findNavController().navigateUp()
            } else findNavController().navigateUp()

        } else {
            familyMember.firstName = firstNameET.text.toString()
            familyMember.lastName = lastNameET.text.toString()
            familyMember.mobile = mobileET.text.toString()
            familyMember.aadhar = aadharET.text.toString()

            familyMemberViewModel.addFamilyMembers(familyMember)

            findNavController().navigateUp()
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        when (p0?.id) {

            R.id.genderSpinner -> {
                familyMemberViewModel.selectedGender =
                    genderSpinner.getItemAtPosition(p3.toInt()) as String
            }
            R.id.relationSpinner -> {
                familyMemberViewModel.selectedRelation =
                    relationSpinner.getItemAtPosition(p3.toInt()) as String
            }
        }
    }
}
