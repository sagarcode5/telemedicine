package com.jfprojects.remotecare.ui.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jfprojects.remotecare.helper.getFullDate
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.PharmacyOrder
import kotlinx.android.synthetic.main.single_item_pharmacy_order_item.view.*
import org.joda.time.DateTime
import java.util.*

open class PharmacyOrderListAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var pharmacyOrderList: ArrayList<PharmacyOrder> = ArrayList()

    open fun onActionClick(
        item: PharmacyOrder
    ) {

    }

    open fun onObjectClick(
        item: PharmacyOrder
    ) {

    }

    open fun onStatusUpdate(
        item: PharmacyOrder
    ) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.single_item_pharmacy_order_item, parent, false)
        return MyViewHolder(v)
    }


    fun setData(list: ArrayList<PharmacyOrder>) {
        pharmacyOrderList = ArrayList()
        pharmacyOrderList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        (viewHolder as MyViewHolder).bind(pharmacyOrderList[i])
    }

    override fun getItemCount(): Int {
        return pharmacyOrderList.size
    }

    private inner class MyViewHolder(v: View) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: PharmacyOrder) = with(itemView) {

            itemView.setOnClickListener { onObjectClick(item) }
            pharmacyNameTV.text = item.pharmacyName
            orderTypeTV.text = when (item.pharmacyOrderType) {
                1 -> itemView.resources.getString(R.string.pharmacy_order_list_type_home_delivery)
                else -> itemView.resources.getString(R.string.pharmacy_order_list_type_pick_up)
            }

            /*
            * 1- pharmacy order placed
            * 2- waiting for order delivery
            * 3- medicine delivered
            *
            * */

            statusTV.apply {
                when (item.pharmacyOrderStatus) {
                    1 -> {
                        text =
                            itemView.resources.getString(R.string.pharmacy_order_list_status_order_booked)
                        setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorResponseReceived
                            )
                        )
                    }

                    2 -> {
                        text =
                            itemView.resources.getString(R.string.pharmacy_order_list_status_waiting_for_delivery)
                        setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorWaitingForResponse
                            )
                        )
                    }

                    3 -> {
                        text =
                            itemView.resources.getString(R.string.pharmacy_order_list_status_medicines_delivered)
                        setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorResponseReceived
                            )
                        )
                    }

                }
            }

            var dateTime = DateTime.now()
            dateTime = dateTime.withMillis(item.dateTime!!)

            dateTV.text = getFullDate(dateTime.millis, 2)
            timeTV.text = getFullDate(dateTime.millis, 3)
            symptomsTV.text = item.comments

//            deleteCL.setOnClickListener { onActionClick(item) }
            imageView3.setOnClickListener { onStatusUpdate(item) }
        }

    }

}