package com.jfprojects.remotecare.ui.splash.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.db.ActionName
import com.jfprojects.remotecare.database.models.db.SymptomOptions
import kotlinx.android.synthetic.main.single_item_detail_appointment_view_user_option_single_item.view.*

open class OptionListAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var symptomOptions: ArrayList<SymptomOptions> = ArrayList()

    open fun onObjectClick(
        item: SymptomOptions
    ) {

    }

    open fun addInSelection(
        item: SymptomOptions
    ) {

    }

    open fun onNextClick(
        item: SymptomOptions
    ) {

    }

    open fun removeInSelection(
        item: SymptomOptions
    ) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(
                R.layout.single_item_user_option_single_item,
                parent,
                false
            )


        return MyViewHolderSingleSelection(v)

    }

    fun setData(list: ArrayList<SymptomOptions>) {
        symptomOptions = ArrayList()
        symptomOptions = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        (viewHolder as MyViewHolderSingleSelection).bind(symptomOptions[i])
    }

    override fun getItemCount(): Int {
        return symptomOptions.size
    }

    private inner class MyViewHolderSingleSelection(v: View) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: SymptomOptions) = with(itemView) {
            titleTV.text = item.name

            when (item.isSelected) {
                true -> {
                    titleTV.setTextColor(
                        ContextCompat.getColor(
                            itemView.context,
                            R.color.white
                        )
                    )
                    titleTV.background =
                        ContextCompat.getDrawable(
                            itemView.context,
                            R.drawable.bg_solid_primary_corner
                        )
                }
                false -> {
                    titleTV.setTextColor(
                        ContextCompat.getColor(
                            itemView.context,
                            R.color.colorPrimary
                        )
                    )
                    titleTV.background =
                        ContextCompat.getDrawable(
                            itemView.context,
                            R.drawable.bg_solid_white_corner_primary
                        )
                }
            }

            if (item.name == ActionName.Next.str) {
                titleTV.setTextColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.white
                    )
                )
                titleTV.background =
                    ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.bg_solid_white_corner_black
                    )
            }

            itemView.setOnClickListener {
                if (item.name == ActionName.NoneOfTheAbove.str || item.name == ActionName.Next.str) {
                    onNextClick(item)
                } else if (item.isSelected) {
                    item.isSelected = false
                    removeInSelection(item)
                } else {
                    item.isSelected = true
                    addInSelection(item)
                }
            }
        }

    }

}