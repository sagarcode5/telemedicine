package com.jfprojects.remotecare.ui.main.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.common.fragment.AbsNavParentFragment
import com.jfprojects.remotecare.database.models.APILogTable

class MedRecordFragment : AbsNavParentFragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_med_record, container, false)
    }

    override fun apiLogSuccess(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun apiLogFailure(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}
