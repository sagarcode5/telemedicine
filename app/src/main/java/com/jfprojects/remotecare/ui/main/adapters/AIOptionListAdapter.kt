package com.jfprojects.remotecare.ui.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.DefaultList
import kotlinx.android.synthetic.main.single_item_detail_appointment_view_user_option_single_item.view.*

open class AIOptionListAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var defaultList: ArrayList<DefaultList> = ArrayList()

    open fun onActionClick(
        item: DefaultList
    ) {

    }

    open fun onObjectClick(
        item: DefaultList
    ) {

    }

    open fun onStatusUpdate(
        item: DefaultList
    ) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(
                R.layout.single_item_detail_appointment_view_user_option_single_item,
                parent,
                false
            )


        return MyViewHolderSingleSelection(v)

    }

    fun setData(list: ArrayList<DefaultList>) {
        defaultList = ArrayList()
        defaultList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        (viewHolder as MyViewHolderSingleSelection).bind(defaultList[i])
    }


    override fun getItemCount(): Int {
        return defaultList.size
    }

    private inner class MyViewHolderSingleSelection(v: View) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: DefaultList) = with(itemView) {
            titleTV.text = item.title

            itemView.setOnClickListener {
                onObjectClick(item)
            }
        }

    }

}