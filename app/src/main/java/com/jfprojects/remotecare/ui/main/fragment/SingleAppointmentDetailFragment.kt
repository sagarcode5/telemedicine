package com.jfprojects.remotecare.ui.main.fragment

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jfprojects.remotecare.helper.KEY_DATA
import com.jfprojects.remotecare.helper.getFullDate
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.common.fragment.AbsNoNavParentFragment
import com.jfprojects.remotecare.database.models.APILogTable
import com.jfprojects.remotecare.database.models.Appointment
import com.jfprojects.remotecare.database.models.AppointmentDetail
import com.jfprojects.remotecare.database.models.DefaultList
import com.jfprojects.remotecare.helper.views.SpeedyLinearLayoutManager
import com.jfprojects.remotecare.ui.main.adapters.AppointmentDetailListAdapter
import com.jfprojects.remotecare.ui.main.viewmodel.AppointmentViewModel
import kotlinx.android.synthetic.main.fragment_single_appointment_detail.*
import org.joda.time.DateTime

class SingleAppointmentDetailFragment : AbsNoNavParentFragment() {
    private var selectedAppointmentDetailList: ArrayList<AppointmentDetail> = ArrayList()
    private lateinit var selectedAppointment: Appointment
    private var appointmentViewModel: AppointmentViewModel? = null
    private var appointmentsListAdapter: AppointmentDetailListAdapter? = null
    private var statusUpdate: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_single_appointment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        appointmentViewModel = ViewModelProviders.of(this).get(AppointmentViewModel::class.java)

        listRV.layoutManager =
            SpeedyLinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

    }

    override fun onResume() {
        super.onResume()
        appointmentViewModel?.getAppointment(requireArguments().getInt(KEY_DATA))
            ?.observe(this, Observer { initUIValues(it) })
    }

    override fun apiLogSuccess(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun apiLogFailure(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    private fun getAppointmentDetails(it: List<AppointmentDetail?>?) {
        if (it?.size!! > 0) {
            initAppointmentAdapter(it as ArrayList<AppointmentDetail>)
        } else {
//            noDataTV.visibility = View.VISIBLE
            listRV.visibility = View.INVISIBLE
        }
    }

    private fun initAppointmentAdapter(arrayList: ArrayList<AppointmentDetail>) {
        if (appointmentsListAdapter == null) {
            appointmentsListAdapter = object : AppointmentDetailListAdapter() {
                override fun onSingleOptionSelection(
                    item: AppointmentDetail,
                    selectedButton: String
                ) {
                    super.onSingleOptionSelection(item, selectedButton)
//                    selectedAppointment.appointmentDetailList!!.remove(item)
                    when (selectedButton) {

                        resources.getString(R.string.appointment_detail_status_book_tests_option) -> {
                            findNavController().navigate(
                                SingleAppointmentDetailFragmentDirections.actionNavigationSingleAppointmentToNavigationAddNewTest(
                                    selectedAppointment.id
                                )
                            )
//                            updateAppointment(
//                                selectedAppointment, 4
//                            )

                        }
                        resources.getString(R.string.appointment_detail_status_self_assessment_option) -> {
                            selectedAppointment.appointmentStatus = 8
                            updateAppointment(
                                selectedAppointment, 0
                            )
                        }

                        else -> {
                            findNavController().navigate(
                                SingleAppointmentDetailFragmentDirections.actionNavigationSingleAppointmentToNavigationAddNewPharmacyOrder(
                                    selectedAppointment.id
                                )
                            )
//                            updateAppointment(
//                            selectedAppointment, 6
//                        )
                        }
                    }
                }
            }
        }

        listRV.adapter = appointmentsListAdapter
        Handler().postDelayed(
            {
                appointmentsListAdapter!!.setData(arrayList)
                listRV.smoothScrollToPosition(arrayList.size - 1)

            }
            , 100
        )

        listRV.visibility = View.VISIBLE
    }

    private fun initUIValues(appointment: Appointment) {
        selectedAppointment = appointment
        selectedAppointmentDetailList = appointment.appointmentDetailList!!
        if (statusUpdate) {
            if (appointmentsListAdapter != null) {
                appointmentsListAdapter!!.notifyDataSetChanged()
                appointmentsListAdapter!!.setData(selectedAppointmentDetailList)
            }
            if (!selectedAppointmentDetailList.isNullOrEmpty())
                if (selectedAppointmentDetailList.size > 1)
                    listRV.smoothScrollToPosition(selectedAppointmentDetailList.size - 1)
                else
                    getAppointmentDetails(selectedAppointmentDetailList)
        } else {
            statusUpdate = false

            patientNameTV.text = appointment.patientName
            professionalNameTV.text = String.format(
                "%s (%s)",
                appointment.professionalName,
                appointment.medicalDepartment
            )
            hospitalNameTV.text = appointment.hospitalName
            symptomsTV.text = appointment.commentSymptoms
            var dateTime = DateTime.now()
            dateTime = dateTime.withMillis(appointment.dateTime!!)

            dateTimeTV.text = getFullDate(dateTime.millis, 4)

            getAppointmentDetails(selectedAppointmentDetailList)

            appointmentTypeTV.text = when (appointment.appointmentType) {
                1 -> resources.getString(R.string.appointment_list_time_appointment_type_telehealth)
                2 -> resources.getString(R.string.appointment_list_time_appointment_type_follow_up_telehealth)
                3 -> resources.getString(R.string.appointment_list_time_appointment_type_video)
                4 -> resources.getString(R.string.appointment_list_time_appointment_type_home_care)
                else -> resources.getString(R.string.appointment_list_time_appointment_type_other)
            }

            dateTimeTV.setOnClickListener {
                updateAppointment(
                    item = selectedAppointment,
                    status = 0
                )
            }
        }
    }

    private fun updateAppointment(item: Appointment, status: Int) {
        if (status == 0) {
            if (item.appointmentStatus!! >= 8) {
                item.appointmentStatus = 1
                selectedAppointmentDetailList.clear()
            } else
                item.appointmentStatus = item.appointmentStatus?.plus(1)
        } else item.appointmentStatus = status

        if (!item.appointmentDetailList.isNullOrEmpty()) {
            selectedAppointmentDetailList = item.appointmentDetailList!!
            for (obj in item.appointmentDetailList!!) {
                if (obj.viewType == 2) {
                    item.appointmentDetailList!!.remove(obj)
                }
            }
            item.appointmentDetailList = selectedAppointmentDetailList
        }

        addAppointmentDetail(item)
        statusUpdate = true

        appointmentViewModel?.updateAppointmentStatus(appointment = item)
    }

/*

    private fun updateAppointment(item: Appointment, status: Int) {
        if (status == 0) {
            if (item.appointmentStatus!! >= 7) {
                item.appointmentStatus = 1
                selectedAppointmentDetailList.clear()
            } else
                item.appointmentStatus = item.appointmentStatus?.plus(1)
        } else item.appointmentStatus = status

        if (!item.appointmentDetailList.isNullOrEmpty() && item.appointmentDetailList!![item.appointmentDetailList!!.size - 1].viewType == 2
//            || item.appointmentDetailList!![item.appointmentDetailList!!.size - 1].viewType == 3
        ) {
            item.appointmentDetailList!!.removeAt(item.appointmentDetailList!!.size - 1)
        }
        addAppointmentDetail(item)

        statusUpdate = true

        appointmentViewModel?.updateAppointmentStatus(appointment = item)
    }
*/

    /*
    * 1- appointment Fixed
    * 2- waiting for doctor Prescription
    * 3- Doctor Uploaded Prescription
    * 4- Testing Lab Appointment Fixed
    * 5- Samples for test sent
    * 6- Tests Done
    * 7- Pharmacy ordered
    * 8- Self- Assessment Pending
    *
    * */
    private fun addAppointmentDetail(item: Appointment) {
        when (item.appointmentStatus) {
            1 -> {
                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_appointment_fixed_title),
                        description = resources.getString(R.string.appointment_detail_status_appointment_fixed_desc)
                    )
                )

            }
            2 -> {
                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_waiting_for_doc_title),
                        description = resources.getString(R.string.appointment_detail_status_waiting_for_doc_desc)
                    )
                )
            }
            3 -> {
                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_doctor_uploaded_prescription_title),
                        description = resources.getString(R.string.appointment_detail_status_doctor_uploaded_prescription_desc)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 2,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        defaultList = arrayListOf(
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_book_tests_option)),
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_order_medicines_option))
                        )

                    )
                )

            }
            4 -> {

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 4,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_tests_submitted_title_by_user),
                        description = resources.getString(R.string.appointment_detail_status_tests_submitted_desc_by_user)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_test_results_awaited_title),
                        description = resources.getString(R.string.appointment_detail_status_test_results_awaited_desc)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 2,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        defaultList = arrayListOf(
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_order_medicines_option)),
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_self_assessment_option))
                        )

                    )
                )

            }
            5 -> {

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 4,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_tests_submitted_title_by_user),
                        description = resources.getString(R.string.appointment_detail_status_tests_submitted_desc_by_user)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_test_results_awaited_title),
                        description = resources.getString(R.string.appointment_detail_status_test_results_awaited_desc)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 2,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        defaultList = arrayListOf(
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_order_medicines_option)),
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_self_assessment_option))
                        )

                    )
                )

            }
            6 -> {
                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_test_results_received_title),
                        description = resources.getString(R.string.appointment_detail_status_test_results_received_desc)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 2,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        defaultList = arrayListOf(
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_order_medicines_option))
                        )

                    )
                )

            }
            7 -> {

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 4,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_pharmacy_order_placed_title_by_user),
                        description = resources.getString(R.string.appointment_detail_status_pharmacy_order_placed_desc_by_user)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_pharmacy_order_placed_title),
                        description = resources.getString(R.string.appointment_detail_status_pharmacy_order_placed_desc)
                    )
                )
            }
            else -> {
                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 1,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        title = resources.getString(R.string.appointment_detail_status_self_assessment_pending_title),
                        description = resources.getString(R.string.appointment_detail_status_self_assessment_pending_desc)
                    )
                )

                selectedAppointmentDetailList.add(
                    AppointmentDetail(
                        viewType = 2,
                        dateTime = DateTime.now().millis,
                        status = item.appointmentStatus,
                        defaultList = arrayListOf(
                            DefaultList(title = resources.getString(R.string.appointment_detail_status_self_assessment_option))
                        )

                    )
                )


            }

        }
        item.appointmentDetailList = selectedAppointmentDetailList

    }


}


