package com.jfprojects.remotecare.ui.splash.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jfprojects.remotecare.helper.QuestionAIInteractionType
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.DefaultList
import kotlinx.android.synthetic.main.single_item_question_ai_question_item.view.*
import java.util.*

open class QuestionsListAdapter_old :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var defaultList: ArrayList<DefaultList> = ArrayList()

    open fun onActionClick(
        pos: Int,
        item: DefaultList
    ) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        return when (i) {
            1 -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.single_item_question_ai_question_item, parent, false)
                MyViewHolderAIQuestion(v)
            }
            2 -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.single_item_question_user_answer_item, parent, false)
                MyViewHolderUserAnswer(v)
            }
            else -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.single_item_question_ai_option_item, parent, false)
                MyViewHolderAIOption(v)
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return when {
            defaultList[position].type.equals(QuestionAIInteractionType.TYPE_AIQuestion) -> 1
            defaultList[position].type.equals(QuestionAIInteractionType.TYPE_UserAnswer) -> 2
            else -> 3
        }
    }

    fun setData(list: ArrayList<DefaultList>) {
        defaultList = ArrayList()
        defaultList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        when (getItemViewType(i)) {
            1 -> (viewHolder as MyViewHolderAIQuestion).bind(defaultList[i])
            2 -> (viewHolder as MyViewHolderUserAnswer).bind(defaultList[i])
            else -> (viewHolder as MyViewHolderAIOption).bind(defaultList[i])
        }
    }

    override fun getItemCount(): Int {
        return defaultList.size
    }

    private inner class MyViewHolderAIQuestion(v: View) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: DefaultList) = with(itemView) {
            titleTV.text = item.title

        }

    }

    private inner class MyViewHolderUserAnswer(v: View) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: DefaultList) = with(itemView) {
            titleTV.text = item.title

        }

    }

    private inner class MyViewHolderAIOption(v: View) :
        RecyclerView.ViewHolder(v) {

        fun bind(item: DefaultList) = with(itemView) {
            titleTV.text = item.title
            titleTV.setOnClickListener { onActionClick(adapterPosition, item) }
        }

    }

}