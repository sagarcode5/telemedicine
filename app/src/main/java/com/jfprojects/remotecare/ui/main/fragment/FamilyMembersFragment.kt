package com.jfprojects.remotecare.ui.main.fragment

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.common.fragment.AbsNoNavParentFragment
import com.jfprojects.remotecare.database.models.APILogTable
import com.jfprojects.remotecare.database.models.FamilyMember
import com.jfprojects.remotecare.ui.main.adapters.FamilyMemberListAdapter
import com.jfprojects.remotecare.ui.main.viewmodel.FamilyMemberViewModel
import kotlinx.android.synthetic.main.fragment_family_members.*

class FamilyMembersFragment : AbsNoNavParentFragment() {
    private val familyMemberViewModel by viewModels<FamilyMemberViewModel>()
    private var familyMemberListAdapter: FamilyMemberListAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_family_members, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        familyMemberViewModel.getFamilyMembers()
            ?.observe(viewLifecycleOwner, Observer { getFamilyMembers(it) })

        listRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        addNewMemberTV.setOnClickListener {
            findNavController().navigate(
                FamilyMembersFragmentDirections.actionNavigationFamilyToNavigationAddNewFamily(
                    false
                )
            )
        }
    }

    override fun apiLogSuccess(table: APILogTable?) {
        TODO("Not yet implemented")
    }

    override fun apiLogFailure(table: APILogTable?) {
        TODO("Not yet implemented")
    }


    private fun getFamilyMembers(it: List<FamilyMember?>?) {
        if (it?.size!! > 0) {
            initAdapter(it as ArrayList<FamilyMember>)
        } else {
            noDataTV.visibility = View.VISIBLE
            listRV.visibility = View.INVISIBLE
        }
    }

    private fun initAdapter(arrayList: ArrayList<FamilyMember>) {
        if (familyMemberListAdapter == null)
            familyMemberListAdapter = object : FamilyMemberListAdapter() {
                override fun onObjectClick(item: FamilyMember) {
                    super.onObjectClick(item)
                    findNavController().navigate(
                        FamilyMembersFragmentDirections.actionNavigationFamilyToNavigationAddNewFamily(
                            true,
                            item.id
                        )
                    )

                }

                override fun onActionClick(item: FamilyMember) {
                    super.onActionClick(item)

                }
            }

        listRV.adapter = familyMemberListAdapter
        Handler().postDelayed(
            {
                familyMemberListAdapter!!.setData(arrayList)
            }
            , 100
        )
        listRV.visibility = View.VISIBLE
        noDataTV.visibility = View.GONE
    }


}
