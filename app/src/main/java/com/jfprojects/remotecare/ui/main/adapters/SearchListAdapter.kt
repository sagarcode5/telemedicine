package com.jfprojects.remotecare.ui.main.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.SearchList
import kotlinx.android.synthetic.main.single_item_search_default.view.*

open class SearchListAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var searchList: ArrayList<SearchList> = ArrayList()

    open fun onObjectClick(
        item: SearchList
    ) {

    }

    open fun setData(list: ArrayList<SearchList>) {
        searchList = ArrayList()
        searchList = list
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        Log.e("tag", position.toString())
        return if (position < searchList.size) searchList[position].viewType else 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        Log.e("tag onCreateViewHolder", i.toString())

        return when (i) {
            1, 2, 3, 4 -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.single_item_search_default, parent, false)
                MyViewHolderDefaultList(v)
            }
            else -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.single_item_search_default, parent, false)
                MyViewHolderDefaultList(v)
            }
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        Log.e("tag onCreateViewHolder", i.toString())
        when (getItemViewType(i)) {
            1, 2, 3, 4 -> (viewHolder as MyViewHolderDefaultList).bind(searchList[i], i)
        }
    }

    override fun getItemCount(): Int {
        return searchList.size
    }

    internal inner class MyViewHolderDefaultList(v: View) :
        RecyclerView.ViewHolder(v) {
        fun bind(item: SearchList, i: Int) = with(itemView) {
            when (getItemViewType(i)) {
                //Pharmacy
                1 -> {
                    descTV.visibility = View.GONE
                    desc2TV.visibility = View.GONE
                    nameTV.text = item.defaultList?.title
                    itemView.setOnClickListener {
                        onObjectClick(item)
                    }
                }
                // Hospital
                2 -> {
                    desc2TV.visibility = View.GONE
                    descTV.visibility = View.VISIBLE
                    nameTV.text = item.defaultList?.title
                    descTV.text = item.defaultList?.message
                    itemView.setOnClickListener {
                        onObjectClick(item)
                    }
                }

                3 -> {
                    descTV.visibility = View.VISIBLE
                    desc2TV.visibility = View.VISIBLE
                    nameTV.text = String.format(
                        "%s - %s",
                        item.defaultList?.title,
                        item.defaultList?.subMessage
                    )

                    desc2TV.setTextColor(
                        ContextCompat.getColor(
                            itemView.context,
                            R.color.colorResponseReceived
                        )
                    )

                    descTV.text = String.format("%s yrs of Exp", item.defaultList?.message)
                    desc2TV.text = String.format("Fees: ₹%s", item.defaultList?.count)
                    itemView.setOnClickListener {
                        onObjectClick(item)
                    }
                }

                4 -> {
                    descTV.visibility = View.VISIBLE
                    desc2TV.visibility = View.VISIBLE
                    nameTV.text = String.format(
                        "%s",
                        item.defaultList?.title
                    )

                    desc2TV.setTextColor(
                        ContextCompat.getColor(
                            itemView.context,
                            R.color.colorResponseReceived
                        )
                    )

                    descTV.text = String.format("%s", item.defaultList?.message)
                    desc2TV.text =
                        String.format("%s Professional Available", item.defaultList?.count)
                    itemView.setOnClickListener {
                        onObjectClick(item)
                    }
                }
            }

        }
    }
}