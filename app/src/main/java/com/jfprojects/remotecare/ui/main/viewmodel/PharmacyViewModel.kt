package com.jfprojects.remotecare.ui.main.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.jfprojects.remotecare.database.models.Appointment
import com.jfprojects.remotecare.database.models.PharmacyOrder
import com.jfprojects.remotecare.database.repository.AppointmentRepository
import com.jfprojects.remotecare.database.repository.PharmacyRepository

class PharmacyViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: PharmacyRepository? = null
    private var appointmentRepository: AppointmentRepository? = null

    init {
        repository = PharmacyRepository(application)
        appointmentRepository = AppointmentRepository(application)
    }

    fun getPharmacyOrders() = repository?.getPharmacyOrders()
    fun getAppointment(id: Int) = appointmentRepository?.getAppointment(id)

    fun addPharmacyOrder(pharmacyOrder: PharmacyOrder) =
        repository?.addPharmacyOrders(pharmacyOrder)


    //    fun deleteAppointment(id: Int) = repository?.deleteAppointment(id)
    fun updateAppointmentStatus(appointment: Appointment) =
        appointmentRepository?.updateAppointment(appointment)
//
//    fun addAppointments(appointment: ArrayList<Appointment>) =
//        repository?.addAppointments(appointment, 1)


}