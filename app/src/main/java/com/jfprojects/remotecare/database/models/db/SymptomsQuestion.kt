package com.jfprojects.remotecare.database.models.db

import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.jfprojects.remotecare.database.converters.OptionsConverter
import java.io.Serializable

class SymptomsQuestion(
    @field:SerializedName("refid")
    var refId: Int = 0,

    @field:SerializedName("question")
    var question: String? = null,

    @field:SerializedName("id")
    var id: Int = 0,

    @field:SerializedName("questiontype")
    var questionType: String? = null,

    @field:SerializedName("question_displaytype")
    var questionDisplayType: String? = null,

    @field:SerializedName("responsetype")
    var responseType: Boolean = false,

    @field:SerializedName("credit")
    var credit: Int = 0,

    @field:SerializedName("status")
    var status: Boolean = false,

    @TypeConverters(OptionsConverter::class)
    @field:SerializedName("options")
    var options: ArrayList<SymptomOptions> = ArrayList()

) : ParentModel(), Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is SymptomsQuestion) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }
}

enum class QuestionDisplayType(val str: String) {
    MultiSelect("multiSelect"), SingleSelect("singleSelect")
}

enum class QuestionType {
    Category, SubCategory
}