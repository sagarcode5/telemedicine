package com.jfprojects.remotecare.database.models

import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AppointmentDetail(

    @SerializedName("dateText")
    var dateText: String? = null,

    @SerializedName("dateTime")
    var dateTime: Long? = 0,

    /*
    * 1- appointment Fixed
    * 2- waiting for doctor Prescription
    * 3- Doctor Uploaded Prescription
    * 4- Testing Lab Appointment Fixed
    * 5- Samples for test sent
    * 6- Tests Done
    * 7- Pharmacy ordered
    * 8- Self- Assessment Pending
    *
    * */
    @SerializedName("status")
    var status: Int? = 0,

    @SerializedName("title")
    var title: String? = null,

    @TypeConverters(DefaultList::class)
    @SerializedName("defaultList")
    var defaultList: ArrayList<DefaultList>? = null,

    /*
    * 1- AI status provided
    * 2- single selection from the answer options
    * 3- Multiple selection from the the answer options
    * 4- User answer to be displayed
    *
    * */
    @SerializedName("viewType")
    var viewType: Int? = null,

    @SerializedName("description")
    var description: String? = null
) : Serializable