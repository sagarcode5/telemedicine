package com.jfprojects.remotecare.database.repository

import android.app.Application
import com.jfprojects.remotecare.database.AppDatabase
import com.jfprojects.remotecare.database.dao.APILogDao
import com.jfprojects.remotecare.database.dao.LabTestsDao
import com.jfprojects.remotecare.database.models.LabTest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class LabTestsRepository(application: Application?) : CoroutineScope {
    private val TAG =
        LabTestsRepository::class.java.simpleName

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    private var apiLogDao: APILogDao? = null
    private var labTestsDao: LabTestsDao? = null

    init {
        val db = AppDatabase.getDatabase(application!!)
        apiLogDao = db?.apiLogDao()
        labTestsDao = db?.labTestDao()
    }

    fun getLabTests() = labTestsDao?.getLabTests()
//    fun getAppointment(id: Int) = appointmentDao?.getAppointment(id)

    fun addLabTests(labTest: LabTest) {
        launch { addLabTestToDb(labTest) }
    }

//    fun addAppointments(appointment: ArrayList<Appointment>, type: Int) {
//        launch { addAppointmentToDb(appointment, type) }
//    }


//    fun deleteAppointment(id: Int) {
//        launch { deleteAppointmentFromDb(id) }
//    }


    fun updateLabTest(labTest: LabTest) {
        launch { updateLabTestFromDb(labTest) }
    }


    private suspend fun addLabTestToDb(
        labTest: LabTest
    ) {
        withContext(Dispatchers.IO) {
            labTestsDao?.insert(labTest)
        }
    }

    private suspend fun deleteLabTestFromDb(
        id: Int
    ) {
        withContext(Dispatchers.IO) {
            labTestsDao?.deleteLabTests(id)
        }
    }

    private suspend fun updateLabTestFromDb(
        labTest: LabTest
    ) {
        withContext(Dispatchers.IO) {
            labTestsDao?.updateLabTest(labTest)
        }
    }

}