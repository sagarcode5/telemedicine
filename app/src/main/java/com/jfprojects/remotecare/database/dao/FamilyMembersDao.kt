package com.jfprojects.remotecare.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.jfprojects.remotecare.database.models.FamilyMember

@Dao
public interface FamilyMembersDao: BaseDao<FamilyMember> {
    /*
     *
     * All FamilyMember Table CRUD
     *
     * */

    @Query("SELECT * from family_member_table")
    fun getFamilyMembers(): LiveData<List<FamilyMember?>?>?

    @Query("SELECT * from family_member_table where id=:id LIMIT 1")
        fun getFamilyMember(id: Int): LiveData<FamilyMember>

    @Query("DELETE FROM family_member_table where id=:id")
    fun deleteFamilyMembers(id: Int)

}