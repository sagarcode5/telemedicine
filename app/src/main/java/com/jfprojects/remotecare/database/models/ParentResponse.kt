package com.jfprojects.remotecare.database.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ParentResponse(

    @SerializedName("headers")
    var headers: Headers? = null,

    @SerializedName("body")
    var body: String? = null,

    @SerializedName("statusCode")
    var statusCode: Int = 0

) : Serializable

data class Headers(

    @SerializedName("xCustomHeader")
    var xCustomHeader: String? = null,

    @SerializedName("Access-Control-Allow-Origin")
    var accessControlAllowOrigin: String? = null,

    @SerializedName("Access-Control-Allow-Methods")
    var accessControlAllowMethods: String? = null,

    @SerializedName("Access-Control-Allow-Headers")
    var accessControlAllowHeaders: String? = null

) : Serializable