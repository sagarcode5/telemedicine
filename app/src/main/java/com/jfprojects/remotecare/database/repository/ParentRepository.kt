package com.jfprojects.remotecare.database.repository

import android.app.Application
import com.jfprojects.remotecare.database.AppDatabase
import com.jfprojects.remotecare.database.NetworkApi
import com.jfprojects.remotecare.database.dao.APILogDao
import com.jfprojects.remotecare.database.models.APILogTable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

abstract class ParentRepository(application: Application?) : NetworkApi.ApiCallBackInterface,
    CoroutineScope {
    private val TAG =
        ParentRepository::class.java.simpleName

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    var apiLogDao: APILogDao?
    var db: AppDatabase? = AppDatabase.getDatabase(application!!)
    var networkApi: NetworkApi? = null

    init {
        apiLogDao = db?.apiLogDao()
    }

    fun addAPITableToDb(
        apiLogTable: APILogTable
    ) {
        apiLogDao?.deleteAllApiLogTable()
        apiLogDao?.insert(apiLogTable)
    }

}