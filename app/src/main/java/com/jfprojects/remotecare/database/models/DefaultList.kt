package com.jfprojects.remotecare.database.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DefaultList(

    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("title")
    var title: String? = null,

    @SerializedName("message")
    var message: String? = null,

    @SerializedName("type")
    var type: String? = null,

    @SerializedName("icon")
    var icon: Int = 0,

    @SerializedName("time")
    var time: String? = null,

    @SerializedName("count")
    var count: Int = 0,

    @SerializedName("subMessage")
    var subMessage: String? = null,

    @SerializedName("isSelected")
    var isSelected: Boolean = false

) : Serializable