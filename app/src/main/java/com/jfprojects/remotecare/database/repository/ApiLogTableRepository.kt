package com.jfprojects.remotecare.database.repository

import android.app.Application
import com.jfprojects.remotecare.database.NetworkApi
import com.jfprojects.remotecare.database.models.ParentResponse
import kotlinx.coroutines.launch

class ApiLogTableRepository(application: Application?) : ParentRepository(application) {
    private val TAG =
        ApiLogTableRepository::class.java.simpleName


    override fun successResponse(apiUrl: String, response: ParentResponse) {
        TODO("Not yet implemented")
    }

    override fun failureResponse(apiUrl: String, response: ParentResponse) {
        TODO("Not yet implemented")
    }

    init {
        networkApi = NetworkApi(apiCallBackInterface = this)
        apiLogDao = db?.apiLogDao()
    }

    fun getAllApiLogs() = apiLogDao?.getAllAPILogTable()

    fun deleteAllApiLogTable() {
        launch { apiLogDao?.deleteAllApiLogTable() }
    }

}