package com.jfprojects.remotecare.database.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Update

interface BaseDao<T> {
    @Insert
    fun insert(vararg obj: T)

    @Insert
    fun insert(appointments: ArrayList<T>)

    @Insert
    fun insert(obj: T)

    @Update
    fun update(obj: T)

    @Update
    fun update(obj: ArrayList<T>)

    @Delete
    fun delete(obj: T)
}