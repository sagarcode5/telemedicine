package com.jfprojects.remotecare.database.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Hospital(

    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("address")
    var address: String? = null

) : Serializable