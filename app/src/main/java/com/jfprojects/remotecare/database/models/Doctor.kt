package com.jfprojects.remotecare.database.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Doctor(

    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("fees")
    var fees: Int? = null,

    @SerializedName("experience")
    var experience: Int? = null,

    @SerializedName("department")
    var department: String? = null

) : Serializable