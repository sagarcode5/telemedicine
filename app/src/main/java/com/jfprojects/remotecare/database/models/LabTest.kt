package com.jfprojects.remotecare.database.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "labTestTable")
data class LabTest(

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("dateTime")
    var dateTime: Long? = 0,

    @SerializedName("labName")
    var labName: String? = null,

    @SerializedName("testsName")
    var testsName: ArrayList<String>? = null,

    /*
    * 1- tests order placed
    * 2- samples taken, waiting for results
    * 3- reports provided
    *
    * */
    @SerializedName("testReportStatus")
    var testReportStatus: Int? = 0,

    /*
    * 1- Home collection
    * 2- Visit Lab
    *
    * */
    @SerializedName("labTestType")
    var labTestType: Int? = 0,

    @SerializedName("patientName")
    var patientName: String? = null,

    @SerializedName("appointmentId")
    var appointmentId: Int? = null,

    @SerializedName("comments")
    var comments: String? = null

) : Serializable