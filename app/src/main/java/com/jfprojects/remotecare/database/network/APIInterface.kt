package com.jfprojects.remotecare.database.network

import com.jfprojects.remotecare.helper.APIConstants
import com.jfprojects.remotecare.database.models.ParentResponse
import com.jfprojects.remotecare.database.models.request.LastQuestionRequest
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface APIInterface {

    @GET(APIConstants.symptoms_url)
    fun getAllQuestions(): Call<ParentResponse>

    @GET(APIConstants.symptoms_url)
    fun getAllQuestionsByOptionId(@Query(APIConstants.refId_query) id: String): Call<ParentResponse>

    @POST(APIConstants.userResponse_url)
    fun updateQuestionsToServer(@Body request: LastQuestionRequest): Call<ParentResponse>

}