package com.jfprojects.remotecare.database.repository

import android.app.Application
import com.jfprojects.remotecare.database.AppDatabase
import com.jfprojects.remotecare.database.dao.APILogDao
import com.jfprojects.remotecare.database.dao.PharmacyDao
import com.jfprojects.remotecare.database.models.PharmacyOrder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class PharmacyRepository(application: Application?) : CoroutineScope {
    private val TAG =
        PharmacyRepository::class.java.simpleName

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    private var apiLogDao: APILogDao? = null
    private var pharmacyDao: PharmacyDao? = null

    init {
        val db = AppDatabase.getDatabase(application!!)
        apiLogDao = db?.apiLogDao()
        pharmacyDao = db?.pharmacyDao()
    }

    fun getPharmacyOrders() = pharmacyDao?.getPharmacyOrders()
//    fun getAppointment(id: Int) = appointmentDao?.getAppointment(id)

    fun addPharmacyOrders(labTest: PharmacyOrder) {
        launch { addPharmacyOrderToDb(labTest) }
    }

//    fun addAppointments(appointment: ArrayList<Appointment>, type: Int) {
//        launch { addAppointmentToDb(appointment, type) }
//    }


//    fun deleteAppointment(id: Int) {
//        launch { deleteAppointmentFromDb(id) }
//    }


    fun updatePharmacyOrder(pharmacyOrder: PharmacyOrder) {
        launch { updatePharmacyOrderFromDb(pharmacyOrder) }
    }


    private suspend fun addPharmacyOrderToDb(
        pharmacyOrder: PharmacyOrder
    ) {
        withContext(Dispatchers.IO) {
            pharmacyDao?.insert(pharmacyOrder)
        }
    }

    private suspend fun deletePharmacyOrderFromDb(
        id: Int
    ) {
        withContext(Dispatchers.IO) {
            pharmacyDao?.deletePharmacyOrders(id)
        }
    }

    private suspend fun updatePharmacyOrderFromDb(
        pharmacyOrder: PharmacyOrder
    ) {
        withContext(Dispatchers.IO) {
            pharmacyDao?.updatePharmacyOrder(pharmacyOrder)
        }
    }

}