package com.jfprojects.remotecare.database.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jfprojects.remotecare.database.models.AppointmentDetail
import java.util.*


class AppointmentDetailConverter {
    @TypeConverter
    fun fromString(value: String?): ArrayList<AppointmentDetail> {
        val listType =
            object : TypeToken<ArrayList<AppointmentDetail?>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: ArrayList<AppointmentDetail?>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}