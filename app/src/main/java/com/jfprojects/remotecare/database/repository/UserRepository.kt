package com.jfprojects.remotecare.database.repository

import android.app.Application
import com.jfprojects.remotecare.database.NetworkApi
import com.jfprojects.remotecare.database.dao.UserDao
import com.jfprojects.remotecare.database.models.ParentResponse
import com.jfprojects.remotecare.database.models.User
import kotlinx.coroutines.launch

class UserRepository(application: Application?) : ParentRepository(application) {
    private val TAG =
        UserRepository::class.java.simpleName

    override fun successResponse(apiUrl: String, response: ParentResponse) {
        TODO("Not yet implemented")
    }

    override fun failureResponse(apiUrl: String, response: ParentResponse) {
        TODO("Not yet implemented")
    }

    private var userDao: UserDao? = null

    init {
        networkApi = NetworkApi(apiCallBackInterface = this)
        userDao = db?.userDao()
    }

    fun getUsers() = userDao?.getUsers()
    fun getUser(id: Int) = userDao?.getUser(id)

    fun addMember(user: User) {
        launch { addUser(user) }
    }

    fun updateMember(user: User) {
        launch { updateUser(user) }
    }

    private fun addUser(
        user: User
    ) {
        userDao?.insert(user)
    }

    private fun updateUser(
        user: User
    ) {
        userDao?.update(user)
    }

}