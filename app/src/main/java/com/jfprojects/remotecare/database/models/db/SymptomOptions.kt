package com.jfprojects.remotecare.database.models.db

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SymptomOptions(

    @field:SerializedName("questionid")
    var questionId: Int = 0,

    @field:SerializedName("name")
    var name: String? = null,

    @field:SerializedName("id")
    var id: Int = 0,

    @field:SerializedName("credit")
    var credit: Int = 0,

    @field:SerializedName("status")
    var status: Boolean = false,

    @field:SerializedName("isSelected")
    var isSelected: Boolean = false

) : Serializable, ParentModel() {
    override fun toString(): String {
        return "SymptomOptions(name=$name)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is SymptomOptions) return false

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name?.hashCode() ?: 0
    }
}

enum class ActionName(val str: String) {
    NoneOfTheAbove("None of the above"), Next("Next")
}
