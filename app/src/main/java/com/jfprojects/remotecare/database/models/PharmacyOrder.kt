package com.jfprojects.remotecare.database.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "pharmacyOrderTable")
data class PharmacyOrder(

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("dateTime")
    var dateTime: Long? = 0,

    @SerializedName("pharmacyName")
    var pharmacyName: String? = null,

    @SerializedName("medicines")
    var medicines: ArrayList<String> = ArrayList(),

    /*
    * 1- pharmacy order placed
    * 2- waiting for order delivery
    * 3- medicine delivered
    *
    * */

    @SerializedName("pharmacyOrderStatus")
    var pharmacyOrderStatus: Int? = 0,

    /*
    * 1- Home delivery
    * 2- Pick up from pharmacy
    *
    * */

    @SerializedName("pharmacyOrderType")
    var pharmacyOrderType: Int? = 0,

    @SerializedName("patientName")
    var patientName: String? = null,

    @SerializedName("appointmentId")
    var appointmentId: Int? = null,

    @SerializedName("comments")
    var comments: String? = null

) : Serializable