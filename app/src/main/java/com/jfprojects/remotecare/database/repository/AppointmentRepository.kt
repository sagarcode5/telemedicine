package com.jfprojects.remotecare.database.repository

import android.app.Application
import com.jfprojects.remotecare.database.dao.AppointmentDao
import com.jfprojects.remotecare.database.models.Appointment
import com.jfprojects.remotecare.database.models.ParentResponse
import kotlinx.coroutines.launch

class AppointmentRepository(application: Application?) : ParentRepository(application) {
    private val TAG =
        AppointmentRepository::class.java.simpleName

    private var appointmentDao: AppointmentDao? = null

    init {
        appointmentDao = db?.appointmentDao()
    }

    fun getAppointments() = appointmentDao?.getAppointments()
    fun getAppointment(id: Int) = appointmentDao?.getAppointment(id)

    fun addAppointment(appointment: Appointment) {
        launch { addAppointmentToDb(appointment, 0) }
    }

    fun addAppointments(appointment: ArrayList<Appointment>, type: Int) {
        launch { addAppointmentToDb(appointment, type) }
    }


    fun deleteAppointment(id: Int) {
        launch { deleteAppointmentFromDb(id) }
    }


    fun updateAppointment(appointment: Appointment) {
        launch { updateAppointmentFromDb(appointment) }
    }


    private fun addAppointmentToDb(
        appointment: Appointment,
        type: Int
    ) {
        appointmentDao?.insert(appointment)
    }

    private fun deleteAppointmentFromDb(
        id: Int
    ) {
        appointmentDao?.delete(Appointment(id = id))

    }

    private fun updateAppointmentFromDb(
        appointment: Appointment
    ) {
        appointmentDao?.update(appointment)
    }


    private fun addAppointmentToDb(
        appointment: ArrayList<Appointment>,
        type: Int
    ) {
        appointmentDao?.insert(appointment)
    }

    override fun successResponse(apiUrl: String, response: ParentResponse) {
        TODO("Not yet implemented")
    }

    override fun failureResponse(apiUrl: String, response: ParentResponse) {
        TODO("Not yet implemented")
    }


}