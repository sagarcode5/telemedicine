package com.jfprojects.remotecare.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.jfprojects.remotecare.database.models.LabTest

@Dao
public interface LabTestsDao {
    /*
     *
     * All LabTest Table CRUD
     *
     * */
    @Insert
    fun insert(labTest: LabTest)

    @Update
    fun updateLabTest(labTest: LabTest)

    @Insert
    fun insertAll(labTests: ArrayList<LabTest>)

    @Query("SELECT * from labTestTable")
    fun getLabTests(): LiveData<List<LabTest?>?>?

    @Query("SELECT * from labTestTable where id=:id LIMIT 1")
    fun getLabTest(id: Int): LiveData<LabTest>

    @Query("DELETE FROM labTestTable where id=:id")
    fun deleteLabTests(id: Int)

}