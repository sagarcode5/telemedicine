package com.jfprojects.remotecare.database.models.response

import android.os.Parcel
import android.os.Parcelable

data class QuestionFinalResponse(
    var color: String? = null,
    var colorStatus: Int = 0,
    var suggestion: String? = null,
    var message: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(color)
        parcel.writeString(suggestion)
        parcel.writeInt(colorStatus)
        parcel.writeString(message)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<QuestionFinalResponse> {
        override fun createFromParcel(parcel: Parcel): QuestionFinalResponse {
            return QuestionFinalResponse(parcel)
        }

        override fun newArray(size: Int): Array<QuestionFinalResponse?> {
            return arrayOfNulls(size)
        }
    }
}

