package com.jfprojects.remotecare.database.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.jfprojects.remotecare.database.converters.AppointmentDetailConverter
import java.io.Serializable

@Entity(tableName = "appointmentTable")
data class Appointment(

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("dateText")
    var dateText: String? = null,

    @SerializedName("dateTime")
    var dateTime: Long = 0,

    @SerializedName("hospitalName")
    var hospitalName: String? = null,

    @SerializedName("professionalName")
    var professionalName: String? = null,

    @SerializedName("medicalDepartment")
    var medicalDepartment: String? = null,

    @TypeConverters(AppointmentDetailConverter::class)
    @SerializedName("appointmentDetailList")
    var appointmentDetailList: ArrayList<AppointmentDetail>? = ArrayList(),

    /*
    * 1- appointment Fixed
    * 2- waiting for doctor Prescription
    * 3- Doctor Uploaded Prescription
    * 4- Testing Lab Appointment Fixed
    * 5- Samples for test sent
    * 6- Tests Done
    * 7- Pharmacy ordered
    * 8- Self- Assessment Pending
    *
    * */
    @SerializedName("appointmentStatus")
    var appointmentStatus: Int? = 0,

    /*
    * 1- Telehealth Consultation
    * 2- Follow-up Telehealth Consultation
    * 3- Video Consultation
    * 4- Home Care
    * 5- Other
    * */
    @SerializedName("appointmentType")
    var appointmentType: Int? = 0,

    @SerializedName("patientName")
    var patientName: String? = null,

    @SerializedName("selectedTime")
    var selectedTime: String? = null,

    @SerializedName("commentSymptoms")
    var commentSymptoms: String? = null
) : Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Appointment) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }
}