package com.jfprojects.remotecare.database.models.db

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class CategoryModel(

    @field:SerializedName("questionid")
    var questionId: Int = 0,

    @field:SerializedName("name")
    var name: String? = null,

    @field:SerializedName("description")
    var description: String? = null,

    @field:SerializedName("credit")
    var credit: Int = 0,

    @field:SerializedName("status")
    var status: Boolean = false
) : Serializable, ParentModel()
