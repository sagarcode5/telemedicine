package com.jfprojects.remotecare.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.jfprojects.remotecare.database.models.User

@Dao
public interface UserDao : BaseDao<User> {
    /*
     *
     * Current User Table CRUD
     *
     * */

    @Query("SELECT * from user_table")
    fun getUsers(): LiveData<List<User?>?>?

    @Query("SELECT * from user_table where id=:id LIMIT 1")
    fun getUser(id: Int): LiveData<User>

    @Query("DELETE FROM user_table where id=:id")
    fun deleteUser(id: Int)

}