//package com.jfprojects.remotecare.database.repository
//
//import android.app.Application
//import com.google.gson.Gson
//import com.jfprojects.remotecare.helper.APIConstants
//import com.jfprojects.remotecare.database.NetworkApi
//import com.jfprojects.remotecare.database.dao.QuestionDao
//import com.jfprojects.remotecare.database.models.APILogTable
//import com.jfprojects.remotecare.database.models.JSONType
//import com.jfprojects.remotecare.database.models.ParentResponse
//import com.jfprojects.remotecare.database.models.db.Question
//import com.jfprojects.remotecare.database.models.db.SymptomsQuestion
//import com.jfprojects.remotecare.database.network.RetrofitClient
//import kotlinx.coroutines.CoroutineScope
//import kotlinx.coroutines.Dispatchers
//import kotlinx.coroutines.launch
//import org.json.JSONArray
//import org.json.JSONException
//import kotlin.coroutines.CoroutineContext
//
//class SymptomsRepository(application: Application?) : ParentRepository(application),
//    NetworkApi.ApiCallBackInterface, CoroutineScope {
//    private val TAG =
//        SymptomsRepository::class.java.simpleName
//
//    override val coroutineContext: CoroutineContext
//        get() = Dispatchers.Main
//
//    private var questionDao: QuestionDao? = null
//    private var networkApi: NetworkApi? = null
//
//    private var optionId: Int = 0
//
//    init {
//        networkApi = NetworkApi(this)
//        questionDao = db?.questionDao()
//    }
//
//    fun getQuestions() = questionDao?.getAllQuestions()
//
//    fun getQuestionsFromServer() {
//        networkApi?.NetworkCall(
//            APIConstants.getAllQuestions,
//            RetrofitClient.getClient()!!.getAllQuestions()
//        )
//    }
//
//    fun getQuestionsFromServerByOptionId(id: Int) {
//        optionId = id
//        networkApi?.NetworkCall(
//            APIConstants.getAllQuestionsByOptionId,
//            RetrofitClient.getClient()!!.getAllQuestionsByOptionId(id)
//        )
//    }
//
//    fun deleteQuestion(id: Int) {
//        launch { deleteQuestionFromDb(id) }
//    }
//
//
//    fun updateQuestion(symptomsQuestion: SymptomsQuestion) {
//        launch { updateQuestionFromDb(symptomsQuestion) }
//    }
//
//    private fun deleteQuestionFromDb(
//        id: Int
//    ) {
//        questionDao?.delete(Question(id = id))
//
//    }
//
//    private fun updateQuestionFromDb(
//        question: SymptomsQuestion
//    ) {
//        questionDao?.update(question)
//    }
//
//    private fun addQuestionToDb(questions: ArrayList<SymptomsQuestion>, type: Int) {
//        if (type == 1) questionDao?.deleteAllApiQuestions()
//        questionDao?.insert(questions)
//    }
//
//    override fun successResponse(apiUrl: String, response: ParentResponse) {
//        when (apiUrl) {
//            APIConstants.getAllQuestions,
//            APIConstants.getAllQuestionsByOptionId -> {
//                val questions: ArrayList<SymptomsQuestion> =
//                    ArrayList()
//                var array = JSONArray()
//                try {
//                    array = JSONArray(response.body)
//                    if (array.length() > 0) {
//                        for (i in 0 until array.length()) {
//                            val question = Gson().fromJson(
//                                array.get(i).toString(),
//                                SymptomsQuestion::class.java
//                            )
//                            questions.add(question)
//                        }
//                    }
//                } catch (e: JSONException) {
//                    e.printStackTrace()
//                } finally {
//
//                    launch {
//                        addQuestionToDb(
//                            questions = questions,
//                            type = when (apiUrl) {
//                                APIConstants.getAllQuestions -> 1
//                                APIConstants.getAllQuestionsByOptionId -> 2
//                                else -> 1
//                            }
//                        )
//                    }
//
//                    launch {
//                        addAPITableToDb(
//                            apiLogTable = APILogTable(
//                                jsonType = JSONType.JsonArray.name,
//                                apiType = apiUrl,
//                                statusCode = response.statusCode,
//                                body = array.toString()
//                            )
//                        )
//                    }
//                }
//            }
//        }
//    }
//
//    override fun failureResponse(apiUrl: String, response: ParentResponse) {
//        when (apiUrl) {
//
//            APIConstants.getAllQuestions -> launch {
//                addAPITableToDb(
//                    apiLogTable = APILogTable(
//                        apiType = apiUrl,
//                        statusCode = response.statusCode,
//                        body = response.body
//                    )
//                )
//            }
//        }
//    }
//
//
//}