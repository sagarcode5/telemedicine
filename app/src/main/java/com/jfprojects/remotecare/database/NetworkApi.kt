package com.jfprojects.remotecare.database

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.jfprojects.remotecare.helper.APIConstants
import com.jfprojects.remotecare.helper.isConnected
import com.jfprojects.remotecare.helper.logError
import com.jfprojects.remotecare.R
import com.jfprojects.remotecare.database.models.ParentResponse
import com.jfprojects.remotecare.helper.RemoteCareApp
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class NetworkApi(var apiCallBackInterface: ApiCallBackInterface? = null) {

    private val TAG = "NetworkApi"

    fun networkCall(apiType: String, apiCall: Call<ParentResponse>) {
        logError(TAG, "================$apiType")
        if (isConnected()) {
            apiCall.enqueue(object : Callback<ParentResponse?> {
                override fun onResponse(
                    call: Call<ParentResponse?>?,
                    response: Response<ParentResponse?>
                ) {
                    if (response.code() == 200) {

                        if ((if (response.body() != null) response.body()!!.statusCode else 0) == 200
                        ) {
                            apiCallBackInterface?.successResponse(apiType, response.body()!!)
                        } else if ((if (response.body() != null) response.body()!!.statusCode else 0) == 400 ||
                            (if (response.body() != null) response.body()!!.statusCode else 0) == 401
                        ) {
                            apiCallBackInterface?.failureResponse(apiType, response.body()!!)
                        } else {

                            apiCallBackInterface?.failureResponse(
                                apiType,
                                ParentResponse(
                                    statusCode = 500, body = Gson().toJson(
                                        JsonObject().addProperty(
                                            APIConstants.ERR,
                                            RemoteCareApp.appContext?.resources?.getString(R.string.something_went_wrong)
                                        )
                                    )
                                )
                            )
                        }
                    } else if (response.code() == 400 || response.code() == 401) {
                        apiCallBackInterface?.failureResponse(
                            apiType,
                            ParentResponse(statusCode = response.code())
                        )
                    } else {

                        apiCallBackInterface?.failureResponse(
                            apiType, ParentResponse(
                                statusCode = response.code(),
                                body = Gson().toJson(
                                    JsonObject().addProperty(
                                        APIConstants.ERR,
                                        RemoteCareApp.appContext?.resources?.getString(R.string.something_went_wrong)
                                    )
                                )
                            )
                        )
                    }
                }

                override fun onFailure(
                    call: Call<ParentResponse?>?,
                    t: Throwable?
                ) {
                    apiCallBackInterface?.failureResponse(
                        apiType, ParentResponse(
                            statusCode = 500, body = Gson().toJson(
                                JsonObject().addProperty(
                                    APIConstants.ERR,
                                    RemoteCareApp.appContext?.resources?.getString(R.string.something_went_wrong)
                                )
                            )
                        )
                    )
                }
            })
        } else {
            apiCallBackInterface?.failureResponse(
                apiType, ParentResponse(
                    statusCode = 500, body = Gson().toJson(
                        JsonObject().addProperty(
                            APIConstants.ERR,
                            RemoteCareApp.appContext?.resources?.getString(R.string.something_went_wrong)
                        )
                    )
                )
            )
        }
    }

    interface ApiCallBackInterface {
        fun successResponse(apiUrl: String, response: ParentResponse)
        fun failureResponse(apiUrl: String, response: ParentResponse)
    }

}