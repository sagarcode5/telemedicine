package com.jfprojects.remotecare.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.jfprojects.remotecare.database.models.APILogTable
import java.util.*

@Dao
public interface APILogDao:BaseDao<APILogTable>{

    /*
     *
     * All API Log Table CRUD
     *
     * */

    @Query("SELECT * from apiLogTable")
    fun getAllAPILogTable(): LiveData<List<APILogTable>?>?

    @Query("DELETE FROM apiLogTable")
    fun deleteAllApiLogTable()

}