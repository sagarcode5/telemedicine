package com.jfprojects.remotecare.database.models.db

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.jfprojects.remotecare.database.converters.OptionsConverter
import java.io.Serializable

@Entity(tableName = "questionTable")
data class Question(

    @PrimaryKey(autoGenerate = true)
    @field:SerializedName("id")
    var id: Int = 0,

    @field:SerializedName("localId")
    var localId: String? = null,

    @field:SerializedName("viewType")
    var viewType: String,

    @field:SerializedName("isQuestionAnswered")
    var isQuestionAnswered: Boolean = false,

    @Embedded(prefix = "question_")
    var symptomsQuestion: SymptomsQuestion = SymptomsQuestion(),
//
    @TypeConverters(OptionsConverter::class)
    var symptomOptions: ArrayList<SymptomOptions> = ArrayList()

) : ParentModel(), Serializable

enum class QuestionViewType {
    AIQuestion, Answer
}