package com.jfprojects.remotecare.database.models.db

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class ParentModel(

    @field:SerializedName("created_at")
    var createAt: String? = null,

    @field:SerializedName("updated_at")
    var updateAt: String? = null

) : Serializable
