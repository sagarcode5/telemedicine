package com.jfprojects.remotecare.database.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "family_member_table")
data class FamilyMember(

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("userId")
    var userId: String? = null,

    @SerializedName("firstName")
    var firstName: String? = null,

    @SerializedName("lastName")
    var lastName: String? = null,

    @SerializedName("mobile")
    var mobile: String? = null,

    @SerializedName("aadhar")
    var aadhar: String? = null,

    @SerializedName("imagePath")
    var imagePath: String? = null,

    @SerializedName("imageUri")
    var imageUri: String? = null,

    @SerializedName("gender")
    var gender: String? = null,

    @SerializedName("relation")
    var relation: String? = null,

    @SerializedName("dob")
    var dob: Long = 0

) : Serializable

enum class RelationType {
    Father,
    Mother,
    Self,
    Spouse,
    Sister,
    Brother,
    Son,
    Daughter,
}