package com.jfprojects.remotecare.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.jfprojects.remotecare.database.models.PharmacyOrder

@Dao
public interface PharmacyDao {
    /*
     *
     * All PharmacyOrder Table CRUD
     *
     * */
    @Insert
    fun insert(pharmacyOrder: PharmacyOrder)

    @Update
    fun updatePharmacyOrder(pharmacyOrder: PharmacyOrder)

    @Insert
    fun insertAll(pharmacyOrders: ArrayList<PharmacyOrder>)

    @Query("SELECT * from pharmacyOrderTable")
    fun getPharmacyOrders(): LiveData<List<PharmacyOrder?>?>?

    @Query("SELECT * from pharmacyOrderTable where id=:id LIMIT 1")
    fun getPharmacyOrder(id: Int): LiveData<PharmacyOrder>

    @Query("DELETE FROM pharmacyOrderTable where id=:id")
    fun deletePharmacyOrders(id: Int)

}