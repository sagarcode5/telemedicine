package com.jfprojects.remotecare.database.models.request

import com.google.gson.annotations.SerializedName
import com.jfprojects.remotecare.database.models.db.SymptomsQuestion
import java.io.Serializable

data class LastQuestionRequest(

    @field:SerializedName("questions")
    var questions: List<SymptomsQuestion> = ArrayList(),

    @field:SerializedName("totalcredit")
    var totalCredit: Int = 0,

    @field:SerializedName("userid")
    var userid: String? = null
) : Serializable