package com.jfprojects.remotecare.database.repository

import android.app.Application
import com.google.gson.Gson
import com.jfprojects.remotecare.helper.APIConstants
import com.jfprojects.remotecare.helper.generateString
import com.jfprojects.remotecare.helper.log
import com.jfprojects.remotecare.database.NetworkApi
import com.jfprojects.remotecare.database.dao.QuestionDao
import com.jfprojects.remotecare.database.models.APILogTable
import com.jfprojects.remotecare.database.models.JSONType
import com.jfprojects.remotecare.database.models.ParentResponse
import com.jfprojects.remotecare.database.models.db.Question
import com.jfprojects.remotecare.database.models.db.QuestionViewType
import com.jfprojects.remotecare.database.models.db.SymptomsQuestion
import com.jfprojects.remotecare.database.models.request.LastQuestionRequest
import com.jfprojects.remotecare.database.network.RetrofitClient
import com.jfprojects.remotecare.ui.main.viewmodel.QuestionsViewModel
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONException

class QuestionsRepository(application: Application?) : ParentRepository(application) {
    private val TAG =
        QuestionsRepository::class.java.simpleName

    private var questionDao: QuestionDao? = null

    private var optionId: String? = null
    private var lastQuestionRequest: LastQuestionRequest? = null

    init {
        networkApi = NetworkApi(apiCallBackInterface = this)
        questionDao = db?.questionDao()
    }

    fun getAllQuestions() = questionDao?.getAllQuestions()
    fun getQuestions() = questionDao?.getQuestions()

    fun updateQuestions(list: ArrayList<Question>) {
        launch { updateQuestionFromDb(list) }
    }

    fun getQuestionsFromServer() {
        networkApi?.networkCall(
            APIConstants.getAllQuestions,
            RetrofitClient.getClient()!!.getAllQuestions()
        )
    }

    fun getQuestionsFromServerByOptionId(id: String) {
        optionId = id
        networkApi?.networkCall(
            APIConstants.getAllQuestionsByOptionId,
            RetrofitClient.getClient()!!.getAllQuestionsByOptionId(id)
        )
    }

    fun updateQuestionsToServer(request: LastQuestionRequest) {
        lastQuestionRequest = request
        networkApi?.networkCall(
            APIConstants.updateQuestionsToServer,
            RetrofitClient.getClient()!!.updateQuestionsToServer(request)
        )
    }
//
//    fun deleteQuestion(id: Int) {
//        launch { deleteQuestionFromDb(id) }
//    }


    fun addQuestion(questions: ArrayList<Question>) {
        launch { addQuestionToDb(questions, 2) }
    }

    fun deleteQuestionFromDb(
    ) {
        launch {
            questionDao?.deleteAllQuestions()
        }
    }

    private fun addQuestionToDb(questions: ArrayList<Question>, type: Int) {
        if (type == 1) questionDao?.deleteAllQuestions()
        questionDao?.insert(questions)
    }

    private fun updateQuestionFromDb(
        question: ArrayList<Question>
    ) {
        questionDao?.update(question)
    }

    override fun successResponse(apiUrl: String, response: ParentResponse) {
        log(TAG, if (response.body != null) Gson().toJson(response.body) else null.toString())
        when (apiUrl) {
            APIConstants.getAllQuestions,
            APIConstants.getAllQuestionsByOptionId -> {
                val questions: ArrayList<Question> =
                    ArrayList()
                var array = JSONArray()
                try {
                    array = JSONArray(response.body)
                    if (array.length() > 0) {
                        for (i in 0 until array.length()) {
                            val question = Gson().fromJson(
                                array.get(i).toString(),
                                SymptomsQuestion::class.java
                            )

                            questions.add(
                                Question(
                                    localId = generateString(8),
                                    symptomsQuestion = question,
                                    isQuestionAnswered = false,
                                    viewType = QuestionViewType.AIQuestion.name
                                )
                            )
                        }
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                } finally {

                    when (apiUrl) {
                        APIConstants.getAllQuestions -> {
                            QuestionsViewModel.localQuestions[0] = questions

                            launch {
                                addQuestionToDb(
                                    questions = arrayListOf(questions.first()),
                                    type = 1
                                )
                            }
                        }

                        APIConstants.getAllQuestionsByOptionId -> {
//                            if (questions.size > 1) {
                            QuestionsViewModel.localQuestions[QuestionsViewModel.selectedCatId] =
                                questions
//                            }

                            launch {
                                addQuestionToDb(
                                    questions = arrayListOf(questions.first()),
                                    type = 2
                                )
                            }
                        }
                    }

                    launch {
                        addAPITableToDb(
                            apiLogTable = APILogTable(
                                jsonType = JSONType.JsonArray.name,
                                apiType = apiUrl,
                                statusCode = response.statusCode,
                                body = array.toString()
                            )
                        )
                    }
                }
            }

            APIConstants.updateQuestionsToServer -> {

                launch {
                    addAPITableToDb(
                        apiLogTable = APILogTable(
                            jsonType = JSONType.JsonObject.name,
                            apiType = apiUrl,
                            statusCode = response.statusCode,
                            body = response.body.toString()
                        )
                    )
                }
            }
        }
    }

    override fun failureResponse(apiUrl: String, response: ParentResponse) {
        log(TAG, if (response.body != null) Gson().toJson(response.body) else null.toString())

        when (apiUrl) {

            APIConstants.getAllQuestions,
            APIConstants.getAllQuestionsByOptionId,
            APIConstants.updateQuestionsToServer -> {

                launch {
                    addAPITableToDb(
                        apiLogTable = APILogTable(
                            apiType = apiUrl,
                            statusCode = response.statusCode,
                            body = response.body
                        )
                    )
                }
            }
        }
    }
}