package com.jfprojects.remotecare.database.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "user_table")
data class User(

    @PrimaryKey
    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("firstName")
    var firstName: String? = null,

    @SerializedName("lastName")
    var lastName: String? = null,

    @SerializedName("email")
    var email: String? = null,

    @SerializedName("mobile")
    var mobile: String? = null,

    @SerializedName("aadhar")
    var aadhar: String? = null,

    @SerializedName("imagePath")
    var imagePath: String? = null,

    @SerializedName("imageUri")
    var imageUri: String? = null,

    @SerializedName("nationality")
    var nationality: String? = null,

    @SerializedName("gender")
    var gender: String? = null,

    @SerializedName("dob")
    var dob: Long = 0,

    @SerializedName("country")
    var country: String? = null,

    @SerializedName("city")
    var city: String? = null,

    @SerializedName("address")
    var address: String? = null,

    @SerializedName("locationPref")
    var locationPref: Int? = 0,

    @SerializedName("description")
    var description: String? = null

) : Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is User) return false

        if (mobile != other.mobile) return false

        return true
    }

    override fun hashCode(): Int {
        return mobile?.hashCode() ?: 0
    }
}