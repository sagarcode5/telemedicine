package com.jfprojects.remotecare.database.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "apiLogTable")
data class APILogTable(

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("body")
    var body: String? = null,

    @SerializedName("statusCode")
    var statusCode: Int? = 0,

    @SerializedName("apiType")
    var apiType: String? = null,

    @SerializedName("jsonType")
    var jsonType: String? = null

) : Serializable


enum class JSONType {
    JsonObject,JsonArray
}

