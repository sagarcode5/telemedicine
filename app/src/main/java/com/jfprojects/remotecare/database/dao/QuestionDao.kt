package com.jfprojects.remotecare.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.jfprojects.remotecare.database.models.db.Question

@Dao
public interface QuestionDao : BaseDao<Question> {

    /*
     *
     * All API Log Table CRUD
     *
     * */

    @Query("SELECT * from questionTable")
    fun getAllQuestions(): LiveData<List<Question>>?

    @Query("SELECT * from questionTable")
    fun getQuestions(): List<Question>

    @Query("DELETE FROM questionTable")
    fun deleteAllQuestions()

}