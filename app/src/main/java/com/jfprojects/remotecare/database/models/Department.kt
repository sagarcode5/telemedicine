package com.jfprojects.remotecare.database.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Department(

    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("description")
    var department: String? = null

) : Serializable