package com.jfprojects.remotecare.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.jfprojects.remotecare.database.models.Appointment

@Dao
public interface AppointmentDao:BaseDao<Appointment> {

    @Insert
    fun insertAll(appointments: ArrayList<Appointment>)

    @Query("SELECT * from appointmentTable")
    fun getAppointments(): LiveData<List<Appointment?>?>?

    @Query("SELECT * from appointmentTable where id=:id LIMIT 1")
    fun getAppointment(id: Int): LiveData<Appointment>

    @Query("DELETE FROM appointmentTable where id=:id")
    fun deleteAppointments(id: Int)

}