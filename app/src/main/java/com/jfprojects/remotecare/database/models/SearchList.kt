package com.jfprojects.remotecare.database.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class SearchList(

    @SerializedName("id")
    var id: Int = 0,

    /**/
    @SerializedName("viewType")
    var viewType: Int = 0,

    @SerializedName("hospital")
    var hospital: Hospital? = null,

    @SerializedName("department")
    var department: Department? = null,

    @SerializedName("defaultList")
    var defaultList: DefaultList? = null,

    @SerializedName("doctor")
    var doctor: Doctor? = null,

    @SerializedName("isSelected")
    var isSelected: Boolean = false
) : Serializable