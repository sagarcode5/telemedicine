package com.jfprojects.remotecare.database.repository

import android.app.Application
import com.jfprojects.remotecare.database.NetworkApi
import com.jfprojects.remotecare.database.dao.FamilyMembersDao
import com.jfprojects.remotecare.database.models.FamilyMember
import com.jfprojects.remotecare.database.models.ParentResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FamilyMembersRepository(application: Application?) : ParentRepository(application) {
    private val TAG =
        FamilyMembersRepository::class.java.simpleName

    override fun successResponse(apiUrl: String, response: ParentResponse) {
        TODO("Not yet implemented")
    }

    override fun failureResponse(apiUrl: String, response: ParentResponse) {
        TODO("Not yet implemented")
    }

    private var familyMembersDao: FamilyMembersDao? = null

    init {
        networkApi = NetworkApi(apiCallBackInterface = this)
        familyMembersDao = db?.familyMemberDao()
    }

    fun getFamilyMembers() = familyMembersDao?.getFamilyMembers()
    fun getFamilyMember(id:Int) = familyMembersDao?.getFamilyMember(id)

    fun addMember(familyMember: FamilyMember) {
        launch { addFamilyMember(familyMember) }
    }

    fun updateMember(familyMember: FamilyMember) {
        launch { updateFamilyMember(familyMember) }
    }

//    fun addAppointments(appointment: ArrayList<Appointment>, type: Int) {
//        launch { addAppointmentToDb(appointment, type) }
//    }


//    fun deleteAppointment(id: Int) {
//        launch { deleteAppointmentFromDb(id) }
//    }


//    fun updateLabTest(labTest: LabTest) {
//        launch { updateLabTestFromDb(labTest) }
//    }

    private fun addFamilyMember(
        familyMember: FamilyMember
    ) {
            familyMembersDao?.insert(familyMember)
    }

    private fun updateFamilyMember(
        familyMember: FamilyMember
    ) {
            familyMembersDao?.update(familyMember)
    }

    private suspend fun updateLabTestFromDb(
        familyMember: FamilyMember
    ) {
//        withContext(Dispatchers.IO) {
        familyMembersDao?.update(familyMember)
//        }
    }

}