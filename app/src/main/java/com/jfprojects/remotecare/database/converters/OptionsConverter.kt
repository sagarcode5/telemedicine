package com.jfprojects.remotecare.database.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jfprojects.remotecare.database.models.db.SymptomOptions
import java.util.*


class OptionsConverter {
    @TypeConverter
    fun fromString(value: String?): ArrayList<SymptomOptions> {
        val listType =
            object : TypeToken<ArrayList<SymptomOptions?>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: ArrayList<SymptomOptions?>?): String {
        return Gson().toJson(list)
    }
}