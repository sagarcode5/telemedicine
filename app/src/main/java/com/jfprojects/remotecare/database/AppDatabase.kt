package com.jfprojects.remotecare.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.jfprojects.remotecare.BuildConfig
import com.jfprojects.remotecare.database.converters.AppointmentDetailConverter
import com.jfprojects.remotecare.database.converters.DefaultListConverter
import com.jfprojects.remotecare.database.converters.OptionsConverter
import com.jfprojects.remotecare.database.converters.StringConverter
import com.jfprojects.remotecare.database.dao.*
import com.jfprojects.remotecare.database.models.*
import com.jfprojects.remotecare.database.models.db.Question


@Database(
    entities = [
        /*Common Module*/
        APILogTable::class,
        Appointment::class,
        LabTest::class,
        PharmacyOrder::class,
        User::class,
        Question::class,
        FamilyMember::class
    ],
    version = BuildConfig.VERSION_CODE,
    exportSchema = false
)

@TypeConverters(
    value = [
        StringConverter::class,
        AppointmentDetailConverter::class,
        DefaultListConverter::class,
        OptionsConverter::class
    ]
)

abstract class AppDatabase : RoomDatabase() {
    abstract fun apiLogDao(): APILogDao?
    abstract fun appointmentDao(): AppointmentDao?
    abstract fun questionDao(): QuestionDao?
    abstract fun labTestDao(): LabTestsDao?
    abstract fun pharmacyDao(): PharmacyDao?
    abstract fun familyMemberDao(): FamilyMembersDao?
    abstract fun userDao(): UserDao?

    companion object {
        private var INSTANCE: AppDatabase? = null

        //    // This callback is called when the database has opened.
        //    // In this case, use PopulateDbAsync to populate the database
        //    // with the initial data set if the database has no entries.
        private val sRoomDatabaseCallback: Callback =
            object : Callback() {
                override fun onOpen(db: SupportSQLiteDatabase) {
                    super.onOpen(db)
                }
            }

        fun getDatabase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class.java) {
                    if (INSTANCE == null) {
                        // Create database here.
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            AppDatabase::class.java, "app_database_remote_care"
                        ) // Wipes and rebuilds instead of migrating if no Migration object.
                            // Migration is not part of this practical.
                            .fallbackToDestructiveMigration()
                            .allowMainThreadQueries()
                            .addCallback(sRoomDatabaseCallback)
                            .build()
                    }
                }
            }
            return INSTANCE
        }
    }
}