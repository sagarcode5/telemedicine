package com.jfprojects.remotecare.database.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jfprojects.remotecare.database.models.DefaultList
import java.util.*

class DefaultListConverter {
    @TypeConverter
    fun fromString(value: String?): ArrayList<DefaultList> {
        val listType =
            object : TypeToken<ArrayList<DefaultList?>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: ArrayList<DefaultList?>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}